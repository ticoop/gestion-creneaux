<?php

return [
    'server' => env('DOLIBARR_SERVER'),
    'username' => env('DOLIBARR_USERNAME'),
    'password' => env('DOLIBARR_PASSWORD'),
];
