<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'ticoop_auth' => [
        'base_url' => env('OAUTH_BASE_URL', 'https://my.ticoop.fr'),
        'realm' => env('OAUTH_REALM', 'ticoop'),
        'auth_url' => env('OAUTH_BASE_URL', 'https://my.ticoop.fr') . '/auth/realms/' . env('OAUTH_REALM', 'ticoop') . '/protocol/openid-connect',
        'client_id' => env('OAUTH_CLIENT_ID', ''),
        'client_secret' => env('OAUTH_CLIENT_SECRET', ''),
        'redirect' => getenv('APP_URL') . '/login/callback',
        'master_realm' => env('OAUTH_MASTER_REALM', 'ticoop'),

        'import_users_username' => env('IMPORT_USERS_USERNAME', ''),
        'import_users_password' => env('IMPORT_USERS_PASSWORD', ''),
    ],

];
