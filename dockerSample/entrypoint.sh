#!/bin/sh

sudo -u www-data php /var/www/html/artisan migrate --force
sudo -u www-data /scheduler.sh &
sudo -u www-data php /var/www/html/artisan queue:work &
apache2-foreground
