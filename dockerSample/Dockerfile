FROM php:7.3-apache

ARG ENV_FILE

WORKDIR /home

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update
RUN apt-get install -y nodejs git unzip wget libmagickwand-dev postgresql-client libpq-dev sudo libzip-dev

RUN docker-php-ext-install exif
RUN docker-php-ext-install pgsql
RUN docker-php-ext-install pdo_pgsql
RUN docker-php-ext-install zip
RUN docker-php-ext-install gd
RUN pecl install imagick
ADD php-ext-imagick.ini /usr/local/etc/php/conf.d/php-ext-imagick.ini

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"

RUN wget https://gitlab.com/TiCoop/gestion-creneaux/-/archive/master/gestion-creneaux-master.zip
RUN unzip gestion-creneaux-master.zip
RUN rm -rf /var/www/html
RUN mv gestion-creneaux-master /var/www/html
RUN rm gestion-creneaux-master.zip

ADD $ENV_FILE /var/www/html/.env

WORKDIR /var/www/html

RUN /home/composer.phar install --no-dev
RUN php artisan config:cache
RUN php artisan vue-i18n:generate

RUN npm i
RUN npm run prod

ADD 000-default.conf /etc/apache2/sites-available/000-default.conf

ADD oauth-private.key /var/www/html/storage/oauth-private.key
ADD oauth-public.key /var/www/html/storage/oauth-public.key

RUN chown www-data:www-data -R /var/www/html/storage

RUN a2enmod rewrite

ADD scheduler.sh /scheduler.sh

ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
