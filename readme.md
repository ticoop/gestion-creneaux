## Gestion-créneaux

Cette application a été développée par Gwendal, un bénévole du supermarché coopératif Ti Coop basé à Brest.
Elle permet de gérer les temps de travail des coopérateur·rice·s au magasin, appelés "Créneaux".
Le planning est découpé en cycle de 4 semaines (A B C D) et un semaine est découpée en créneaux.
Un·e coopérateur·rice effectue 1 créneau par cycle, soit tous les 28 jours.
Ceci est le principe de base de l'application, bien sûr de nombreuses particularités viennent l'enrichir.

Des vidéos de démonstration de l'application ont été réalisées et sont disponible à cette adresse : https://ticoop.fr/application-de-gestion-des-creneaux/

<img src="./Accueil.png"></img>


## Installation

Nous l'avons déployé chez nous dans un Docker, les DOCKERFILES sont disponibles à la demande.
L'application a besoin d'un serveur web capable d'interprêter le PHP et d'une base de données Postgres. Quelques variables d'environnement doivent être configurées.
L'authentification des comptes se fait via un serveur OAuth à configurer


## Contribuer

Il n'y a pour l'instant pas de règles réellement définies pour vous permettre de partager vos contributions. Prenez contact avec nous et nous nous ferons une joie d'intégrer vos contributions


## Licence

Le code est sous licence AGPLv3, celle-ci est contaminante imposant à toute personne souhaitant ré-utiliser le code, de mettre son propre projet sous AGPLv3