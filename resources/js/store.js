import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        user: null,
        authLoaded: false,
        beforeImpersonateRoute: null,
        debug: false,
        technicalView: null,
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
            Vue.prototype.$cache.clear();
        },
        setAuthLoaded(state, value) {
            state.authLoaded = value;
        },
        setBeforeImpersonateRoute(state, value) {
            state.beforeImpersonateRoute = value;
        },
        setDebug(state, value) {
            state.debug = value;
        },
        setTechnicalView(state, params) {
            state.technicalView = params;
        },
    },
    actions: {
        getUser({commit}) {
            commit('setAuthLoaded', false);
            const roleMap = {};
            Vue.prototype.$cache.getRoles().then((roles) => {
                roles.forEach((role) => {
                    roleMap[role.id] = role.key;
                });
            });
            return Vue.prototype.$request.get('/api/user?with_roles=1').then((response) => {
                commit('setUser', {...response.data.data, roles: response.data.data.roles.map((r) => roleMap[r])});
                commit('setAuthLoaded', true);
                return response.data;
            }).catch((error) => {
                commit('setUser', null);
                commit('setAuthLoaded', true);
                return null;
            });
        },
        setBeforeImpersonateRoute({commit}, route) {
            commit('setBeforeImpersonateRoute', route);
        },
        setDebug({commit}, value) {
            commit('setDebug', value);
        },
        setTechnicalView({commit}, params) {
            commit('setTechnicalView', params);
        },
    },
    getters: {
        authenticated(state) {
            return state.user !== null;
        },
        userRoles(state) {
            return !!state.user ? state.user.roles : [];
        },
    },
});

export default store;
