import Home from './views/Home.vue';
import Schedule from './views/Schedule.vue';
import UserIndex from './views/users/Index.vue';
import UserUpdate from './views/users/Update.vue';
import UserShow from './views/users/Show.vue';
import ScheduleIndex from './views/admin-schedule/Index.vue';
import ScheduleForm from './views/admin-schedule/Form.vue';
import ScheduleDuplicate from './views/admin-schedule/Duplicate.vue';
import SlotAssignment from './views/SlotAssignment.vue';
import TrainingIndex from './views/trainings/Index.vue';
import TrainingCreate from './views/trainings/Create.vue';
import TrainingUpdate from './views/trainings/Update.vue';
import LeaveTypeIndex from './views/leave-types/Index.vue';
import LeaveTypeCreate from './views/leave-types/Create.vue';
import LeaveTypeUpdate from './views/leave-types/Update.vue';
import LeaveIndex from './views/leaves/Index.vue';
import LeaveCreate from './views/leaves/Create.vue';
import LeaveUpdate from './views/leaves/Update.vue';
import KeyCodeIndex from './views/key-codes/Index.vue';
import SentEmailIndex from './views/sent-emails/Index.vue';
import SentEmailShow from './views/sent-emails/Show.vue';
import Config from './views/Config.vue';
import AssignmentSwitchOfferIndex from './views/assignment-switch-offers/Index.vue';
import AssignmentSwitchOfferCreate from './views/assignment-switch-offers/Create.vue';
import AssignmentSwitchOfferUpdate from './views/assignment-switch-offers/Update.vue';
import AssignmentSwitchOfferReply from './views/assignment-switch-offers/Reply.vue';
import AssignmentSwitchOfferCheckReplies from './views/assignment-switch-offers/CheckReplies.vue';
import MySchedule from './views/MySchedule.vue';
import ErrorPage from './views/ErrorPage.vue';
import Profile from './views/Profile.vue';
import Badging from './views/Badging.vue';
import RescueTeam from './views/RescueTeam.vue';
import UserStatuses from './views/UserStatuses.vue';
import EmergencyTeamPlanning from './views/EmergencyTeamPlanning.vue';
import EmergencyNumbers from './views/EmergencyNumbers.vue';
import AssignmentTeam from './views/AssignmentTeam.vue';
import Stats from './views/Stats.vue';
import AttendanceMonitoring from './views/AttendanceMonitoring.vue';
import TechnicalTools from './views/TechnicalTools.vue';
import IncompleteSwitches from './views/IncompleteSwitches.vue';
import SlotsWithoutReferent from './views/SlotsWithoutReferent.vue';
import IncompleteSlots from './views/IncompleteSlots.vue';
import MyTeamAttendance from './views/MyTeamAttendance.vue';
import NewReferents from './views/NewReferents.vue';
import EndingLeaves from './views/EndingLeaves.vue';

const routes = [
    {
        path: '/',
        component: Home,
        name: 'home',
    },
    {
        path: '/schedule',
        component: Schedule,
        name: 'schedule',
        children: [
            {
                path: 'users/:user',
                components: {
                    modal: UserShow,
                },
                name: 'schedule-show-user',
            },
            {
                path: 'users/:user/edit',
                components: {
                    modal: UserUpdate,
                },
                name: 'schedule-update-user',
            },
        ],
    },
    {
        path: '/my-schedule',
        component: MySchedule,
        name: 'my-schedule',
        children: [
            {
                path: 'view-team/:assignment',
                components: {
                    modal: AssignmentTeam,
                },
                name: 'view-assignment-team',
            },
        ],
    },
    {
        path: '/admin/users',
        component: UserIndex,
        name: 'admin-users',
        children: [
            {
                path: ':user',
                components: {
                    modal: UserShow,
                },
                name: 'show-user',
            },
            {
                path: ':user/edit',
                components: {
                    modal: UserUpdate,
                },
                name: 'update-user',
            },
            {
                path: ':user/note',
                components: {
                    modal: UserUpdate,
                },
                name: 'user-note',
            },
        ],
    },
    {
        path: '/admin/schedule',
        component: ScheduleIndex,
        name: 'admin-schedule',
        children: [
            {
                path: 'duplicate/:fromSchedule',
                components: {
                    modal: ScheduleDuplicate,
                },
                name: 'duplicate-schedule',
            },
        ],
    },
    {
        path: '/admin/schedule/create',
        component: ScheduleForm,
        name: 'create-schedule',
    },
    {
        path: '/admin/schedule/:schedule',
        component: ScheduleForm,
        name: 'update-schedule',
        children: [
            {
                path: 'overrides/create',
                component: ScheduleForm,
                name: 'create-schedule-override',
            },
            {
                path: 'overrides/:override',
                component: ScheduleForm,
                name: 'update-schedule-override',
            },
        ],
    },
    {
        path: '/admin/slot-assignment',
        component: SlotAssignment,
        name: 'slot-assignment',
    },
    {
        path: '/config/trainings',
        component: TrainingIndex,
        name: 'admin-trainings',
        children: [
            {
                path: 'create',
                components: {
                    modal: TrainingCreate,
                },
                name: 'create-training',
            },
            {
                path: ':training/edit',
                components: {
                    modal: TrainingUpdate,
                },
                name: 'update-training',
            },
        ],
    },
    {
        path: '/config/leave-types',
        component: LeaveTypeIndex,
        name: 'admin-leave-types',
        children: [
            {
                path: 'create',
                components: {
                    modal: LeaveTypeCreate,
                },
                name: 'create-leaveType',
            },
            {
                path: ':leaveType/edit',
                components: {
                    modal: LeaveTypeUpdate,
                },
                name: 'update-leaveType',
            },
        ],
    },
    {
        path: '/config/:configGroup',
        component: Config,
        name: 'config',
    },
    {
        path: '/leaves',
        component: LeaveIndex,
        name: 'leaves',
        children: [
            {
                path: 'create',
                components: {
                    modal: LeaveCreate,
                },
                name: 'create-leave',
            },
            {
                path: ':leave/edit',
                components: {
                    modal: LeaveUpdate,
                },
                name: 'update-leave',
            },
        ],
    },
    {
        path: '/admin/key-codes',
        component: KeyCodeIndex,
        name: 'key-codes',
    },
    {
        path: '/admin/sent-emails',
        component: SentEmailIndex,
        name: 'sent-emails',
        children: [
            {
                path: ':sentEmail',
                components: {
                    modal: SentEmailShow,
                },
                name: 'show-sentEmail',
            },
        ],
    },
    {
        path: '/assignment-switch-offers',
        component: AssignmentSwitchOfferIndex,
        name: 'assignment-switch-offers',
        children: [
            {
                path: 'create',
                components: {
                    modal: AssignmentSwitchOfferCreate,
                },
                name: 'create-assignmentSwitchOffer',
            },
            {
                path: ':assignmentSwitchOffer/edit',
                components: {
                    modal: AssignmentSwitchOfferUpdate,
                },
                name: 'update-assignmentSwitchOffer',
            },
            {
                path: ':assignmentSwitchOffer/reply',
                components: {
                    modal: AssignmentSwitchOfferReply,
                },
                name: 'reply-assignmentSwitchOffer',
            },
            {
                path: ':assignmentSwitchOffer/check-replies',
                components: {
                    modal: AssignmentSwitchOfferCheckReplies,
                },
                name: 'check-replies-assignmentSwitchOffer',
            },
        ],
    },
    {
        path: '/profile',
        component: Profile,
        name: 'profile',
    },
    {
        path: '/badging',
        component: Badging,
        name: 'badging',
    },
    {
        path: '/rescue-team',
        component: RescueTeam,
        name: 'rescue-team',
    },
    {
        path: '/user-statuses',
        component: UserStatuses,
        name: 'user-statuses',
    },
    {
        path: '/my-team-attendance',
        component: MyTeamAttendance,
        name: 'my-team-attendance',
    },
    {
        path: '/admin/emergency-team-planning',
        component: EmergencyTeamPlanning,
        name: 'emergency-team-planning',
    },
    {
        path: '/emergency-numbers',
        component: EmergencyNumbers,
        name: 'emergency-numbers',
    },
    {
        path: '/admin/stats',
        component: Stats,
        name: 'stats',
    },
    {
        path: '/admin/attendance-monitoring',
        component: AttendanceMonitoring,
        name: 'attendance-monitoring',
    },
    {
        path: '/admin/technical-tools',
        component: TechnicalTools,
        name: 'technical-tools',
    },
    {
        path: '/admin/incomplete-switches',
        component: IncompleteSwitches,
        name: 'incomplete-switches',
    },
    {
        path: '/admin/slots-without-referent',
        component: SlotsWithoutReferent,
        name: 'slots-without-referent',
    },
    {
        path: '/admin/incomplete-slots',
        component: IncompleteSlots,
        name: 'incomplete-slots',
    },
    {
        path: '/admin/new-referents',
        component: NewReferents,
        name: 'new-referents',
    },
    {
        path: '/admin/ending-leaves',
        component: EndingLeaves,
        name: 'ending-leaves',
    },
    {
        path: '/error/:error',
        component: ErrorPage,
    },
    {
        path :'*',
        redirect: '/error/404',
    },
];

export default routes;