/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Setup vue-router
 */

import VueRouter from 'vue-router';
Vue.use(VueRouter);
const routes = require('./routes').default;
const router = new VueRouter({
    mode: 'history',
    routes,
});

/**
 * Setup vue-i18n (localisation)
 */
import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

Vue.use(VueInternationalization);

const lang = window.locale;

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale,
});

import Axios from './plugins/axios';
Vue.use(Axios);

import Lodash from './plugins/lodash';
Vue.use(Lodash);

import Cache from './plugins/cache';
Vue.use(Cache);

import EventBus from './plugins/event-bus';
Vue.use(EventBus);

import Moment from './plugins/moment';
Vue.use(Moment);

import Breadcrumbs from './plugins/breadcrumbs';
Vue.use(Breadcrumbs);

import SendUserMessage from './plugins/send-user-message';
Vue.use(SendUserMessage);

import Impersonate from './plugins/impersonate';
Vue.use(Impersonate);

import store from './store';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import vuetify from './plugins/vuetify';
import App from './App';

new Vue({
    router,
    vuetify,
    i18n,
    render: h => h(App),
    store,
}).$mount('#app');
