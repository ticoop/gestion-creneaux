import * as moment from 'moment';

export default {
    install(Vue, options) {
        moment.locale(window.locale);
        Vue.prototype.$moment = moment;
    },
}