import store from '../store';

export default {
    install(Vue, options) {
        Vue.prototype.$impersonate = function(user) {
            this.$store.dispatch('setBeforeImpersonateRoute', this.$route);
            this.$request.post(`/api/users/${user.id}/impersonate`).then((response) => {
                this.$router.push({name: 'home'});
                this.$store.dispatch('getUser');
            });
        };
        Vue.prototype.$stopImpersonating = function(user) {
            this.$request.post('/api/users/stop-impersonating').then(() => {
                const destRoute = !!this.$store.state.beforeImpersonateRoute ? this.$store.state.beforeImpersonateRoute : {name: 'home'};
                if (this.$route.name != destRoute.name) {
                    this.$router.push(destRoute);
                }
                this.$store.dispatch('getUser');
            });
        };
    },
}