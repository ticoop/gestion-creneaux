export default {
    install(Vue, options) {
        if (!!window.mattermostMessageUrl) {
            Vue.prototype.$sendUserMessage = (recipient) => {
                window.open(window.mattermostMessageUrl.replace('{username}', recipient.username), '_blank');
            };
        } else {
            Vue.prototype.$sendUserMessage = null;
        }
    },
}