import * as moment from 'moment';

export default {
    install(Vue, options) {
        Vue.prototype.$breadcrumbs = {
            breadcrumbs: [],
            callbacks: [],
            onUpdate(cb) {
                this.callbacks.push(cb);
                cb(this.breadcrumbs);
            },
            update(breadcrumbs) {
                this.breadcrumbs = breadcrumbs;
                this.callbacks.forEach((cb) => {
                    cb(breadcrumbs.map((b) => {
                        return {
                            exact: true,
                            ...b,
                        };
                    }));
                });
            },
        };

        Vue.mixin({
            beforeRouteEnter(to, from, next) {
                next((vm) => {
                    vm.$breadcrumbs.update(!!vm.getBreadcrumbs ? vm.getBreadcrumbs(to) : []);
                });
            },
            beforeRouteUpdate(to, from, next) {
                this.$breadcrumbs.update(!!this.getBreadcrumbs ? this.getBreadcrumbs(to) : []);
                next();
            },
        });
    },
}