export default {
    install(Vue, options) {
        Vue.prototype.$request = require('axios');
        Vue.prototype.$request.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        Vue.prototype.$request.interceptors.response.use(function (response) {
            // Any status code that lie within the range of 2xx cause this function to trigger
            // Do something with response data
            return response;
        }, function (error) {
            // Any status codes that falls outside the range of 2xx cause this function to trigger
            // Do something with response error
            if (!!error.response) {
                if (error.response.status === 401) {
                    Vue.prototype.$eventBus.emit('unauthenticated');
                } else {
                    Vue.prototype.$eventBus.emit('http-error', error.response.status);
                }
            }
            
            return Promise.reject(error);
        });
    },
}