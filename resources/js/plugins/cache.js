export default {
    install(Vue, options) {
        Vue.prototype.$cache = {
            promises: {},
            get(cacheKey, builder) {
                if (!this.promises[cacheKey]) {
                    this.promises[cacheKey] = new Promise(builder);
                }
                return this.promises[cacheKey];
            },
            forget(key) {
                this.promises[key] = null;
            },
            clear() {
                Object.keys(this.promises).forEach((key) => {
                    this.forget(key);
                });
            },
            getRoles() {
                return this.get('roles', (resolve, reject) => {
                    Vue.prototype.$request.get('/api/roles').then((response) => {
                        resolve(response.data.data);
                    }).catch(reject);
                });
            },
            getTrainings() {
                return this.get('trainings', (resolve, reject) => {
                    Vue.prototype.$request.get('/api/trainings').then((response) => {
                        resolve(response.data.data);
                    }).catch(reject);
                });
            },
            getLeaveTypes() {
                return this.get('leaveTypes', (resolve, reject) => {
                    Vue.prototype.$request.get('/api/leave-types').then((response) => {
                        resolve(response.data.data);
                    }).catch(reject);
                });
            },
            getConfig() {
                return this.get('config', (resolve, reject) => {
                    Vue.prototype.$request.get('/api/config').then((response) => {
                        resolve(response.data);
                    }).catch(reject);
                });
            },
            getConfigItem(key) {
                return this.getConfig().then((config) => {
                    if (Object.keys(config).indexOf(key) === -1) {
                        return null;
                    } else {
                        return config[key];
                    }
                });
            },
            getDayParts() {
                return this.get('dayParts', (resolve, reject) => {
                    Vue.prototype.$request.get('/api/day-parts').then((response) => {
                        resolve(response.data.data);
                    }).catch(reject);
                });
            },
        };
    },
}