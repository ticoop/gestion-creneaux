import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import fr from 'vuetify/lib/locale/fr';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'md',
        values: {
            clear: 'clear',
        },
    },
    lang: {
        locales: { fr },
        current: 'fr',
    },
    theme: {
        themes: {
            light: {
                primary: '#639999',
                warning: '#EE7052',
            },
        },
        options: {
            customProperties: true,
        },
    },
});