export default {
    install(Vue, options) {
        Vue.prototype.$eventBus = {
            callbacks: {},
            callbackId: 0,
            emit(event, data) {
                if (!!this.callbacks[event]) {
                    this.callbacks[event].forEach((cb) => {
                        cb['callback'](data);
                    });
                }
            },
            registerCallback(event, callback) {
                if (!this.callbacks[event]) {
                    this.callbacks[event] = [];
                }
                const cb = {
                    callback,
                    id: ++this.callbackId,
                }
                this.callbacks[event].push(cb);
                return cb.id;
            },
            unregisterCallback(id) {
                Object.keys(this.callbacks).forEach((event) => {
                    this.callbacks[event] = this.callbacks[event].filter((cb) => cb.id !== id);
                });
            },
        };
    },
}