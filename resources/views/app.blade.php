<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ __('global.shop-name') . ' - ' . __('global.app-name') }}</title>

        <link rel="shortcut icon" href="/favicon.png" type="image/png">

        <!-- Fonts -->
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        <script type="text/javascript">
            window.timezone = "{{ config('app.timezone') }}";
            window.locale = "{{ app()->getLocale() }}";
            @if (config('app.mattermost_message_url'))
                window.mattermostMessageUrl = "{{ config('app.mattermost_message_url') }}";
            @endif
        </script>
    </head>
    <body>
        <div id="app"></div>

        <script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
