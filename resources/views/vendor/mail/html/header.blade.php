<tr>
    <td class="header" align="center">
        <table>
            <tr>
                <td class="logo">
                    <img src="{{ asset('logo.png') }}" width="125">
                </td>
                <td class="title">
                    {{ $slot }}
                </td>
            </tr>
        </table>
    </td>
</tr>
