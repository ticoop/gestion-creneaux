<?php

return [
    'view-team' => 'Voir l\'équipe',
    'team' => 'Équipe',
    'attended' => 'Effectué',
    'future-assignments' => 'Créneaux à venir',
    'past-assignments' => 'Créneaux passés',
];
