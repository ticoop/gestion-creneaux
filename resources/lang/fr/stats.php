<?php

return [
    'assignment-switches' => [
        'title' => 'Échanges de créneaux',
        'nb-switches' => 'Nombre d\'échanges',
        'nb-users' => 'Nombre de membres concerné·e·s',
        'stats-chart-title' => 'Historique',
        'chart-serie-label' => [
            'count' => 'Nombre d\'échanges',
            'nb_users' => 'Nombre de membres concerné·e·s',
        ],
    ],
    'users' => [
        'title' => 'Membres',
        'nb-active' => 'Actifs',
        'nb-assigned' => 'Affectés à un créneau cyclique',
        'nb-flying' => 'Équipe volante',
        'status' => 'Statut',
        'chart-serie-label' => [
            'active_count' => 'Actifs',
            'flying_count' => 'Équipe volante',
            'assigned_count' => 'Affectés à un créneau cyclique',
            'by_status' => [
                'up-to-date' => 'À jour',
                'alert' => 'Alerte',
                'suspended' => 'Suspendu',
                'blocked' => 'Désinscrit',
                'inactive' => 'Inactif',
            ],
        ],
        'stats-chart-title' => 'Historique',
    ],
    'members-age' => [
        'title' => 'Âge des membres',
        'average-age' => 'Âge moyen',
    ],
    'members-gender' => [
        'title' => 'Genre des membres',
    ],
    'attendance' => [
        'title' => 'Taux de présence',
        'chart-serie-label' => [
            'percentage' => 'Taux de présence',
        ],
    ],
];
