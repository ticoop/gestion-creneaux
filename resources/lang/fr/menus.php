<?php

return [
    'main' => [
        'home' => 'Accueil',
        'my-schedule' => 'Mon planning',
        'schedule' => 'Planning complet',
        'leaves' => 'Congés',
        'assignment-switch-offers' => 'Échanges de créneaux',
        'profile' => 'Mon profil',
        'rescue-team' => 'Équipe secours',
        'help' => 'Aide',
        'user-statuses' => 'Statut des membres',
        'emergency-numbers' => 'Numéros d\'urgence',
        'my-team-attendance' => 'Présence de mon équipe',
    ],
    'admin' => [
        'schedule' => 'Gestion du planning',
        'users' => 'Membres',
        'slot-assignment' => 'Affectation aux créneaux',
        'key-codes' => 'Codes boîte à clé',
        'sent-emails' => 'Emails envoyés',
        'emergency-team-planning' => 'Numéros d\'urgence',
        'stats' => 'Statistiques',
        'attendance-monitoring' => 'Suivi de présence',
        'technical-tools' => 'Outils techniques',
        'incomplete-switches' => 'Échanges non aboutis',
        'slots-without-referent' => 'Créneaux sans référent',
        'incomplete-slots' => 'Créneaux incomplets',
        'new-referents' => 'Nouveaux référents',
        'ending-leaves' => 'Congés en fin de droit',
    ],
    'config' => [
        'trainings' => 'Formations',
        'leave-types' => 'Types de congés',
    ],
];
