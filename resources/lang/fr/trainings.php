<?php

return [
    'label' => 'Libellé',
    'create-modal-title' => 'Création formation',
    'update-modal-title' => 'Édition formation',
    'delete-modal-title' => 'Suppression formation',
    'confirm-delete' => 'Confirmer la suppression de cette formation ?',
];
