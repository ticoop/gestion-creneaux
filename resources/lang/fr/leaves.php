<?php

return [
    'show-passed' => 'Afficher les congés passés',
    'leave-type' => 'Type',
    'start-at' => 'Date début',
    'end-at' => 'Date fin',
    'confirm-delete' => 'Confirmer la suppression de ce congé ?',
    'create-modal-title' => 'Création congé',
    'update-modal-title' => 'Édition congé',
    'comment' => 'Commentaire',
    'cannot-modify-start-date-of-started-leave' => 'Impossible de modifier la date de début d\'un congé commencé.',
    'cannot-modify-end-date-of-ended-leave' => 'Impossible de modifier la date de fin d\'un congé terminé.',
    'cannot-modify-leave-type-of-ended-leave' => 'Impossible de modifier le type d\'un congé commencé.',
    'user' => 'Membre',
];
