<?php

return [
    'id' => 'Id',
    'username' => 'Nom d\'utilisat·eur·rice',
    'full-name' => 'Nom',
    'email' => 'Email',
    'phone' => 'Téléphone',
    'edit-user' => 'Édition membre',
    'role' => 'Rôle',
    'roles' => 'Rôles',
    'update-modal-title' => 'Édition membre : :username',
    'note-modal-title' => 'Édition note : :username',
    'trainings-received' => 'Formations reçues',
    'referent' => 'Référent·e',
    'referents' => 'Référent·e·s',
    'flying-team' => 'Équipe volante',
    'start-date' => 'Date de début',
    'end-date' => 'Date de fin',
    'calendar-event-title' => config('app.shop_name') . ' - Créneau de travail',
    'calendar-title' => config('app.shop_name') . ' - Créneaux de travail',
    'add-calendar-to-phone' => 'Ajouter à mon téléphone',
    'rescue-days' => 'Jours de disponibilité pour l\'équipe secours',
    'receive-shift-email-reminders' => 'Recevoir un email de rappel la veille de mes créneaux',
    'member-status' => 'Statut',
    'next-assignment' => 'Prochain créneau',
    'no-next-assignment' => 'Aucun',
    'begin-shift' => 'Commencer le créneau',
    'status' => 'Statut',
    'status-up-to-date' => 'À jour',
    'status-alert' => 'Alerte',
    'status-suspended' => 'Suspendu·e',
    'status-blocked' => 'Désinscrit·e (trop de créneaux manqués)',
    'status-inactive' => 'Inactif·ve',
    'status-leave-no-shopping' => 'Gel de créneaux',
    'status-unsubscribed' => 'A quitté la coop',
    'allowed-to-shop' => 'Autorisé à faire ses courses',
    'assignment-started' => 'Créneau de travail commencé',
    'failed-starting-assignment' => 'Erreur lors du lancement du créneau',
    'impersonate' => 'Se connecter',
    'stop-impersonating' => 'Retour à mon compte',
    'compensations-to-perform' => 'Rattrapages à effectuer',
    'compensation-to-perform-description' => ':total rattrapage(s) à effectuer avant le :limit_date : :unscheduled à programmer, :scheduled déjà programmé(s)',
    'allow-phone-number-sharing' => 'Autoriser le partage de mon numéro de téléphone avec les autres coopérateur·rice·s',
    'accounting-team' => 'Équipe compta',
    'admin-leaves' => 'Gestion des congés',
    'service-account' => 'Compte de service',
    'rescue-days-hint' => 'Pour pouvoir être contacté le plus facilement possible en cas de besoin, il est préférable de cocher la case "Autoriser le partage de mon numéro de téléphone avec les autres coopérateur·rice·s" si vous participez à l\'équipe secours.',
    'send-message' => 'Envoyer un message',
    'assigned-slot' => 'Créneau',
    'no-next-assignment' => 'Pas de prochain créneau prévu',
    'general-informations' => 'Informations générales',
    'slots' => 'Créneaux',
    'switches' => 'Échanges',
    'without-assignment' => 'Sans créneau affecté',
    'flying-without-assignments' => 'Volant·e n\'ayant effectué aucun créneau',
    'note' => 'Note',
    'export-notes' => 'Exporter les notes',
    'leaves' => 'Congés',
    'exemption' => 'Dispense',
    'subscription-date' => 'Date d\'inscription',
    'switch-offers-without-reply' => 'Échanges non aboutis',
];
