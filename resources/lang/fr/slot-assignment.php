<?php

return [
    'selected-schedule' => 'Planning sélectionné',
    'main-schedule' => 'Planning principal',
    'week' => 'Semaine',
    'referents' => 'Référent·e·s',
    'others' => 'Bénévoles',
    'update-assignment' => 'Modification d\'une affectation',
    'cannot-assign-a-user-twice-on-same-slot' => 'Impossible d\'affecter une personne 2 fois au même créneau.',
    'unassigned-count' => ':count places',
    'confirm-add-duplicate' => '<p>Cette personne est déjà affectée au créneau suivant :<br>:conflictSlot</p>' .
        '<p>Confirmer l\'ajout d\'un créneau supplémentaire ?</p>',
    'you-are-already-assigned-to-that-slot' => 'Vous êtes déjà affecté·e à ce créneau.',
    'assign-myself-to-slot' => 'M\'affecter à ce créneau',
    'unassign-myself-from-slot' => 'Me retirer de ce créneau',
    'confirm-assign-to-slot' => 'Confirmer l\'affectation à ce créneau ?',
    'confirm-unassign-from-slot' => 'Confirmer le retrait de ce créneau ?',
    'too-many-flying-assignments-that-slot' => 'Le nombre maximal de personnes de l\'équipe volante a été atteint pour ce créneau.',
    'assignment-done' => 'Affectation effectuée.',
    'unassignment-done' => 'Retrait effectué.',
    'add-as-referent' => 'Ajouter référent',
    'remove-referent' => 'Retirer référent',
    'not-in-accounting-team' => 'Vous ne faites pas partie de l\'équipe compta',
    'add-additional-user' => 'Ajouter un·e bénévole',
    'remove-user' => 'Retirer bénévole',
    'confirm-remove-user' => 'Retirer ce·tte bénévole du créneau ?',
    'switch-offered-info' => 'Demande d\'échange en attente',
];
