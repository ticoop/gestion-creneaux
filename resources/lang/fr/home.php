<?php

return [
    'my-assignments' => 'Mes créneaux',
    'assigned-slot' => 'Créneau affecté',
    'assigned-slots' => 'Créneaux affectés',
    'no-assigned-slot' => 'Aucun créneau affecté',
    'see-my-schedule' => 'Voir mon planning complet',
    'my-status' => 'Mon statut',
    'schedule-compensations' => 'Programmer mes rattrapages',
    'possible-assignment-switches' => 'Échanges de créneaux possibles',
    'switch-assigngment' => 'Échanger un créneau',
    'opening-key-code' => 'Code boîte à clé ouverture',
    'closing-key-code' => 'Code boîte à clé fermeture',
    'flying-team-switch-info' => 'Avant de programmer un créneau, pensez à regarder dans la liste des échanges proposés (voir ci-dessous ou sur la page "Échanges de créneaux"). Vous trouverez peut-être un créneau qui vous intéresse et vous faciliterez la vie d\'un·e autre membre par la même occasion !',
    'schedule-assignment' => 'M\'affecter à un créneau',
];
