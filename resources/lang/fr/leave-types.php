<?php

return [
    'label' => 'Libellé',
    'confirm-delete' => 'Confirmer la suppression de ce type de congés ?',
    'create-modal-title' => 'Création type de congés',
    'update-modal-title' => 'Édition type de congés',
    'shopping-allowed' => 'Courses autorisées',
    'min-duration' => 'Durée minimale',
    'min-duration-hint' => 'En semaines',
    'min-notice' => 'Délai minimal d\'annonce',
    'min-notice-hint' => 'Nombre minimal de semaines en avance pour l\'annonce du congé',
    'requires-admin' => 'Création par les admins seulement',
];
