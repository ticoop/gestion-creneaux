<?php

return [
    'recipient' => 'Destinataire',
    'subject' => 'Objet',
    'opens' => 'Ouvertures',
    'clicks' => 'Clics',
    'sent-at' => 'Date d\'envoi',
    'view' => 'Voir',
    'content' => 'Contenu',
    'show-modal-title' => 'Email envoyé',
];
