<?php

return [
    'mattermost-message' => 'Envoyer un message Mattermost',
    'suggested-user' => 'Membre à contacter',
    'get-another-user' => 'Rechercher un autre membre',
    'no-user-available' => 'Aucun membre disponible pour l\'équipe secours !',
    'now' => 'Maintenant',
    'next-assignment' => 'Mon prochain créneau',
    'period' => 'Afficher l\'équipe secours pour',
];
