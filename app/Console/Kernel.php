<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('generate-actual-schedule')
            ->everyMinute()
            ->withoutOverlapping()
            ->onOneServer();
        
        $schedule->command('generate-key-codes')
            ->dailyAt('01:00')
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command('reminders:shifts')
            ->dailyAt('16:00')
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command('reminders:shifts --weekly')
            ->dailyAt('16:00')
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command('import-keycloak-users')
            ->dailyAt('03:00')
            ->withoutOverlapping()
            ->onOneServer();
        
        if (!!config('dolibarr.server')) {
            $schedule->command('dolibarr:fetch-data')
                ->hourly()
                ->withoutOverlapping()
                ->onOneServer();
            $schedule->command('dolibarr:fetch-data --all --ignore-documents')
                ->dailyAt('05:00')
                ->withoutOverlapping()
                ->onOneServer();
            $schedule->command('dolibarr:fetch-data --action cleanup')
                ->dailyAt('04:00')
                ->withoutOverlapping()
                ->onOneServer();
        }

        $schedule->command('mark-assignments-not-done')
            ->dailyAt('02:00')
            ->withoutOverlapping()
            ->onOneServer();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return config('app.timezone');
    }
}
