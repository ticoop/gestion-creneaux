<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ShopSchedule;
use App\Assignment;
use App\AssignmentWish;

class ProcessAssignmentWishes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wishes:process {scheduleId} {--store}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process assignment wishes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scheduleId = $this->argument('scheduleId');
        $schedule = ShopSchedule::with('slots')->findOrfail($scheduleId);

        $wishes = $schedule->assignmentWishes()
            ->where('flying_team', false)
            ->with(['user', 'slots.assignments.user'])
            ->get();
        
        $referentWishes = $wishes->filter(function ($w) {
            return $w->referent;
        });
        $referentAssignments = $this->assign($schedule, $referentWishes, 'referents');

        $assignedUsers = collect($referentAssignments)->reduce(function ($prev, $a) {
            return $prev->merge($a['referents']);
        }, collect())->pluck('id');
        $otherWishes = $wishes->filter(function ($w) use ($assignedUsers) {
            return !$assignedUsers->contains($w->user->id);
        });
        $otherAssignments = $this->assign($schedule, $otherWishes);

        $assignedUsers = $assignedUsers->merge(collect($otherAssignments)->reduce(function ($prev, $a) {
            return $prev->merge($a['others']);
        }, collect())->pluck('id'));
        $unassignedUsers = $wishes
            ->map(function ($w) {
                return $w->user;
            })
            ->unique('id')
            ->filter(function ($u) use ($assignedUsers) {
                return !$assignedUsers->contains($u->id);
            });
        
        $finalized = $this->finalize($schedule, $referentAssignments, $otherAssignments, $unassignedUsers);
        
        if ($this->option('store')) {
            $this->store($finalized);
        } else {
            $this->csvOutput($finalized, $unassignedUsers);
        }
    }

    protected function store($assignments)
    {
        $finalAssignments = [];
        foreach ($assignments as $entry) {
            foreach ($entry['assignments'] as $weekIndex => $weekAssignments) {
                if (isset($weekAssignments['referent'])) {
                    $finalAssignments[] = [
                        'user' => $weekAssignments['referent'],
                        'slot' => $entry['slot'],
                        'week_index' => $weekIndex,
                        'referent' => true,
                    ];
                }
                foreach ($weekAssignments['others'] as $user) {
                    $finalAssignments[] = [
                        'user' => $user,
                        'slot' => $entry['slot'],
                        'week_index' => $weekIndex,
                        'referent' => false,
                    ];
                }
            }
        }
        
        foreach ($finalAssignments as $assignment) {
            if ($assignment['user']->assignments->count() == 0) {
                Assignment::create([
                    'user_id' => $assignment['user']->id,
                    'shop_schedule_slot_id' => $assignment['slot']->id,
                    'week_index' => $assignment['week_index'],
                    'referent' => $assignment['referent'],
                ]);
            }
        }
    }

    protected function finalize($schedule, $referentAssignments, $otherAssignments, $unassignedUsers)
    {
        $slots = $schedule->slots->sort(function ($a, $b) {
            if ($a->weekday == $b->weekday) {
                return strcasecmp($a->start_at, $b->start_at);
            } else {
                return (($a->weekday + 6) % 7) - (($b->weekday + 6) % 7);
            }
        });

        $data = [];

        foreach ($slots as $slot) {
            $entry = [
                'slot' => $slot,
                'assignments' => [
                    ['referent' => null, 'others' => []],
                    ['referent' => null, 'others' => []],
                    ['referent' => null, 'others' => []],
                    ['referent' => null, 'others' => []],
                ],
            ];

            $referents = isset($referentAssignments[$slot->id]) ? $referentAssignments[$slot->id]['referents'] : [];
            $others = isset($otherAssignments[$slot->id]) ? $otherAssignments[$slot->id]['others'] : [];

            foreach ($slot->assignments as $assignment) {
                if ($assignment->referent) {
                    $entry['assignments'][$assignment->week_index]['referent'] = $assignment->user;
                } else {
                    $entry['assignments'][$assignment->week_index]['others'][] = $assignment->user;
                }
            }

            $assignmentMap = $slot->assignments->pluck('week_index', 'user_id');

            $referents = collect($referents)->filter(function ($u) use ($assignmentMap) {
                return !$assignmentMap->has($u->id);
            });
            $others = collect($others)->filter(function ($u) use ($assignmentMap) {
                return !$assignmentMap->has($u->id);
            });

            foreach ($entry['assignments'] as $i => $weekAssignments) {
                if ($weekAssignments['referent'] === null) {
                    $entry['assignments'][$i]['referent'] = $referents->shift();
                }
            }

            while ($others->count() > 0) {
                $minCount = collect($entry['assignments'])->map(function ($a) {
                    return count($a['others']);
                })->min();
                foreach ($entry['assignments'] as $i => $weekAssignments) {
                    if (count($weekAssignments['others']) == $minCount) {
                        $entry['assignments'][$i]['others'][] = $others->shift();
                        break;
                    }
                }
            }

            $data[] = $entry;
        }

        return $data;
    }

    protected function csvOutput($data, $unassignedUsers)
    {
        $filename = storage_path("auto_assignment.csv");
        $fp = fopen($filename, 'w');

        fputcsv($fp, ['', 'Semaine A', 'Semaine B', 'Semaine C', 'Semaine D']);

        foreach ($data as $entry) {
            $row = [__('global.weekday-' . $entry['slot']->weekday) . ' ' . substr($entry['slot']->start_at, 0, 5)];
            foreach ($entry['assignments'] as $weekAssignment) {
                $lines = [];
                if (isset($weekAssignment['referent'])) {
                    $lines[] = $weekAssignment['referent']->fullname . " (Référent)";
                }
                $lines = array_merge($lines, collect($weekAssignment['others'])->map(function ($u) {
                    return $u->fullname;
                })->toArray());
                $row[] = implode("\n", $lines);
            }

            fputcsv($fp, $row);
        }

        fputcsv($fp, ['', '', '', '', '']);
        fputcsv($fp, ['', '', '', '', '']);
        
        fputcsv($fp, ['Non affectés', $unassignedUsers->map(function ($u) {
            return $u->fullname;
        })->implode("\n"), '', '', '']);

        fclose($fp);
    }

    protected function assign($schedule, $wishes, $assignmentType = 'others')
    {
        $assignments = [];

        foreach ($schedule->slots as $slot) {
            if (!isset($assignments[$slot->id])) {
                $assignments[$slot->id] = [
                    'referents' =>[],
                    'others' =>[],
                ];
            }
            foreach ($slot->assignments as $assignment) {
                if ($assignment->referent) {
                    $assignments[$slot->id]['referents'][] = $assignment->user;
                } else {
                    $assignments[$slot->id]['others'][] = $assignment->user;
                }
            }
        }

        $distribution = $this->distribute($wishes);
        $distribution = $distribution
            ->map(function ($d) use ($assignments, $assignmentType) {
                $d['wishes'] = $d['wishes']->filter(function ($w) use ($assignments, $assignmentType) {
                    return !collect($assignments)
                        ->map(function ($a) use ($assignmentType) {
                            return $a[$assignmentType];
                        })
                        ->reduce(function ($prev, $a) {
                            return $prev->merge($a);
                        }, collect())
                        ->pluck('id')
                        ->contains($w->user->id);
                });
                return $d;
            })
            ->filter(function ($d) use ($slot, $assignments, $assignmentType) {
                $limit = ($assignmentType == 'others' ? ($slot->shop_open ? 4 : 1) : 1) * 4;
                return count($d['wishes']) > 0 &&
                    (!isset($assignments[$d['slot']->id]) ||
                        count($assignments[$d['slot']->id][$assignmentType]) < $limit
                    );
            });

        while ($distribution->count() > 0) {
            $distribution->sort(function ($a, $b) {
                return count($b['wishes']) - count($a['wishes']);
            });
            $slot = $distribution->first()['slot'];
            $user = $distribution->first()['wishes']->first()->user;
            
            $assignments[$slot->id][$assignmentType][] = $user;
            $distribution = $distribution
                ->map(function ($d) use ($user) {
                    $d['wishes'] = $d['wishes']->filter(function ($w) use ($user) {
                        return $w->user->id != $user->id;
                    });
                    return $d;
                })
                ->filter(function ($d) use ($slot, $assignments, $assignmentType) {
                    $limit = ($assignmentType == 'others' ? ($slot->shop_open ? 4 : 1) : 1) * 4;
                    return count($d['wishes']) > 0 &&
                        (!isset($assignments[$d['slot']->id]) ||
                            count($assignments[$d['slot']->id][$assignmentType]) < $limit
                        );
                });
        }
        return $assignments;
    }

    protected function distribute(&$wishes)
    {
        $res = [];
        foreach ($wishes as $wish) {
            foreach ($wish->slots as $slot) {
                if (!array_key_exists($slot->id, $res)) {
                    $res[$slot->id] = [
                        'slot' => $slot,
                        'wishes' => collect(),
                    ];
                }
                $res[$slot->id]['wishes']->push($wish);
            }
        }
        return collect($res);
    }
}
