<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ExportDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '1G');
        
        $tables = collect(DB::select("
            SELECT *
            FROM pg_catalog.pg_tables
            WHERE schemaname != 'pg_catalog'
                AND schemaname != 'information_schema'
        "))
            ->map(function ($i) {
                return $i->tablename;
            })
            ->filter(function ($table) {
                return $table != 'migrations' && substr($table, 0, 6) != 'oauth_';
            });
        if (!file_exists(storage_path('db-export'))) {
            mkdir(storage_path('db-export'));
        }
        $order = [
            'genders',
            'users',
            'shop_schedules',
            'shop_schedule_slots',
            'assignments',
            'assignment_switch_offers',
            'assignment_switch_offer_replies',
            'assignment_switches',
            'config_groups',
            'leave_types',
            'usage_events',
            'jobs',
            'sent_emails',
            'roles',
            'trainings',
            'assignment_wishes',
            'day_parts',
        ];
        foreach ($tables as $i => $table) {
            if (($index = array_search($table, $order)) === false) {
                $index = count($order) + $i;
            }
            $index = str_pad($index, 3, '0', STR_PAD_LEFT);
            $data = DB::select('SELECT * FROM ' . $table);
            file_put_contents(storage_path('db-export/' . $index . '_' . $table . '.json'), json_encode($data));
        }
    }
}
