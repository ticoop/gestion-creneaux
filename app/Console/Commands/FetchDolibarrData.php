<?php

namespace App\Console\Commands;

use App\Gender;
use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class FetchDolibarrData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dolibarr:fetch-data {--all} {--ignore-documents} {--action=import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch data from Dolibarr server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = app()->make('DolibarrClient');
        
        switch ($this->option('action')) {
            case 'import':
                $genderMap = Gender::all()->pluck('id', 'dolibarr_code')->toArray();
                $members = collect($client->listMembers())
                    ->map(function ($m) use ($genderMap) {
                        $m['date_modification'] = Carbon::createFromTimestamp($m['date_modification']);
                        if (isset($m['first_subscription_date_start'])) {
                            $m['first_subscription_date_start'] =
                                Carbon::createFromTimestamp($m['first_subscription_date_start']);
                        }
                        if (isset($m['last_subscription_date_end'])) {
                            $m['last_subscription_date_end'] =
                                Carbon::createFromTimestamp($m['last_subscription_date_end']);
                        }
                        if (isset($m['birth'])) {
                            $m['birth'] = Carbon::createFromTimestamp($m['birth']);
                        } else {
                            $m['birth'] = null;
                        }
                        if (array_key_exists($m['gender'], $genderMap)) {
                            $m['gender_id'] = $genderMap[$m['gender']];
                        } else {
                            $m['gender_id'] = null;
                        }
                        return $m;
                    })
                    ->filter(function ($m) {
                        if ($this->option('all')) {
                            return true;//$m['login'] == 'gwendal.lebihan';
                        } else {
                            return $m['date_modification']->gt(now()->subDay()->startOfDay());
                        }
                    });

                $users = User::with('media')->get();
                $userMap = [];
                foreach ($users as $user) {
                    $userMap[$user->username] = $user;
                }

                $minStartDate = config('app.min_user_start_date');
                if (!!$minStartDate) {
                    $minStartDate = Carbon::parse($minStartDate);
                }
                
                foreach ($members as $member) {
                    if (isset($userMap[$member['login']])) {
                        if (!$this->option('ignore-documents')) {
                            if (isset($member['photo'])) {
                                $photoPath = $member['id'] . '/photos/' . $member['photo'];
                                try {
                                    $photoData = $client->downloadDocument('memberphoto', $photoPath);
                                } catch (\Exception $e) {
                                    $photoData = null;
                                }
                                if (!!$photoData) {
                                    $userMap[$member['login']]->clearMediaCollection('photo');
                                    $userMap[$member['login']]->addMediaFromBase64($photoData['content'])
                                        ->toMediaCollection('photo');
                                }
                            }
                        }

                        if (isset($member['phone_perso'])) {
                            $userMap[$member['login']]->phone = $member['phone_perso'];
                        } elseif (isset($member['phone'])) {
                            $userMap[$member['login']]->phone = $member['phone'];
                        } elseif (isset($member['phone_mobile'])) {
                            $userMap[$member['login']]->phone = $member['phone_mobile'];
                        }

                        if (isset($member['first_subscription_date_start'])) {
                            $userMap[$member['login']]->subscription_date = $member['first_subscription_date_start'];
                        }

                        if (isset($member['first_subscription_date_start']) && !$userMap[$member['login']]->start_date) {
                            if (!!$minStartDate && $member['last_subscription_date_end']->lt($minStartDate)) {
                                $userMap[$member['login']]->start_date = $minStartDate;
                            } else {
                                $userMap[$member['login']]->start_date = $member['first_subscription_date_start'];
                            }
                        }
                        if (isset($member['last_subscription_date_end'])) {
                            $userMap[$member['login']]->end_date = $member['last_subscription_date_end'];
                        }

                        $userMap[$member['login']]->birth_date = $member['birth'];
                        $userMap[$member['login']]->gender_id = $member['gender_id'];

                        if (isset($member['array_options']) && isset($member['array_options']['options_usedfirstname'])) {
                            $userMap[$member['login']]->used_firstname =
                                $member['array_options']['options_usedfirstname'];
                        } else {
                            $userMap[$member['login']]->used_firstname = null;
                        }

                        $userMap[$member['login']]->updateSearchText();
                        $userMap[$member['login']]->save();
                    }
                }

                Cache::flush();
                break;

            case 'cleanup':
                $existingLogins = collect($client->listMembers())->pluck('login');
                
                User::whereNotIn('username', $existingLogins)
                    ->where('service_account', false)
                    ->doesntHave('assignments')
                    ->doesntHave('actualAssignments')
                    ->delete();
                
                Cache::flush();
                break;

            default:
                $this->error('Unknown action: ' . $this->option('action'));
                break;
        }

        User::whereNotNull('end_date')
            ->where('end_date', '<', now()->startOfDay())
            ->has('assignments')
            ->get()
            ->each(function ($user) {
                $user->assignments->each(function ($assignment) {
                    $assignment->delete();
                });
            });
    }
}
