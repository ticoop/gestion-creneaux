<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ActualAssignment;
use App\Notifications\ShiftReminder;

class SendShiftReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminders:shifts {--weekly}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send shift reminders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('weekly')) {
            $assignmentDate = now()->addDays(7);
        } else {
            $assignmentDate = now()->addDay();
        }

        $this->call(GenerateKeyCodes::class);

        $assignments = ActualAssignment::whereDate('date', $assignmentDate)
            ->with('user')
            ->whereHas('user', function ($q) {
                $q->where('shift_email_reminders', true);
            })
            ->get();
        foreach ($assignments as $assignment) {
            $assignment->user->notify(new ShiftReminder($assignment, $this->option('weekly')));
        }
    }
}
