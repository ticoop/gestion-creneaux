<?php

namespace App\Console\Commands;

use App\ActualAssignment;
use App\ShopSchedule;
use Illuminate\Console\Command;

class CleanupOverridenActualAssignments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup-overriden-actual-assignments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schedules = ShopSchedule::doesntHave('parent')->get();
        $actualAssignments = collect();
        foreach ($schedules as $schedule) {
            $computed = $schedule->computeSlotsByDay($schedule->start_at, $schedule->end_at);
            foreach ($computed as $date => $slots) {
                $actualAssignments = $actualAssignments->merge($slots->map(function ($s) use ($date) {
                    return $s->actualAssignments->filter(function ($a) use ($date) {
                        return $a->date_string == $date;
                    })->pluck('id');
                })->reduce(function ($a, $b) {
                    return $a->merge($b);
                }, collect()));
            }
        }
        ActualAssignment::all()->filter(function ($a) use ($actualAssignments) {
            return !$actualAssignments->contains($a->id);
        })->each(function ($a) {
            $a->delete();
        });
    }
}
