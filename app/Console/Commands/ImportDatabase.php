<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\Console\Helper\ProgressBar;
use ZipArchive;

class ImportDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import {--remote}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function recRmdir($path)
    {
        $files = scandir($path);
        foreach ($files as $f) {
            if ($f != '.' && $f != '..') {
                $fullPath = $path . '/' . $f;
                if (is_dir($fullPath)) {
                    $this->recRmdir($fullPath);
                } else {
                    unlink($fullPath);
                }
            }
        }
        rmdir($path);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '1G');

        if ($this->option('remote')) {
            $url = config('database.import-url');
            $token = config('database.import-token');
            if (!$url) {
                $this->error('Missing DB_IMPORT_URL');
                return;
            }
            if (!$token) {
                $this->error('Missing DB_IMPORT_TOKEN');
                return;
            }
            
            $client = new GuzzleClient();
            $response = $client->request('GET', $url . '/api/export-database', [
                'allow_redirects' => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
            ]);
            if ($response->getStatusCode() == 200) {
                if (file_exists(storage_path('db-import'))) {
                    $this->recRmdir(storage_path('db-import'));
                }
                mkdir(storage_path('db-import'));
                $filename = tempnam(sys_get_temp_dir(), '');
                file_put_contents($filename, $response->getBody());
                $zip = new ZipArchive;
                $zip->open($filename);
                $zip->extractTo(storage_path('db-import'));
                $zip->close();
                unlink($filename);
            } else {
                $this->error('Failed retrieving database export from remote!');
                return;
            }
        }

        $imports = collect(glob(storage_path('db-import/*.json')))->map(function ($path) {
            return explode('.', basename($path))[0];
        })->sort()->map(function ($item) {
            return [
                'filename' => storage_path('db-import/' . $item . '.json'),
                'table' => explode('_', $item, 2)[1],
            ];
        })->filter(function ($item) {
            return substr($item['table'], 0, 6) != 'oauth_';
        });

        DB::transaction(function () use ($imports) {
            foreach ($imports as $import) {
                DB::statement("DELETE FROM " . $import['table']);
            }

            ProgressBar::setFormatDefinition('custom', ' %current%/%max% %bar% %percent%% -- %message%');
            $bar = $this->output->createProgressBar($imports->count());
            $bar->setFormat('custom');
            $bar->start();
            while ($imports->count() > 0) {
                $item = $imports->shift();
                $bar->setMessage("Importing " . $item['table'] . "...");
                $data = json_decode(file_get_contents($item['filename']), true);
                if ($item['table'] == 'actual_assignments') {
                    usort($data, function ($a, $b) {
                        return $a['compensates_actual_assignment_id'] - $b['compensates_actual_assignment_id'];
                    });
                }
                foreach (array_chunk($data, 1000) as $chunk) {
                    DB::table($item['table'])->insert($chunk);
                }
                $bar->advance();
            }
            $bar->finish();
        });

        $this->fixAutoincrement();
        $this->fixLocalOauthId();
        
        Cache::flush();
    }

    protected function fixAutoincrement()
    {
        $tables = collect(DB::select("
            SELECT *
            FROM pg_catalog.pg_tables
            WHERE schemaname != 'pg_catalog'
                AND schemaname != 'information_schema'
        "))
            ->map(function ($i) {
                return $i->tablename;
            })
            ->filter(function ($table) {
                return $table != 'migrations' && substr($table, 0, 6) != 'oauth_';
            });
        foreach ($tables as $table) {
            try {
                $maxId = DB::select('SELECT MAX(id) FROM ' . $table)[0]->max;
            } catch (\Exception $e) {
                continue;
            }
            if ($maxId !== null) {
                DB::statement("ALTER SEQUENCE " . $table . "_id_seq RESTART WITH " . ($maxId + 1));
            }
        }
    }

    protected function fixLocalOauthId()
    {
        if (file_exists(storage_path('local_oauth_id.json'))) {
            $data = json_decode(file_get_contents(storage_path('local_oauth_id.json')), true);
            foreach ($data as $username => $oauthId) {
                User::where('username', $username)->first()->update(['oauth_id' => $oauthId]);
            }
        }
    }
}
