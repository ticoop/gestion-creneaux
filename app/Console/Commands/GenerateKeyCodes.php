<?php

namespace App\Console\Commands;

use App\DayKeyCode;
use Illuminate\Console\Command;
use App\ShopSchedule;

class GenerateKeyCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-key-codes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate key codes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = now()->addDay()->startOfDay();

        $schedule = ShopSchedule::whereNull('override_shop_schedule_id')
            ->where('start_at', '<=', $date)
            ->where('end_at', '>=', $date)
            ->first();
        $slots = $schedule->computeSlotsByDay($date, $date)[$date->format('Y-m-d')]
            ->filter(function ($slot) {
                return $slot->generate_key_code;
            });
        if ($slots->count() > 0) {
            $existingCodes = DayKeyCode::where('date', $date)->first();
            if (!$existingCodes) {
                $prevCode = DayKeyCode::orderBy('date', 'desc')->first();
                $prevCode = !!$prevCode ? $prevCode->closing_code : config('key-codes.default-code');

                DayKeyCode::create([
                    'date' => $date,
                    'opening_code' => $prevCode,
                    'closing_code' => DayKeyCode::generateCode(),
                ]);
            }
        }
    }
}
