<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client as GuzzleClient;
use App\User;

class ImportKeyclockUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-keycloak-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import full user list from Keycloak';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $guzzleClient = new GuzzleClient();

        $baseUrl = config('services.ticoop_auth.base_url');
        $realm = config('services.ticoop_auth.realm');
        $masterRealm = config('services.ticoop_auth.master_realm');

        $username = config('services.ticoop_auth.import_users_username');
        $password = config('services.ticoop_auth.import_users_password');

        $response = $guzzleClient->request(
            'POST',
            $baseUrl . '/auth/realms/' . $masterRealm . '/protocol/openid-connect/token',
            [
                'form_params' => [
                    'username' => $username,
                    'password' => $password,
                    'grant_type' => 'password',
                    'client_id' => 'admin-cli',
                ],
            ]
        );
        $token = json_decode((string)$response->getBody(), true)['access_token'];

        $perPage = 100;
        $offset = 0;
        $page = null;
        $users = collect();
        while ($page === null || count($page) > 0) {
            $response = $guzzleClient->request(
                'GET',
                $baseUrl . '/auth/admin/realms/' . $realm . '/users?max=' . $perPage . '&first=' . $offset,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                    ],
                ]
            );
            $page = json_decode((string)$response->getBody(), true);
            $users = $users->merge($page);
            $offset += $perPage;
        }
        
        foreach ($users as $user) {
            User::withTrashed()->updateOrCreate([
                'oauth_id' => $user['id'],
            ], [
                'firstname' => $user['firstName'],
                'lastname' => $user['lastName'],
                'email' => $user['email'],
                'username' => $user['username'],
            ]);
        }
    }
}
