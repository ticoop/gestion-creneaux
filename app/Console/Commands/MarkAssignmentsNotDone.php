<?php

namespace App\Console\Commands;

use App\ActualAssignment;
use App\ConfigItem;
use Illuminate\Console\Command;

class MarkAssignmentsNotDone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mark-assignments-not-done';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $delay = ConfigItem::getCached('slot_max_start_after');
        ActualAssignment::whereNull('done')
            ->where('start_at', '<', now()->subMinutes($delay))
            ->update(['done' => false, 'compensations_needed' => 2]);
    }
}
