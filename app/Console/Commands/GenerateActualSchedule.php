<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ShopSchedule;
use App\ShopScheduleSlot;
use App\ConfigItem;
use Carbon\Carbon;
use DB;

class GenerateActualSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-actual-schedule {--include-past}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate actual schedule from assignments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $nbWeeks = ConfigItem::getCached('working_shift_frequency');

        $deletedSlots = ShopScheduleSlot::onlyTrashed()
            ->has('actualAssignments')
            ->get();
        foreach ($deletedSlots as $slot) {
            $slot->actualAssignments()->delete();
        }

        $slotsToProcess = ShopScheduleSlot::where('should_update_assignments', true)->get()->pluck('id');
        ShopScheduleSlot::query()->update(['should_update_assignments' => false]);

        $specialSlots = ShopScheduleSlot::whereNotNull('special_slot_type')->with([
            'actualAssignments' => function ($q) {
                $q->where('date', '>', now());
            },
        ])->get();
        foreach ($specialSlots as $slot) {
            foreach ($slot->actualAssignments as $a) {
                if ($a->date->diffInWeeks($slot->start_date) % $slot->frequency != 0 ||
                    ($slot->end_date !== null && $a->date->gt($slot->end_date))
                ) {
                    $a->delete();
                }
            }
        }

        $query = ShopSchedule::query();
        if (!$this->option('include-past')) {
            $query->where('end_at', '>=', now());
        }

        $schedules = $query->whereNull('override_shop_schedule_id')
            ->with('slots.assignments.user.leaves')
            ->with('slots.assignments.switches')
            ->with('slots.actualAssignments')
            ->with('overrides.slots.assignments.user.leaves')
            ->with('overrides.slots.assignments.switches')
            ->with('overrides.slots.actualAssignments')
            ->with('overrides.overrides.slots.assignments.user.leaves')
            ->with('overrides.overrides.slots.assignments.switches')
            ->with('overrides.overrides.slots.actualAssignments')
            ->get();
        foreach ($schedules as $schedule) {
            if ($this->option('include-past')) {
                $startDate = $schedule->start_at;
            } else {
                $startDate = max(now()->startOfDay(), $schedule->start_at);
            }
            $computedSlots = $schedule->computeSlotsByDay($startDate, $schedule->end_at);
            foreach ($computedSlots as $day => $slots) {
                $carbonDay = Carbon::parse($day);
                $weekIndex = getWeekIndex($carbonDay);
                $processSlots = $slots->filter(function ($s) use ($slotsToProcess, $carbonDay) {
                    return $slotsToProcess->contains($s->id) && (
                        $this->option('include-past') ||
                        Carbon::parse(substr($carbonDay, 0, 10) . ' ' . $s->start_at)->gt(now())
                    );
                });
                foreach ($processSlots as $slot) {
                    $slot->actualAssignments->filter(function ($a) use ($carbonDay) {
                        return !!$a->assignment_id && $a->date->equalTo($carbonDay);
                    })->each(function ($a) {
                        $a->delete();
                    });
                    $assignments = $slot->assignments->filter(function ($a) use ($weekIndex) {
                        return $a->week_index == $weekIndex;
                    });
                    foreach ($assignments as $assignment) {
                        $switch = $assignment->switches->filter(function ($s) use ($carbonDay) {
                            return $s->date->equalTo($carbonDay);
                        })->first();
                        $finalUser = !!$switch ? $switch->replacementUser : $assignment->user;
                        if (!!$finalUser) {
                            $hasLeave = $finalUser->leaves->reduce(function ($result, $leave) use ($carbonDay) {
                                return $result || ($leave->start_at->lte($carbonDay) && $leave->end_at->gte($carbonDay));
                            }, false);
                            $exclude = $hasLeave ||
                                is_null($assignment->user->start_date) ||
                                $carbonDay->lt($assignment->user->start_date) ||
                                (!is_null($assignment->user->end_date) && $carbonDay->gt($assignment->user->end_date));
                            if (!$exclude) {
                                $slot->actualAssignments()->create([
                                    'user_id' => !!$switch ? $switch->replacement_user_id : $assignment->user_id,
                                    'date' => $carbonDay,
                                    'start_at' => substr($carbonDay, 0, 10) . ' ' . $slot->start_at,
                                    'end_at' => substr($carbonDay, 0, 10) . ' ' . $slot->end_at,
                                    'assignment_id' => $assignment->id,
                                    'referent' => !!$switch ? ($switch->replacementUser->referent && $assignment->referent) : $assignment->referent,
                                ]);
                            }
                        }
                    }
                }
            }
        }

        $this->call(CleanupOverridenActualAssignments::class);
    }
}
