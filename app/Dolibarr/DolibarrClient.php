<?php

namespace App\Dolibarr;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;

class DolibarrClient
{
    private $token = null;

    public function __construct()
    {
        $this->guzzleClient = new GuzzleClient([
            'base_uri' => config('dolibarr.server') . 'api/index.php/',
        ]);
    }

    public function listMembers()
    {
        $page = 0;
        $perPage = 100;
        $finished = false;
        $res = [];
        while (!$finished) {
            try {
                $data = $this->query('GET', "members?sortfield=t.rowid&sortorder=ASC&limit=$perPage&page=$page");
                $res = array_merge($res, $data);
                $page++;
            } catch (ClientException $e) {
                if ($e->getCode() == 404) {
                    $finished = true;
                } else {
                    throw $e;
                }
            }
        }
        return $res;
    }

    public function downloadDocument($module, $path)
    {
        return $this->query('GET', 'documents/download?module_part=' . $module . '&original_file=' . $path);
    }

    protected function login()
    {
        try {
            $response = $this->guzzleClient->request('POST', 'login', [
                'form_params' => [
                    'login' => config('dolibarr.username'),
                    'password' => config('dolibarr.password'),
                ],
            ]);
            if ($response->getStatusCode() == 200) {
                $data = json_decode((string)$response->getBody(), true);
                $this->token = $data['success']['token'];
                return true;
            } else {
                $this->token = null;
                return false;
            }
        } catch (ClientException $e) {
            $this->token = null;
            return false;
        }
    }

    protected function query($method, $path, $params = [], $retry = false)
    {
        if ($this->token === null) {
            $this->login();
        }

        try {
            $response = $this->guzzleClient->request($method, $path, [
                'form_params' => $params,
                'headers' => [
                    'DOLAPIKEY' => $this->token,
                ],
            ]);
            $data = json_decode((string)$response->getBody(), true);
            return $data;
        } catch (ClientException $e) {
            if ($e->getCode() == 401 && !$retry) {
                $this->login();
                return $this->query($method, $path, $params, true);
            } else {
                throw $e;
            }
        }
    }
}
