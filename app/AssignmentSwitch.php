<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignmentSwitch extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'replacement_user_id',
        'assignment_id',
        'date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date',
    ];

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }
    
    public function replacementUser()
    {
        return $this->belongsTo(User::class, 'replacement_user_id');
    }
}
