<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Relations\AssignmentReferentUsersRelation;

class Assignment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_schedule_slot_id',
        'user_id',
        'week_index',
        'referent',
    ];

    public function slot()
    {
        return $this->belongsTo(ShopScheduleSlot::class, 'shop_schedule_slot_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function referentUsers()
    {
        return new AssignmentReferentUsersRelation($this);
    }

    public function switches()
    {
        return $this->hasMany(AssignmentSwitch::class);
    }
    
    public function switchOffers()
    {
        return $this->hasMany(AssignmentSwitchOffer::class, 'offer_assignment_id');
    }

    public function actualAssignments()
    {
        return $this->hasMany(ActualAssignment::class);
    }
}