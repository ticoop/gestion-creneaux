<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RescueAssignment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'date',
        'day_part_id',
        'order',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function dayPart()
    {
        return $this->belongsTo(DayPart::class, 'day_part_id');
    }
}
