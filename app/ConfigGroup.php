<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ConfigGroup extends Model
{
    use HasTranslations;

    public $translatable = ['label'];

    public function children()
    {
        return $this->hasMany(ConfigGroup::class, 'parent_group_id')->orderBy('order');
    }

    public function items()
    {
        return $this->hasMany(ConfigItem::class, 'config_group_id')->orderBy('order');
    }
}
