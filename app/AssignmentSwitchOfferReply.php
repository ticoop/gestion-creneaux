<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignmentSwitchOfferReply extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reply_assignment_id',
        'reply_date',
        'reply_user_id',
        'reply_flying',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'replyUser',
        'slot',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'reply_flying' => 'boolean',
    ];

    public function offer()
    {
        return $this->belongsTo(AssignmentSwitchOffer::class, 'assignment_switch_offer_id');
    }
    
    public function replyAssignment()
    {
        return $this->belongsTo(Assignment::class, 'reply_assignment_id');
    }

    public function slot()
    {
        return $this->hasOneThrough(
            ShopScheduleSlot::class,
            Assignment::class,
            'id',
            'id',
            'reply_assignment_id',
            'shop_schedule_slot_id'
        );
    }

    public function replyUser()
    {
        return $this->belongsTo(User::class, 'reply_user_id');
    }
}
