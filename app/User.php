<?php

namespace App;

use App\Helpers\StringParser;
use App\Relations\UserAssignmentSwitches;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, HasMediaTrait, SoftDeletes, HasApiTokens;

    const STATUS_UP_TO_DATE        = 1;
    const STATUS_ALERT             = 2;
    const STATUS_SUSPENDED         = 3;
    const STATUS_BLOCKED           = 4;
    const STATUS_INACTIVE          = 5;
    const STATUS_LEAVE_NO_SHOPPING = 6;
    const STATUS_UNSUBSCRIBED      = 7;
    const STATUSES = [
        self::STATUS_UP_TO_DATE => 'up-to-date',
        self::STATUS_ALERT => 'alert',
        self::STATUS_SUSPENDED => 'suspended',
        self::STATUS_BLOCKED => 'blocked',
        self::STATUS_INACTIVE => 'inactive',
        self::STATUS_LEAVE_NO_SHOPPING => 'leave-no-shopping',
        self::STATUS_UNSUBSCRIBED => 'unsubscribed',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'lastname',
        'firstname',
        'email',
        'oauth_id',
        'referent',
        'flying_team',
        'start_date',
        'end_date',
        'emergency_team_days',
        'shift_email_reminders',
        'allow_phone_number_sharing',
        'phone',
        'accounting_team',
        'service_account',
        'birth_date',
        'gender_id',
        'note',
        'subscription_date',
        'used_firstname',
    ];

    protected $appends = [
        'fullname',
        'active',
        'age',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_date',
        'end_date',
        'birth_date',
        'subscription_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'shift_email_reminders' => 'boolean',
        'allow_phone_number_sharing' => 'boolean',
        'accounting_team' => 'boolean',
        'referent' => 'boolean',
        'flying_team' => 'boolean',
        'service_account' => 'boolean',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'rescueTeamDays',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function trainings()
    {
        return $this->belongsToMany(Training::class);
    }

    public function leaves()
    {
        return $this->hasMany(Leave::class);
    }

    public function currentNoShoppingAllowedLeaves()
    {
        return $this->leaves()
            ->whereHas('leaveType', function ($q) {
                $q->where('shopping_allowed', false);
            })
            ->where('start_at', '<=', now())
            ->where('end_at', '>=', now());
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class);
    }

    public function actualAssignments()
    {
        return $this->hasMany(ActualAssignment::class);
    }

    public function doneNotCompensationActualAssignments()
    {
        return $this->actualAssignments()
            ->where('done', true)
            ->whereNull('compensates_actual_assignment_id')
            ->whereDoesntHave('slot', function ($q) {
                $q->where('volunteer', true);
            });
    }

    public function actualAssignmentsWithCompensationNeeded()
    {
        return $this->actualAssignments()
            ->where('compensations_needed', '>', 0)
            ->withCount('compensationsDone');
    }

    public function assignmentWish()
    {
        return $this->hasOne(AssignmentWish::class);
    }

    public function rescueTeamDays()
    {
        return $this->belongsToMany(DayPart::class, 'rescue_team_days')->withPivot('weekdays');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }
    
    public function currentAssignments()
    {
        return $this->assignments()->whereHas('slot.schedule', function ($q) {
            $q->where('start_at', '<=', now())
                ->where('end_at', '>=', now()->startOfDay())
                ->whereNull('override_shop_schedule_id');
        });
    }

    public function assignmentSwitches()
    {
        return new UserAssignmentSwitches($this);
    }

    public function owedAssignments()
    {
        return $this->hasMany(OwedAssignment::class);
    }

    public function getFullnameAttribute()
    {
        return trim((isset($this->used_firstname) ? $this->used_firstname : $this->firstname) . ' ' . $this->lastname);
    }

    public function scopeWhereMatchesText($query, $text)
    {
        $words = array_filter(explode(' ', $text), 'strlen');
        foreach ($words as $w) {
            $query->where('search_text', 'ILIKE', '%' . StringParser::removeAccents($w) . '%');
        }
    }

    public function hasRole(string $roleKey)
    {
        $userRoles = $this->roles->pluck('key');
        return ($userRoles->contains('admin') && $roleKey != 'dev') || $userRoles->contains($roleKey);
    }

    public function getActiveAttribute()
    {
        return !is_null($this->start_date) && $this->start_date->lte(now()->startOfDay()) &&
            (is_null($this->end_date) || $this->end_date->gte(now()->startOfDay()));
    }

    public function scopeWhereActive($query)
    {
        $query->whereNotNull('start_date')
            ->where('start_date', '<=', now()->startOfDay())
            ->where(function ($q) {
                $q->whereNull('end_date')->orWhere('end_date', '>=', now()->startOfDay());
            });
    }

    public function getEmergencyTeamDaysAttribute($value)
    {
        if ($value) {
            return explode(',', $value);
        } else {
            return [];
        }
    }
    
    public function setEmergencyTeamDaysAttribute(array $value)
    {
        if (count($value) > 0) {
            $this->attributes['emergency_team_days'] = implode(',', $value);
        } else {
            $this->attributes['emergency_team_days'] = null;
        }
    }

    public function getStatusAttribute()
    {
        if ($this->active) {
            $leaves = $this->currentNoShoppingAllowedLeaves->count();
            if ($leaves > 0) {
                return self::STATUS_LEAVE_NO_SHOPPING;
            }

            if ($this->flying_team) {
                $workingShiftFrequency = ConfigItem::getCached('working_shift_frequency');
                $duration = now()->startOfDay()->diffInWeeks($this->start_date->copy()->startOfDay());
                $leaveWeeks = $this->leaves->map(function ($leave) {
                    return min($leave->end_at, now())->diffInWeeks(max($leave->start_at, $this->start_date));
                })->sum();
                $duration -= $leaveWeeks;
                $nbPeriods = (int)floor($duration / $workingShiftFrequency);
                $nbAssignments = $this->doneNotCompensationActualAssignments->count();
                if ($nbAssignments >= $nbPeriods) {
                    $status = self::STATUS_UP_TO_DATE;
                } elseif ($nbAssignments == $nbPeriods - 1) {
                    $status = self::STATUS_ALERT;
                } else {
                    $status = self::STATUS_SUSPENDED;
                }
            } else {
                $status = self::STATUS_UP_TO_DATE;
            }

            $assignments = $this->actualAssignmentsWithCompensationNeeded
                ->filter(function ($a) {
                    return $a->compensations_done_count < $a->compensations_needed;
                });
            
            foreach ($assignments as $assignment) {
                $dateLimit = $assignment->date->addDays($assignment->compensation_delay);
                if ($dateLimit->lt(now()->startOfDay())) {
                    if ($status == self::STATUS_SUSPENDED) {
                        $status = self::STATUS_BLOCKED;
                    } else {
                        $status = max($status, self::STATUS_SUSPENDED);
                    }
                } else {
                    $status = max($status, self::STATUS_ALERT);
                }
            }

            $owedAssignments = $this->owedAssignments()->whereDoesntHave('actualAssignment', function ($q) {
                $q->where('done', true);
            })->where('initial_assignment_date', '<', now()->startOfDay()->subDays(ConfigItem::getCached('flying_switch_delay')))->get();
            foreach ($owedAssignments as $owedAssignment) {
                if ($status == self::STATUS_UP_TO_DATE) {
                    $status = self::STATUS_ALERT;
                } elseif ($status == self::STATUS_ALERT) {
                    $status = self::STATUS_SUSPENDED;
                } elseif ($status == self::STATUS_SUSPENDED) {
                    $status = self::STATUS_BLOCKED;
                }
            }
        } else {
            if (!!$this->end_date && $this->end_date->lt(now()->startOfDay())) {
                $status = self::STATUS_UNSUBSCRIBED;
            } else {
                $status = self::STATUS_INACTIVE;
            }
        }

        return $status;
    }

    public function runningAssignments($referent = false)
    {
        $res = $this->actualAssignments()
            ->where('start_at', '<', now()->addHour())
            ->where('end_at', '>', now()->subMinutes(30));
        if ($referent) {
            $res = $res->where('referent', true);
        }
        return $res;
    }

    public function isWorking()
    {
        return $this->runningAssignments()->count() > 0;
    }

    public function isWorkingAsReferent()
    {
        return $this->runningAssignments(true)->count() > 0;
    }

    public function hasUpcomingReferentAssignment()
    {
        return $this->actualAssignments()
            ->where('referent', true)
            ->where('start_at', '<', now()->addDay())
            ->where('end_at', '>', now()->subMinutes(30))
            ->count() > 0;
    }

    public function getAgeAttribute()
    {
        if ($this->birth_date) {
            return now()->diffInYears($this->birth_date);
        } else {
            return null;
        }
    }
    
    public function setFirstnameAttribute($value)
    {
        $this->attributes['firstname'] = $value;
        $this->updateSearchText();
    }

    public function setLastnameAttribute($value)
    {
        $this->attributes['lastname'] = $value;
        $this->updateSearchText();
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = $value;
        $this->updateSearchText();
    }

    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = $value;
        $this->updateSearchText();
    }

    public function updateSearchText()
    {
        $this->search_text = collect(
            Arr::only($this->toArray(), ["firstname", "lastname", "email", "username", "used_firstname"])
        )
            ->filter(function ($item) {
                return !!$item;
            })
            ->map(function ($item) {
                return StringParser::removeAccents($item);
            })
            ->join(" ");
    }
}
