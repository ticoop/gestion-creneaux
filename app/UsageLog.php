<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UsageEvent;

/**
 * The UsageLog model represents individual logged events
 */
class UsageLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usage_event_id',
        'user_id',
        'data',
    ];

    /**
     * Get a usage event by key
     * 
     * @return \App\UsageEvent
     */
    private static function getEvent(string $eventKey): UsageEvent
    {
        // A map of possible events is stored in session to limit requests
        $eventMap = session('usage-events', null);
        if (!$eventMap) {
            $eventMap = [];
            foreach (UsageEvent::all() as $event) {
                $eventMap[$event->key] = $event;
            }
            session(['usage-events' => $eventMap]);
        }

        return isset($eventMap[$eventKey]) ? $eventMap[$eventKey] : null;
    }

    /**
     * Create a new UsageLog entry
     * 
     * @param string $eventKey The event key
     * @param array|null $data Additional data associated with the event
     * 
     * @return \App\UsageLog
     */
    public static function store(string $eventKey, array $data = null): UsageLog
    {
        $event = static::getEvent($eventKey);
        if (!!$event) {
            return $event->logs()->create([
                'user_id' => auth()->check() ? auth()->user()->id : null,
                'data' => !!$data ? json_encode($data) : null,
            ]);
        }
    }

    public function event()
    {
        return $this->belongsTo(UsageEvent::class, 'usage_event_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
