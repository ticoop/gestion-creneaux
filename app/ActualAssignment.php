<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ConfigItem;
use App\KeyCode;
use App\Relations\ActualAssignmentSwitchOffers;
use App\Relations\ParallelActualAssignments;
use App\Scopes\ActualAssignmentScope;

class ActualAssignment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'date',
        'start_at',
        'end_at',
        'assignment_id',
        'compensation',
        'done',
        'referent',
        'compensates_actual_assignment_id',
        'compensations_needed',
        'owed_assignment_id',
        'compensation_delay',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
        'start_at',
        'end_at',
        'arrived_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'done' => 'boolean',
    ];

    protected $appends = [
        'startable',
    ];

    public $loadKeyCodes = false;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new ActualAssignmentScope);
    }

    public function slot()
    {
        return $this->belongsTo(ShopScheduleSlot::class, 'shop_schedule_slot_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class, 'assignment_id');
    }

    public function compensations()
    {
        return $this->hasMany(ActualAssignment::class, 'compensates_actual_assignment_id');
    }

    public function compensationsDone()
    {
        return $this->compensations()->where('done', true);
    }

    public function parallelAssignments()
    {
        return new ParallelActualAssignments($this);
    }

    public function owedAssignment()
    {
        return $this->belongsTo(OwedAssignment::class);
    }
    
    public function switchOffers()
    {
        return new ActualAssignmentSwitchOffers($this);
    }

    public function hasReferentUser(User $user)
    {
        return $this->parallelAssignments->filter(function ($a) use ($user) {
            return $a->user_id == $user->id && $a->referent;
        })->count() == 1;
    }

    public function getStartableAttribute()
    {
        $maxDelayBefore = ConfigItem::getCached('slot_max_start_before');
        $maxDelayAfter = ConfigItem::getCached('slot_max_start_after');

        return is_null($this->arrived_at) &&
            now()->gte($this->start_at->subMinutes($maxDelayBefore)) &&
            now()->lte($this->start_at->addMinutes($maxDelayAfter));
    }

    public function getDateStringAttribute()
    {
        return $this->attributes['date'];
    }

    public function getCompensationDelayAttribute($value)
    {
        if ($value === null) {
            return ConfigItem::getCached('compensation_delay');
        } else {
            return $value;
        }
    }

    public function start()
    {
        if ($this->startable) {
            $this->done = true;
            $this->arrived_at = now();
            $this->save();
        } else {
            throw new \Exception('ActualAssignment ' . $this->id . ' is not startable.');
        }
    }

    public function getKeyCodes()
    {
        $schedule = ShopSchedule::whereNull('override_shop_schedule_id')
            ->where('start_at', '<=', $this->start_at)
            ->where('end_at', '>=', $this->start_at)
            ->first();
        $slots = $schedule->computeSlotsByDay($this->start_at, $this->start_at)[$this->start_at->format('Y-m-d')]
            ->filter(function ($slot) {
                return $slot->generate_key_code;
            })
            ->sort(function ($a, $b) {
                return strcasecmp($a->start_at, $b->start_at);
            });
        $codes = DayKeyCode::whereDate('date', $this->start_at)->first();
        $openingCode = $codes->opening_code;
        $closingCode = $this->shop_schedule_slot_id == $slots->last()->id ? $codes->closing_code : $codes->opening_code;
        return [
            'opening' => $openingCode,
            'closing' => $closingCode,
        ];
    }
}
