<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            $redirectUrl = $request->fullUrl();
            if (config('app.force_https') && substr($redirectUrl, 0, 7) == 'http://') {
                $redirectUrl = 'https://' . substr($redirectUrl, 7);
            }
            session(['postlogin-redirect' => $redirectUrl]);
            return route('login');
        }
    }
}
