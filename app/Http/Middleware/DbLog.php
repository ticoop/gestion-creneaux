<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class DbLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('database.enable-query-log')) {
            DB::enableQueryLog();
        }
        return $next($request);
    }

    /**
     * Called at the end of processing a request
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Reponse $response
     * @return void
     */
    public function terminate($request, $response)
    {
        if (config('database.enable-query-log')) {
            switch (config('database.query-log-mode')) {
                case 'dump':
                    dd(DB::getQueryLog());
                    break;
                case 'json':
                    $url = url()->current();
                    $method = $request->method();
                    $logFile = storage_path('logs/db_queries.json');
                    $lockFile = storage_path('logs/db_queries_lock');
                    $fpLock = fopen($lockFile, "w");
                    flock($fpLock, LOCK_EX);
                    if (file_exists($logFile)) {
                        $data = json_decode(file_get_contents($logFile), true);
                        if (!$data) {
                            $data = [
                                'calls' => [],
                                'queries' => [],
                            ];
                        }
                    } else {
                        $data = [
                            'calls' => [],
                            'queries' => [],
                        ];
                    }
                    if (!array_key_exists($url, $data['calls'])) {
                        $data['calls'][$url] = [];
                    }
                    if (!array_key_exists($method, $data['calls'][$url])) {
                        $data['calls'][$url][$method] = 0;
                    }
                    $data['calls'][$url][$method] += 1;
                    foreach (DB::getQueryLog() as $entry) {
                        $entry['url'] = $url;
                        $entry['timestamp'] = microtime(true);
                        $entry['http_method'] = $method;
                        $entry['input'] = $request->input();
                        $data['queries'][] = $entry;
                    }
                    file_put_contents(
                        $logFile,
                        json_encode($data)
                    );
                    flock($fpLock, LOCK_UN);
                    fclose($fpLock);
                    break;
                case 'txt':
                default:
                    file_put_contents(
                        storage_path('logs/db_queries.log'),
                        "#####" . url()->current(),
                        FILE_APPEND
                    );
                    file_put_contents(
                        storage_path('logs/db_queries.log'),
                        print_r(DB::getQueryLog(), true),
                        FILE_APPEND
                    );
                    break;
            }
        }
    }
}
