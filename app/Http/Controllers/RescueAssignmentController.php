<?php

namespace App\Http\Controllers;

use App\DayPart;
use App\Http\Requests\GetNextRescueAssignmentRequest;
use App\Http\Requests\SetUnavailableForRescueTeamRequest;
use App\Http\Resources\User as UserResource;
use App\RescueAssignment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RescueAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return \Illuminate\Http\Response
     */
    public function show(RescueAssignment $rescueAssignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RescueAssignment $rescueAssignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(RescueAssignment $rescueAssignment)
    {
        //
    }

    protected function getDayPart(Carbon $date)
    {
        return DayPart::where(function ($q) use ($date) {
            $q->whereNull('start_time')->orWhere('start_time', '<=', $date->format('H:i'));
        })->where(function ($q) use ($date) {
            $q->whereNull('end_time')->orWhere('end_time', '>', $date->format('H:i'));
        })->first();
    }

    public function getNextUser(GetNextRescueAssignmentRequest $request)
    {
        $period = $request->input('period', 'now');
        if ($period == 'now') {
            $date = now();
        } else {
            $date = auth()->user()->actualAssignments()->where('start_at', '>=', now())->first()->start_at;
        }

        $dayPart = $this->getDayPart($date);

        $hasAssignemnts = RescueAssignment::whereDate('date', $date)
            ->where('day_part_id', $dayPart->id)
            ->count() > 0;
        
        if (!$hasAssignemnts) {
            $this->initAssignments($date);
        }

        $assignment = RescueAssignment::whereDate('date', $date)
            ->where('day_part_id', $dayPart->id)
            ->where('tried', false)
            ->orderBy('order')
            ->first();
        if (!!$assignment) {
            return new UserResource($assignment->user);
        } else {
            return null;
        }
    }

    protected function initAssignments($date)
    {
        $dayPart = $this->getDayPart($date);
        $users = User::whereHas('rescueTeamDays', function ($q) use ($date, $dayPart) {
            $q->where('rescue_team_days.weekdays', 'LIKE', '%' . $date->dayOfWeek . '%')->where('id', $dayPart->id);
        })->get()
            ->sort(function ($a, $b) {
                return rand(-1, 1);
            })
            ->merge([]);
        foreach ($users as $i => $user) {
            RescueAssignment::create([
                'user_id' => $user->id,
                'date' => $date,
                'day_part_id' => $dayPart->id,
                'order' => $i,
            ]);
        }
    }

    public function setUnavailable(SetUnavailableForRescueTeamRequest $request)
    {
        $period = $request->input('period', 'now');
        if ($period == 'now') {
            $date = now();
        } else {
            $date = auth()->user()->actualAssignments()->where('start_at', '>=', now())->first()->start_at;
        }

        $dayPart = $this->getDayPart($date);

        RescueAssignment::whereDate('date', $date)
            ->where('day_part_id', $dayPart->id)
            ->where('user_id', $request->user_id)
            ->update(['tried' => true]);
    }
}
