<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use ZipArchive;

class TechnicalToolsController extends Controller
{
    public function exportDatabase()
    {
        $this->authorize('admin');

        Artisan::call('db:export');

        $path = storage_path('db-export');
        $zipFile = tempnam(sys_get_temp_dir(), '') . '.zip';
        $zip = new ZipArchive;
        $zip->open($zipFile, ZipArchive::CREATE);
        foreach (scandir($path) as $f) {
            if ($f != '.' && $f != '..') {
                $zip->addFile($path . '/' . $f, '/' . $f);
            }
        }
        $zip->close();

        return response()->download($zipFile);
    }
}
