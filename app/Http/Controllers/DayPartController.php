<?php

namespace App\Http\Controllers;

use App\DayPart;
use App\Http\Resources\DayPartCollection as DayPartCollectionResource;
use Illuminate\Http\Request;

class DayPartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new DayPartCollectionResource(DayPart::orderBy('order')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DayPart  $dayPart
     * @return \Illuminate\Http\Response
     */
    public function show(DayPart $dayPart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DayPart  $dayPart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DayPart $dayPart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DayPart  $dayPart
     * @return \Illuminate\Http\Response
     */
    public function destroy(DayPart $dayPart)
    {
        //
    }
}
