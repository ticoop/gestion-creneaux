<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleIndexRequest;
use App\Http\Resources\RoleCollection as RoleCollectionResource;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RoleIndexRequest $request)
    {
        return new RoleCollectionResource(Role::all());
    }
}
