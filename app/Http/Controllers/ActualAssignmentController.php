<?php

namespace App\Http\Controllers;

use App\ActualAssignment;
use Illuminate\Http\Request;
use App\Http\Requests\ActualAssignmentUpdateRequest;
use App\Http\Requests\ActualAssignmentShowRequest;
use App\Http\Resources\ActualAssignment as ActualAssignmentResource;

class ActualAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ActualAssignment  $actualAssignment
     * @return \Illuminate\Http\Response
     */
    public function show(ActualAssignmentShowRequest $request, ActualAssignment $actualAssignment)
    {
        $actualAssignment->load(['slot', 'parallelAssignments.user']);
        return new ActualAssignmentResource($actualAssignment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ActualAssignment  $actualAssignment
     * @return \Illuminate\Http\Response
     */
    public function update(ActualAssignmentUpdateRequest $request, ActualAssignment $actualAssignment)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        if (array_key_exists('done', $data) && !array_key_exists('compensations_needed', $data)) {
            $data['compensations_needed'] = $data['done'] ? 0 : 2;
        }

        $actualAssignment->update($data);

        return new ActualAssignmentResource($actualAssignment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ActualAssignment  $actualAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActualAssignment $actualAssignment)
    {
        //
    }
}
