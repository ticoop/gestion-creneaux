<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExportUsersRequest;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserIndexRequest;
use App\Http\Requests\UserShowRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\ImpersonateRequest;
use App\Http\Requests\StopImpersonateRequest;
use App\Http\Resources\User as UserResource;
use Eluceo\iCal\Component\Calendar as IcalCalendar;
use Eluceo\iCal\Component\Event as IcalEvent;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\UrlGenerator\UrlGeneratorFactory;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserIndexRequest $request)
    {
        return $request->process();
    }

    protected function doShow(Request $request, User $user)
    {
        if ($request->with_roles) {
            $user->load('roles');
        }

        if ($request->with_trainings) {
            $user->load('trainings');
        }

        $user->append('status');
        
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(UserShowRequest $request, User $user)
    {
        return $this->doShow($request, $user);
    }

    public function showCurrentUser(Request $request)
    {
        return $this->doShow($request, auth()->user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        if (array_key_exists('roles', $data)) {
            $user->roles()->sync($data['roles']);
            unset($data['roles']);
        }

        if (array_key_exists('trainings', $data)) {
            $user->trainings()->sync($data['trainings']);
            unset($data['trainings']);
        }

        if (array_key_exists('rescue_team_days', $data)) {
            $rescueTeamDays = [];
            foreach ($data['rescue_team_days'] as $day) {
                $rescueTeamDays[$day['id']] = ['weekdays' => $day['pivot']['weekdays']];
            }
            $user->rescueTeamDays()->sync($rescueTeamDays);
            unset($data['rescue_team_days']);
        }
        
        if (count($data) > 0) {
            $user->update($data);
        }

        return new UserResource($user);
    }

    public function getIcsCalendar(User $user)
    {
        $assignments = $user->actualAssignments()->with('slot')->get();

        $calendar = (new IcalCalendar(config('app.shop_name')))->setName(__('users.calendar-title'));

        foreach ($assignments as $assignment) {
            $event = new IcalEvent();
            $startAt = Carbon::parse(substr($assignment->date, 0, 10) . ' ' . $assignment->slot->start_at);
            $endAt = Carbon::parse(substr($assignment->date, 0, 10) . ' ' . $assignment->slot->end_at);
            $event->setDtStart($startAt)
                ->setDtEnd($endAt)
                ->setSummary(__('users.calendar-event-title'));
            $calendar->addComponent($event);
        }

        return response($calendar->render())
            ->header('Content-Type', 'text/calendar')
            ->header('Content-Disposition', 'attachment; filename="' . config('app.shop_name') . '.ics"');;
    }

    public function getPhoto(User $user, Request $request)
    {
        $media = $user->getFirstMedia('photo');
        if (!!$media &&
            Storage::disk($media->disk)->exists(UrlGeneratorFactory::createForMedia($media)->getPathRelativeToRoot())
        ) {
            $downloadHeaders = [
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Type' => $media->mime_type,
                'Content-Length' => $media->size,
                'Pragma' => 'public',
            ];
    
            return response()->stream(function () use ($media) {
                $stream = $media->stream();
    
                fpassthru($stream);
    
                if (is_resource($stream)) {
                    fclose($stream);
                }
            }, 200, $downloadHeaders);
        } else {
            return response()->file(public_path('user.png'));
        }
    }

    public function impersonate(User $user, ImpersonateRequest $request)
    {
        return $request->process($user);
    }

    public function stopImpersonating(StopImpersonateRequest $request)
    {
        return $request->process();
    }

    public function export(ExportUsersRequest $request)
    {
        return $request->process();
    }
}
