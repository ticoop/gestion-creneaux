<?php

namespace App\Http\Controllers;

use App\Http\Requests\OwedAssignmentIndexRequest;
use App\OwedAssignment;
use App\User;
use Illuminate\Http\Request;

class UserOwedAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function index(OwedAssignmentIndexRequest $request, User $user)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @param  \App\OwedAssignment  $owedAssignment
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, OwedAssignment $owedAssignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @param  \App\OwedAssignment  $owedAssignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, OwedAssignment $owedAssignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @param  \App\OwedAssignment  $owedAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, OwedAssignment $owedAssignment)
    {
        //
    }
}
