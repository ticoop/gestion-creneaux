<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ConfigGroup;
use App\RescueAssignment;

class MenuController extends Controller
{
    public function show(string $menu)
    {
        switch ($menu) {
            case 'all':
                return [
                    'main' => $this->showMainMenu(),
                    'admin' => $this->showAdminMenu(),
                    'config' => $this->showConfigMenu(),
                    'tech' => $this->showTechMenu(),
                ];
                break;
            case 'main':
                return $this->showMainMenu();
                break;
            case 'admin':
                return $this->showAdminMenu();
                break;
            case 'config':
                return $this->showConfigMenu();
                break;
            case 'tech':
                return $this->showTechMenu();
                break;
            default:
                abort(404);
        }
    }

    private function showMainMenu()
    {
        $menu = [];

        $menu[] = [
            'label' => __('menus.main.home'),
            'icon' => 'home',
            'url' => '/',
        ];

        $menu[] = [
            'label' => __('menus.main.my-schedule'),
            'icon' => 'perm_contact_calendar',
            'url' => '/my-schedule',
        ];

        if (auth()->user()->isWorkingAsReferent()) {
            $menu[] = [
                'label' => __('menus.main.my-team-attendance'),
                'icon' => 'verified',
                'url' => '/my-team-attendance',
            ];
        }

        $menu[] = [
            'label' => __('menus.main.schedule'),
            'icon' => 'calendar_today',
            'url' => '/schedule',
        ];

        if (auth()->user()->can('viewAny', RescueAssignment::class)) {
            $menu[] = [
                'label' => __('menus.main.rescue-team'),
                'icon' => 'call',
                'url' => '/rescue-team',
            ];
            if (User::whereNotNull('emergency_team_days')->count() > 0) {
                $menu[] = [
                    'label' => __('menus.main.emergency-numbers'),
                    'icon' => 'call',
                    'url' => '/emergency-numbers',
                ];
            }
        }

        if (auth()->user()->can('viewStatus', User::class)) {
            $menu[] = [
                'label' => __('menus.main.user-statuses'),
                'icon' => 'done',
                'url' => '/user-statuses',
            ];
        }

        $menu[] = [
            'label' => __('menus.main.assignment-switch-offers'),
            'icon' => 'swap_horiz',
            'url' => '/assignment-switch-offers',
        ];

        $menu[] = [
            'label' => __('menus.main.leaves'),
            'icon' => 'beach_access',
            'url' => '/leaves',
        ];

        $menu[] = [
            'label' => __('menus.main.profile'),
            'icon' => 'person',
            'url' => '/profile',
        ];

        if (!!config('app.help_wiki_page')) {
            $menu[] = [
                'label' => __('menus.main.help'),
                'icon' => 'help_outline',
                'url' => config('app.help_wiki_page'),
                'external' => true,
            ];
        }

        return $menu;
    }

    private function showAdminMenu()
    {
        $menu = [];

        if (auth()->user()->hasRole('admin-users')) {
            $menu[] = [
                'label' => __('menus.admin.users'),
                'icon' => 'people',
                'url' => '/admin/users',
            ];
        }

        if (auth()->user()->hasRole('admin-shop-schedule')) {
            $menu[] = [
                'label' => __('menus.admin.schedule'),
                'icon' => 'calendar_today',
                'url' => '/admin/schedule',
            ];
        }

        if (auth()->user()->hasRole('admin-slot-assignment')) {
            $menu[] = [
                'label' => __('menus.admin.slot-assignment'),
                'icon' => 'timer',
                'url' => '/admin/slot-assignment',
            ];
        }

        if (auth()->user()->hasRole('admin-key-codes')) {
            $menu[] = [
                'label' => __('menus.admin.key-codes'),
                'icon' => 'vpn_key',
                'url' => '/admin/key-codes',
            ];
        }

        if (auth()->user()->hasRole('admin-users')) {
            $menu[] = [
                'label' => __('menus.admin.emergency-team-planning'),
                'icon' => 'call',
                'url' => '/admin/emergency-team-planning',
            ];
        }

        if (auth()->user()->hasRole('admin-attendance')) {
            $menu[] = [
                'label' => __('menus.admin.attendance-monitoring'),
                'icon' => 'verified',
                'url' => '/admin/attendance-monitoring',
            ];
        }

        if (auth()->user()->hasRole('admin-slot-assignment')) {
            $menu[] = [
                'label' => __('menus.admin.incomplete-switches'),
                'icon' => 'swap_horiz',
                'url' => '/admin/incomplete-switches',
            ];
        }

        if (auth()->user()->hasRole('admin-slot-assignment')) {
            $menu[] = [
                'label' => __('menus.admin.slots-without-referent'),
                'icon' => 'warning',
                'url' => '/admin/slots-without-referent',
            ];
        }

        if (auth()->user()->hasRole('admin-slot-assignment')) {
            $menu[] = [
                'label' => __('menus.admin.incomplete-slots'),
                'icon' => 'warning',
                'url' => '/admin/incomplete-slots',
            ];
        }

        if (auth()->user()->hasRole('view-new-referents')) {
            $menu[] = [
                'label' => __('menus.admin.new-referents'),
                'icon' => 'support',
                'url' => '/admin/new-referents',
            ];
        }

        if (auth()->user()->hasRole('admin-shop-schedule')) {
            $menu[] = [
                'label' => __('menus.admin.ending-leaves'),
                'icon' => 'beach_access',
                'url' => '/admin/ending-leaves',
            ];
        }

        if (auth()->user()->hasRole('view-stats')) {
            $menu[] = [
                'label' => __('menus.admin.stats'),
                'icon' => 'analytics',
                'url' => '/admin/stats',
            ];
        }

        return $menu;
    }

    private function showConfigMenu()
    {
        $menu = [];

        if (auth()->user()->hasRole('admin-trainings')) {
            $menu[] = [
                'label' => __('menus.config.trainings'),
                'icon' => 'school',
                'url' => '/config/trainings',
            ];
        }

        if (auth()->user()->hasRole('admin-leave-types')) {
            $menu[] = [
                'label' => __('menus.config.leave-types'),
                'icon' => 'beach_access',
                'url' => '/config/leave-types',
            ];
        }

        $configGroups = ConfigGroup::whereNull('parent_group_id')
            ->where('display_in_menu', 1)
            ->orderBy('order')
            ->get();

        foreach ($configGroups as $configGroup) {
            if (is_null($configGroup->admin_user_role) ||
                auth()->user()->hasRole($configGroup->admin_user_role)
            ) {
                $menu[] = [
                    'label' => $configGroup->label,
                    'icon' => $configGroup->icon,
                    'url' => '/config/' . $configGroup->id,
                ];
            }
        }

        return $menu;
    }

    private function showTechMenu()
    {
        $menu = [];

        if (auth()->user()->hasRole('see-sent-emails')) {
            $menu[] = [
                'label' => __('menus.admin.sent-emails'),
                'icon' => 'mail',
                'url' => '/admin/sent-emails',
            ];
        }

        if (auth()->user()->hasRole('dev')) {
            $menu[] = [
                'label' => __('menus.admin.technical-tools'),
                'icon' => 'build',
                'url' => '/admin/technical-tools',
            ];
        }

        return $menu;
    }
}
