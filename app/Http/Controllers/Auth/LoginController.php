<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use App\UsageLog;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the oauth provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('ticoop')->redirect();
    }

    /**
     * Obtain the user information from oauth provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        // Retrieve user info from oauth provider
        $remoteUser = Socialite::driver('ticoop')->user();

        // Check if the user already exists locally
        $localUser = User::where('oauth_id', $remoteUser->oauth_id)->first();
        if (!$localUser) {
            // If not, create it
            $localUser = User::create([
                'oauth_id' => $remoteUser->oauth_id,
                'firstname' => $remoteUser->firstname,
                'lastname' => $remoteUser->lastname,
                'email' => $remoteUser->email,
                'username' => $remoteUser->username,
            ]);
        } else {
            $localUser->update([
                'firstname' => $remoteUser->firstname,
                'lastname' => $remoteUser->lastname,
                'email' => $remoteUser->email,
                'username' => $remoteUser->username,
            ]);
        }

        // Login
        auth()->login($localUser);

        UsageLog::store('login');

        $redirectUrl = session('postlogin-redirect', '/');
        session()->forget('postlogin-redirect');
        
        return redirect($redirectUrl);
    }

    public function logout()
    {
        UsageLog::store('logout');
        auth()->logout();
        return redirect('/');
    }
}
