<?php

namespace App\Http\Controllers;

use App\Helpers\Mattermost;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class MattermostController extends Controller
{
    public function getConversationUrl(Request $request)
    {
        if (count($request->usernames) == 1) {
            return str_replace("{username}", $request->usernames[0], config('app.mattermost_message_url'));
        } else {
            $userIds = collect($request->usernames)->map(function ($u) {
                return Mattermost::getUserByUsername($u)['id'];
            });

            $user = auth()->user();
            if (!$user->mattermost_access_token) {
                $currentUserId = Mattermost::getUserByUsername(auth()->user()->username)['id'];
                $user->mattermost_access_token = Mattermost::createUserToken($currentUserId);
                $user->save();
            }

            try {
                $conversation = Mattermost::createGroupConversation($user->mattermost_access_token, $userIds);
            } catch (ClientException $e) {
                $currentUserId = Mattermost::getUserByUsername(auth()->user()->username)['id'];
                $user->mattermost_access_token = Mattermost::createUserToken($currentUserId);
                $user->save();
                $conversation = Mattermost::createGroupConversation($user->mattermost_access_token, $userIds);
            }

            return str_replace("@{username}", $conversation["name"], config('app.mattermost_message_url'));
        }
    }
}
