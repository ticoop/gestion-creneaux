<?php

namespace App\Http\Controllers;

use App\AssignmentSwitch;
use App\Http\Requests\AssignmentSwitchDestroyRequest;
use Illuminate\Http\Request;

class AssignmentSwitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return \Illuminate\Http\Response
     */
    public function show(AssignmentSwitch $assignmentSwitch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssignmentSwitch $assignmentSwitch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignmentSwitchDestroyRequest $request, AssignmentSwitch $assignmentSwitch)
    {
        $assignmentSwitch->delete();
    }
}
