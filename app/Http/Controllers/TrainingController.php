<?php

namespace App\Http\Controllers;

use App\Training;
use Illuminate\Http\Request;
use App\Http\Requests\TrainingIndexRequest;
use App\Http\Requests\TrainingShowRequest;
use App\Http\Requests\TrainingStoreRequest;
use App\Http\Requests\TrainingUpdateRequest;
use App\Http\Requests\TrainingDestroyRequest;
use App\Http\Resources\Training as TrainingResource;
use App\Http\Resources\TrainingCollection as TrainingCollectionResource;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TrainingIndexRequest $request)
    {
        $results = Training::orderByRaw("label->>'" . app()->getLocale() . "'")->get();
        return new TrainingCollectionResource($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrainingStoreRequest $request)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $training = Training::create($data);

        return new TrainingResource($training);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function show(TrainingShowRequest $request, Training $training)
    {
        return new TrainingResource($training);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function update(TrainingUpdateRequest $request, Training $training)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $training->update($data);

        return new TrainingResource($training);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrainingDestroyRequest $request, Training $training)
    {
        $training->delete();
    }
}
