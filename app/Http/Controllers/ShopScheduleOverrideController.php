<?php

namespace App\Http\Controllers;

use App\ShopSchedule;
use Illuminate\Http\Request;
use App\Http\Requests\ShopScheduleIndexRequest;
use App\Http\Requests\ShopScheduleStoreRequest;
use App\Http\Requests\ShopScheduleUpdateRequest;

class ShopScheduleOverrideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return \Illuminate\Http\Response
     */
    public function index(ShopScheduleIndexRequest $request, ShopSchedule $shopSchedule)
    {
        return $request->process($shopSchedule);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShopSchedule  $shopSchedule
     * @return \Illuminate\Http\Response
     */
    public function store(ShopScheduleStoreRequest $request, ShopSchedule $shopSchedule)
    {
        return $request->process($shopSchedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShopSchedule  $shopSchedule
     * @param  \App\ShopSchedule  $shopSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(ShopScheduleUpdateRequest $request, ShopSchedule $shopSchedule, ShopSchedule $override)
    {
        return $request->process($override, $shopSchedule);
    }
}
