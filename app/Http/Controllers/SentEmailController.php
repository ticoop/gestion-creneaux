<?php

namespace App\Http\Controllers;

use jdavidbakr\MailTracker\Model\SentEmail;
use Illuminate\Http\Request;
use App\Http\Requests\SentEmailIndexRequest;
use App\Http\Requests\SentEmailShowRequest;
use App\Http\Resources\SentEmail as SentEmailResource;

class SentEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SentEmailIndexRequest $request)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SentEmail  $sentEmail
     * @return \Illuminate\Http\Response
     */
    public function show(SentEmailShowRequest $request, SentEmail $sentEmail)
    {
        return new SentEmailResource($sentEmail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SentEmail  $sentEmail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SentEmail $sentEmail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SentEmail  $sentEmail
     * @return \Illuminate\Http\Response
     */
    public function destroy(SentEmail $sentEmail)
    {
        //
    }
}
