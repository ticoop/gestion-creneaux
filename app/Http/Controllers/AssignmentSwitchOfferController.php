<?php

namespace App\Http\Controllers;

use App\AssignmentSwitchOffer;
use App\AssignmentSwitchOfferReply;
use Illuminate\Http\Request;
use App\Http\Requests\AssignmentSwitchOfferIndexRequest;
use App\Http\Requests\AssignmentSwitchOfferStoreRequest;
use App\Http\Requests\AssignmentSwitchOfferDestroyRequest;
use App\Http\Requests\AssignmentSwitchOfferShowRequest;
use App\Http\Requests\AssignmentSwitchOfferReplyRequest;
use App\Http\Requests\AssignmentSwitchOfferAcceptReplyRequest;
use App\Http\Requests\AssignmentSwitchOfferRejectReplyRequest;
use App\Http\Requests\AssignmentSwitchOfferUpdateRequest;
use App\Http\Resources\AssignmentSwitchCollection as AssignmentSwitchCollectionResource;
use App\Http\Resources\AssignmentSwitchOffer as AssignmentSwitchOfferResource;
use App\Notifications\AssignmentSwitchOfferReplyAcceptedNotification;
use App\Notifications\NewAssignmentSwitchOfferReplyNotification;
use App\User;

class AssignmentSwitchOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AssignmentSwitchOfferIndexRequest $request)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentSwitchOfferStoreRequest $request)
    {
        $possibleReplacements = [];
        foreach ($request->possible_replacements as $slotId => $value) {
            $possibleReplacements[$slotId] = ["dates" => implode(",", $value['dates'])];
        }
        $data = array_intersect_key($request->input(), $request->rules());

        $actualAssignment = $request->user()
            ->actualAssignments()
            ->findOrFail($data['offer_actual_assignment_id']);
        
        unset($data['offer_actual_assignment_id']);

        if (!$request->ignore_existing_offers) {
            $existingOffers = AssignmentSwitchOffer::
                where(function ($q) {
                    $q->whereNull('target_user_id')->orWhere('target_user_id', auth()->user()->id);
                })
                ->where(function ($q) use ($possibleReplacements) {
                    foreach ($possibleReplacements as $slotId => $dates) {
                        $q->orWhere(function ($q2) use ($slotId, $dates) {
                            $q2->whereIn('offer_date', explode(",", $dates["dates"]))->whereHas('assignment', function ($q3) use ($slotId) {
                                $q3->where('shop_schedule_slot_id', $slotId);
                            });
                        });
                    }
                })
                ->doesntHave('acceptedReplies')
                ->with(['possibleReplacements', 'offerUser', 'targetUser', 'slot'])
                ->get()
                ->filter(function ($offer) use ($actualAssignment) {
                    foreach ($offer->possibleReplacements as $replacement) {
                        if ($replacement->pivot->shop_schedule_slot_id == $actualAssignment->assignment->shop_schedule_slot_id &&
                            collect(explode(",", $replacement->pivot->dates))->contains($actualAssignment->date_string)
                        ) {
                            return true;
                        }
                    }
                    return false;
                });
            if ($existingOffers->count() > 0) {
                return ['existing_offers' => new AssignmentSwitchCollectionResource($existingOffers)];
            }
        }

        if (isset($data['target_user_id'])) {
            User::findOrFail($data['target_user_id']);
        }

        $data['offer_date'] = $actualAssignment->date;
        $data['offer_user_id'] = $actualAssignment->user_id;

        $conflict = $actualAssignment
            ->assignment
            ->switchOffers()
            ->where('offer_date', $data['offer_date'])
            ->doesntHave('acceptedReplies')
            ->first();
        if (!!$conflict) {
            return response()->json([
                'errors' => [
                    'offer_actual_assignment_id' => __('assignment-switch-offers.offer-already-submitted'),
                ],
            ], 409);
        }

        $assignmentSwitchOffer = $actualAssignment->assignment->switchOffers()->create($data);

        $assignmentSwitchOffer->possibleReplacements()->sync($possibleReplacements);

        if (isset($data['target_user_id'])) {
            $assignmentSwitchOffer->notifyRecipients();
        }

        $assignmentSwitchOffer->load("possibleReplacements");

        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssignmentSwitchOffer  $assignmentSwitchOffer
     * @return \Illuminate\Http\Response
     */
    public function show(AssignmentSwitchOfferShowRequest $request, AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        if ($request->with_offer_user) {
            $assignmentSwitchOffer->load('offerUser');
        }
        
        if ($request->with_slot) {
            $assignmentSwitchOffer->load('slot');
        }

        if ($request->with_switchable_assignments) {
            $assignmentSwitchOffer->load('switchableAssignments.slot');
        }

        if ($request->with_pending_replies) {
            $assignmentSwitchOffer->load('pendingReplies');
        }

        if ($request->with_actual_assignment) {
            $assignmentSwitchOffer->append('actual_assignment');
        }

        $assignmentSwitchOffer->load('possibleReplacements');

        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssignmentSwitchOffer  $assignmentSwitchOffer
     * @return \Illuminate\Http\Response
     */
    public function update(AssignmentSwitchOfferUpdateRequest $request, AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        $possibleReplacements = [];
        foreach ($request->possible_replacements as $slotId => $value) {
            $possibleReplacements[$slotId] = ["dates" => implode(",", $value['dates'])];
        }
        $data = array_intersect_key($request->input(), $request->rules());

        $assignmentSwitchOffer->update($data);

        $assignmentSwitchOffer->possibleReplacements()->sync($possibleReplacements);

        $assignmentSwitchOffer->load("possibleReplacements");

        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssignmentSwitchOffer  $assignmentSwitchOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignmentSwitchOfferDestroyRequest $request, AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        $assignmentSwitchOffer->delete();
    }

    public function reply(AssignmentSwitchOfferReplyRequest $request, AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        if ($assignmentSwitchOffer->acceptedReplies()->count() > 0) {
            return response()->json([
                'errors' => [
                    'reply_actual_assignment_id' => __('assignment-switch-offers.no-longer-available'),
                ],
            ], 409);
        }

        if ($assignmentSwitchOffer->currentUserReplies()->whereNull('accepted')->count() > 0) {
            return response()->json([
                'errors' => [
                    'reply_actual_assignment_id' => __('assignment-switch-offers.already-replied', [
                        'offer_user_fullname' => $assignmentSwitchOffer->offerUser->fullname,
                    ]),
                ],
            ], 409);
        }

        if ($assignmentSwitchOffer->currentUserReplies()->where('accepted', 0)->count() > 0) {
            return response()->json([
                'errors' => [
                    'reply_actual_assignment_id' => __('assignment-switch-offers.already-rejected', [
                        'offer_user_fullname' => $assignmentSwitchOffer->offerUser->fullname,
                    ]),
                ],
            ], 409);
        }

        if (isset($data['reply_actual_assignment_id'])) {
            $actualAssignment = $request->user()
                ->actualAssignments()
                ->findOrFail($data['reply_actual_assignment_id']);
            unset($data['reply_actual_assignment_id']);
            $data['reply_assignment_id'] = $actualAssignment->assignment->id;
            $data['reply_date'] = $actualAssignment->date;
        } else {
            $data['reply_flying'] = true;
        }
        $data['reply_user_id'] = auth()->user()->id;

        $reply = $assignmentSwitchOffer->replies()->create($data);

        $assignmentSwitchOffer->offerUser->notify(new NewAssignmentSwitchOfferReplyNotification($reply));

        $assignmentSwitchOffer->load('currentUserReplies');

        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }

    public function cancelReply(AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        $assignmentSwitchOffer->currentUserReplies()->whereNull('accepted')->delete();

        $assignmentSwitchOffer->load('currentUserReplies');

        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }

    public function acceptReply(AssignmentSwitchOfferAcceptReplyRequest $request, AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        $reply = $assignmentSwitchOffer->pendingReplies()->findOrFail($request->reply);
        $reply->accepted = true;
        $reply->save();

        if (!!$reply->reply_assignment_id) {
            AssignmentSwitchOffer::where('offer_assignment_id', $reply->reply_assignment_id)
                ->where('offer_date', $reply->reply_date)
                ->where('id', '!=', $assignmentSwitchOffer->id)
                ->doesntHave('acceptedReplies')
                ->delete();
            AssignmentSwitchOfferReply::where('reply_assignment_id', $reply->reply_assignment_id)
                ->where('reply_date', $reply->reply_date)
                ->where('id', '!=', $reply->id)
                ->update(['accepted' => false]);
        }
        AssignmentSwitchOfferReply::where('reply_assignment_id', $assignmentSwitchOffer->offer_assignment_id)
            ->where('reply_date', $assignmentSwitchOffer->offer_date)
            ->where('id', '!=', $reply->id)
            ->update(['accepted' => false]);

        $assignmentSwitchOffer->pendingReplies()->update(['accepted' => false]);

        $assignmentSwitchOffer->assignment->switches()->where('date', $assignmentSwitchOffer->offer_date)->delete();
        $assignmentSwitchOffer->assignment->switches()->create([
            'date' => $assignmentSwitchOffer->offer_date,
            'replacement_user_id' => $reply->replyUser->id,
        ]);

        if (!!$reply->reply_assignment_id) {
            $reply->replyAssignment->switches()->where('date', $reply->reply_date)->delete();
            $reply->replyAssignment->switches()->create([
                'date' => $reply->reply_date,
                'replacement_user_id' => $assignmentSwitchOffer->offerUser->id,
            ]);
        } else {
            $assignmentSwitchOffer->offerUser->owedAssignments()->create([
                'initial_assignment_date' => $assignmentSwitchOffer->offer_date,
            ]);
        }

        $reply->replyUser->notify(new AssignmentSwitchOfferReplyAcceptedNotification($reply));
        
        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }

    public function rejectReply(AssignmentSwitchOfferRejectReplyRequest $request, AssignmentSwitchOffer $assignmentSwitchOffer)
    {
        $reply = $assignmentSwitchOffer->pendingReplies()->findOrFail($request->reply);
        $reply->accepted = false;
        $reply->save();

        $assignmentSwitchOffer->load('pendingReplies');
        
        return new AssignmentSwitchOfferResource($assignmentSwitchOffer);
    }
}
