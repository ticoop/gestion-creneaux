<?php

namespace App\Http\Controllers;

use App\LeaveType;
use Illuminate\Http\Request;
use App\Http\Requests\LeaveTypeIndexRequest;
use App\Http\Requests\LeaveTypeStoreRequest;
use App\Http\Requests\LeaveTypeShowRequest;
use App\Http\Requests\LeaveTypeUpdateRequest;
use App\Http\Requests\LeaveTypeDestroyRequest;
use App\Http\Resources\LeaveType as LeaveTypeResource;

class LeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LeaveTypeIndexRequest $request)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeaveTypeStoreRequest $request)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $leaveType = LeaveType::create($data);

        return new LeaveTypeResource($leaveType);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveTypeShowRequest $request, LeaveType $leaveType)
    {
        return new LeaveTypeResource($leaveType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function update(LeaveTypeUpdateRequest $request, LeaveType $leaveType)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $leaveType->update($data);

        return new LeaveTypeResource($leaveType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveTypeDestroyRequest $request, LeaveType $leaveType)
    {
        $leaveType->delete();
    }
}
