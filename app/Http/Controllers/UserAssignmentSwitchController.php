<?php

namespace App\Http\Controllers;

use App\AssignmentSwitch;
use App\Http\Requests\AssignmentSwitchIndexRequest;
use App\User;
use Illuminate\Http\Request;

class UserAssignmentSwitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function index(AssignmentSwitchIndexRequest $request, User $user)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, AssignmentSwitch $assignmentSwitch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, AssignmentSwitch $assignmentSwitch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, AssignmentSwitch $assignmentSwitch)
    {
        //
    }
}
