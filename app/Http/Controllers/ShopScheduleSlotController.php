<?php

namespace App\Http\Controllers;

use App\ShopScheduleSlot;
use App\ConfigItem;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\ShopScheduleSlotAssignFlyingRequest;
use App\Http\Requests\ShopScheduleSlotUnassignFlyingRequest;

class ShopScheduleSlotController extends Controller
{
    public function assignFlying(ShopScheduleSlotAssignFlyingRequest $request, ShopScheduleSlot $shopScheduleSlot)
    {
        $hasConflict = $shopScheduleSlot->actualAssignments()
            ->where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->count() > 0;
        if ($hasConflict) {
            return response()->json([
                'error' => __('slot-assignment.you-are-already-assigned-to-that-slot'),
            ], 409);
        }

        if (!auth()->user()->hasRole('admin-slot-assignment')) {
            $currentAssignmentCount = $shopScheduleSlot->actualAssignments()
                ->whereNull('assignment_id')
                ->where('date', $request->date)
                ->count();
            if ($currentAssignmentCount >= $shopScheduleSlot->flying_members_per_slot) {
                return response()->json([
                    'error' => __('slot-assignment.too-many-flying-assignments-that-slot'),
                ], 409);
            }
        }

        $data = [
            'user_id' => $request->user_id,
            'date' => $request->date,
            'start_at' => substr($request->date, 0, 10) . ' ' . $shopScheduleSlot->start_at,
            'end_at' => substr($request->date, 0, 10) . ' ' . $shopScheduleSlot->end_at,
            'referent' => false,
        ];

        $user = User::findOrFail($request->user_id);
        if (!$user->flying_team && !$shopScheduleSlot->volunteer) {
            $compensationFor = $user->actualAssignments()
                ->where('compensations_needed', '>', 0)
                ->withCount('compensations')
                ->orderby('start_at')
                ->get()
                ->filter(function ($a) {
                    return $a->compensations_count < $a->compensations_needed;
                })
                ->first();
            if (!!$compensationFor) {
                $data['compensates_actual_assignment_id'] = $compensationFor->id;
            } else {
                $owedAssignment = $user->owedAssignments()
                    ->doesntHave('actualAssignment')
                    ->orderBy('initial_assignment_date')
                    ->first();
                if (!!$owedAssignment) {
                    $data['owed_assignment_id'] = $owedAssignment->id;
                } elseif (!$request->admin || !$request->user()->hasRole('admin-slot-assignment')) {
                    return response()->json([
                        'error' => __('slot-assignment.no-compensations-needed'),
                    ], 403);
                }
            }
        }

        if (!$user->accounting_team && $shopScheduleSlot->accounting) {
            return response()->json([
                'error' => __('slot-assignment.not-in-accounting-team'),
            ], 403);
        }
        
        $shopScheduleSlot->actualAssignments()->create($data);
    }

    public function unassignFlying(ShopScheduleSlotUnassignFlyingRequest $request, ShopScheduleSlot $shopScheduleSlot)
    {
        $assignment = $shopScheduleSlot->actualAssignments()
            ->where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->firstOrFail();
        
        if ($assignment->assignment_id === null) {
            $assignment->delete();
        } elseif ($request->admin && $request->user()->hasRole('admin-slot-assignment')) {
            $assignment->assignment->switches()->create([
                'date' => $assignment->start_at,
                'replacement_user_id' => null,
            ]);
            $assignment->delete();
        } else {
            abort(403);
        }
    }
}
