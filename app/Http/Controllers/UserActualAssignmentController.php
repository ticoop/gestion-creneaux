<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\ActualAssignmentIndexRequest;

class UserActualAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function index(ActualAssignmentIndexRequest $request, User $user)
    {
        return $request->process();
    }
}
