<?php

namespace App\Http\Controllers;

use App\ActualAssignment;
use App\AssignmentSwitch;
use App\Http\Requests\GetAssignmentSwitchesStatsRequest;
use App\Http\Requests\GetAttendanceStatsRequest;
use App\Http\Requests\GetMembersAgeStatsRequest;
use App\Http\Requests\GetMembersGenderStatsRequest;
use App\Http\Requests\GetUsersStatsRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class StatsController extends Controller
{
    public function getAssignmentSwitchesStats(GetAssignmentSwitchesStatsRequest $request)
    {
        $dates = [];
        $date = now()->subMonths(12)->startOfMonth();
        while ($date->lte(now())) {
            $dates[] = $date->copy();
            $date->addMonth();
        }
        $res = [];
        foreach ($dates as $startDate) {
            $endDate = $startDate->copy()->endOfMonth();
            $switches = AssignmentSwitch::where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate)
                ->get();
            $res[] = [
                'year' => $startDate->year,
                'month' => $startDate->month,
                'data' => [
                    'nb_users' => $switches->pluck('replacement_user_id')->unique()->count(),
                    'count' => $switches->count() / 2,
                ],
            ];
        }
        return $res;
    }

    public function getUsersStats(GetUsersStatsRequest $request)
    {
        $cacheKey = 'StatsController::getUsersStats';
        $res = Cache::get($cacheKey);
        if (!$res) {
            $dates = [];
            $date = now()->subMonths(12)->endOfMonth();
            $minStartDate = config('app.min_user_start_date');
            if (!!$minStartDate) {
                $minStartDate = Carbon::parse($minStartDate);
            }
            while ($date->lt(now())) {
                if (!$minStartDate || $date->gte($minStartDate)) {
                    $dates[] = $date->copy();
                }
                $date = $date->startOfMonth()->addMonth()->endOfMonth();
            }
            $dates[] = now();
            $res = [];
            foreach ($dates as $i => $date) {
                $activeUsers = User::whereNotNull('start_date')
                    ->where('start_date', '<', $date)
                    ->where(function ($q) use ($date) {
                        $q->whereNull('end_date')->orWhere('end_date', '>=', $date->startOfDay());
                    })
                    ->with('assignments.slot.schedule')
                    ->get();
                $flyginTeam = $activeUsers->filter(function ($user) {
                    return $user->flying_team;
                });
                $assigned = $activeUsers->filter(function ($user) use ($date) {
                    return $user->assignments->filter(function ($a) use ($date) {
                        return $a->slot->schedule->start_at->isBefore($date) &&
                            $a->slot->schedule->end_at->isAfter($date->startOfDay());
                    })->count() > 0;
                });
                $byStatus = [];
                if ($i == count($dates) - 1) {
                    foreach (User::STATUSES as $key => $value) {
                        if ($value != 'inactive') {
                            $byStatus[$value] = 0;
                        }
                    }
                    foreach ($activeUsers as $user) {
                        $status = User::STATUSES[$user->status];
                        if ($status != 'inactive') {
                            $byStatus[$status]++;
                        }
                    }
                }
                $res[] = [
                    'date' => $date,
                    'data' => [
                        'active_count' => $activeUsers->count(),
                        'flying_count' => $flyginTeam->count(),
                        'assigned_count' => $assigned->count(),
                        'by_status' => $byStatus,
                    ],
                ];
            }
            Cache::put($cacheKey, $res, now()->addDay());
        }
        return $res;
    }

    public function getMembersAgeStats(GetMembersAgeStatsRequest $request)
    {
        $cacheKey = 'StatsController::getMembersAgeStats';
        $finalRes = Cache::get($cacheKey);
        if (!$finalRes) {
            $minAge = 18;
            $rangeWidth = 5;
            $users = User::whereNotNull('birth_date')->get()->filter(function ($u) use ($minAge) {
                return $u->active && $u->age >= $minAge;
            });
            $res = [];
            $sum = 0;
            foreach ($users as $user) {
                $from = floor($user->age / $rangeWidth) * $rangeWidth;
                if (!isset($res[$from])) {
                    $res[$from] = 0;
                }
                $res[$from]++;
                $sum += $user->age;
            }
            $min = collect(array_keys($res))->min();
            $max = collect(array_keys($res))->max();
            for ($i = $min; $i <= $max; $i += $rangeWidth) {
                if (!isset($res[$i])) {
                    $res[$i] = 0;
                }
            }
            $finalRes = [
                'distribution' => collect($res)
                    ->map(function ($count, $index) use ($rangeWidth) {
                        return [
                            'from' => $index,
                            'to' => $index + $rangeWidth - 1,
                            'count' => $count,
                        ];
                    })->sort(function ($a, $b) {
                        return $a['from'] - $b['from'];
                    })->values(),
                'average_age' => $sum / $users->count(),
            ];
            Cache::put($cacheKey, $finalRes, now()->addDay());
        }
        return $finalRes;
    }

    public function getMembersGenderStats(GetMembersGenderStatsRequest $request)
    {
        $cacheKey = 'StatsController::getMembersGenderStats';
        $finalRes = Cache::get($cacheKey);
        if (!$finalRes) {
            $users = User::has('gender')->with('gender')->get();
            $res = [];
            foreach ($users as $user) {
                if (!isset($res[$user->gender->label])) {
                    $res[$user->gender->label] = 0;
                }
                $res[$user->gender->label]++;
            }
            $finalRes = [];
            foreach ($res as $key => $value) {
                $finalRes[] = [
                    'label' => $key,
                    'count' => $value,
                ];
            }
            Cache::put($cacheKey, $finalRes, now()->addDay());
        }
        return $finalRes;
    }

    public function getAttendanceStats(GetAttendanceStatsRequest $request)
    {
        $assignments = ActualAssignment::where('start_at', '<', now()->startOfDay())
            ->where('start_at', '>=', now()->subYear()->startOfMonth())
            ->get();
        $startDate = $assignments->map(function ($a) {
            return $a->start_at;
        })->min()->startOfMonth();
        $byMonth = [];
        $d = $startDate->copy();
        while ($d->lte(now())) {
            $byMonth[$d->format('Y-m')] = [
                'done' => 0,
                'not_done' => 0,
                'total' => 0,
            ];
            $d->addMonth();
        }
        foreach ($assignments as $assignment) {
            $byMonth[$assignment->start_at->format('Y-m')]['total']++;
            if ($assignment->done) {
                $byMonth[$assignment->start_at->format('Y-m')]['done']++;
            } else {
                $byMonth[$assignment->start_at->format('Y-m')]['not_done']++;
            }
        }
        $res = collect();
        foreach ($byMonth as $month => $data) {
            $res->push([
                'month' => $month,
                'data' => $data,
            ]);
        }
        return $res->sort(function ($a, $b) {
            return strcasecmp($a['month'], $b['month']);
        });
    }
}
