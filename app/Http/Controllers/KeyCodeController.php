<?php

namespace App\Http\Controllers;

use App\Http\Requests\KeyCodeIndexRequest;

class KeyCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KeyCodeIndexRequest $request)
    {
        return $request->process();
    }
}
