<?php

namespace App\Http\Controllers;

use App\AssignmentWish;
use Illuminate\Http\Request;

class AssignmentWishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssignmentWish  $assignmentWish
     * @return \Illuminate\Http\Response
     */
    public function show(AssignmentWish $assignmentWish)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssignmentWish  $assignmentWish
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssignmentWish $assignmentWish)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssignmentWish  $assignmentWish
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignmentWish $assignmentWish)
    {
        //
    }
}
