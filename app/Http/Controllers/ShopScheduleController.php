<?php

namespace App\Http\Controllers;

use App\ShopSchedule;
use App\ShopScheduleSlot;
use App\Assignment;
use App\User;
use App\ConfigItem;
use Illuminate\Http\Request;
use App\Http\Requests\ShopScheduleComputeRequest;
use App\Http\Requests\ShopScheduleIndexRequest;
use App\Http\Requests\ShopScheduleStoreRequest;
use App\Http\Requests\ShopScheduleShowRequest;
use App\Http\Requests\ShopScheduleUpdateRequest;
use App\Http\Requests\ShopScheduleDestroyRequest;
use App\Http\Requests\ShopScheduleGetUserAssignmentsRequest;
use App\Http\Resources\AssignmentCollection as AssignmentCollectionResource;
use App\Http\Resources\ActualAssignment as ActualAssignmentResource;
use Carbon\Carbon;
use App\Http\Resources\ShopSchedule as ShopScheduleResource;
use App\Http\Resources\ShopScheduleSlotCollection as ShopScheduleSlotCollectionResource;

class ShopScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ShopScheduleIndexRequest $request)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopScheduleStoreRequest $request)
    {
        return $request->process();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(ShopScheduleShowRequest $request, ShopSchedule $shopSchedule)
    {
        $withChildren = $request->input('with_children', false);
        $withSlots = $request->input('with_slots', false);
        $withAssignments = $request->input('with_assignments', false);

        if ($withSlots) {
            if ($withAssignments) {
                $shopSchedule->load('slots.assignments.user.leaves');
            } else {
                $shopSchedule->load('slots');
            }
        }

        if ($withChildren) {
            if ($withSlots) {
                if ($withAssignments) {
                    $shopSchedule->load(['overrides' => function ($q) {
                        $q->with('slots.assignments.user.leaves')->with('overrides.slots.assignments.user.leaves');
                    }]);
                } else {
                    $shopSchedule->load(['overrides' => function ($q) {
                        $q->with('slots')->with('overrides.slots');
                    }]);
                }
            } else {
                $shopSchedule->load('overrides.overrides');
            }
        }
        
        return new ShopScheduleResource($shopSchedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShopSchedule  $shopSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(ShopScheduleUpdateRequest $request, ShopSchedule $shopSchedule)
    {
        return $request->process($shopSchedule);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopScheduleDestroyRequest $request, ShopSchedule $shopSchedule)
    {
        if ($shopSchedule->start_at->gt(now())) {
            $shopSchedule->delete();
        } else {
            abort(403);
        }
    }

    /**
     * Compute shop schedule for a period of time.
     *
     * @return \Illuminate\Http\Response
     */
    public function compute(ShopScheduleComputeRequest $request)
    {
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end);

        $schedules = ShopSchedule::where('start_at', '<=', $end)
            ->where('end_at', '>=', $start)
            ->whereNull('override_shop_schedule_id')
            ->with([
                'slots.actualAssignments' => function ($q) use ($request) {
                    if ($request->new_referent) {
                        $q->with('user.actualAssignments');
                    } else {
                        $q->with('user');
                    }
                    if ($request->incomplete || $request->with_switch_offers) {
                        $q->with([
                            'switchOffers' => function ($q2) {
                                $q2->doesntHave('acceptedReplies');
                            },
                        ]);
                    }
                }
            ])
            ->with([
                'overrides.slots.actualAssignments' => function ($q) use ($request) {
                    if ($request->new_referent) {
                        $q->with('user.actualAssignments');
                    } else {
                        $q->with('user');
                    }
                    if ($request->incomplete || $request->with_switch_offers) {
                        $q->with([
                            'switchOffers' => function ($q2) {
                                $q2->doesntHave('acceptedReplies');
                            },
                        ]);
                    }
                }
            ])
            ->with([
                'overrides.overrides.slots.actualAssignments' => function ($q) use ($request) {
                    if ($request->new_referent) {
                        $q->with('user.actualAssignments');
                    } else {
                        $q->with('user');
                    }
                    if ($request->incomplete || $request->with_switch_offers) {
                        $q->with([
                            'switchOffers' => function ($q2) {
                                $q2->doesntHave('acceptedReplies');
                            },
                        ]);
                    }
                }
            ])
            ->get();
        $res = collect();
        foreach ($schedules as $schedule) {
            foreach ($schedule->computeSlotsByDay($start, $end) as $dayString => $slots) {
                foreach ($slots as $slot) {
                    $assignments = new AssignmentCollectionResource($slot->actualAssignments->filter(function ($a) use ($dayString) {
                        return $a->date_string == $dayString;
                    })->merge([]));
                    if ($request->new_referent) {
                        $include = false;
                        $referentAssignments = $assignments->filter(function ($a) {
                            return $a->referent;
                        });
                        foreach ($referentAssignments as $referentAssignment) {
                            $previousAssignments = $referentAssignment->user->actualAssignments
                                ->filter(function ($a) use ($dayString, $referentAssignment) {
                                    return $a->date_string < $dayString && $a->referent;
                                });
                            if ($previousAssignments->count() == 0) {
                                $include = true;
                            }
                        }
                    } else {
                        $include = true;
                    }
                    if ($include) {
                        $res->push([
                            'shop_schedule_slot_id' => $slot->id,
                            'start' => $dayString . ' ' . $slot->start_at,
                            'end' => $dayString . ' ' . $slot->end_at,
                            'shop_open' => $slot->shop_open,
                            'comment' => $slot->comment,
                            'assignments' => $assignments,
                            'min_workers_per_slot' => $slot->min_workers_per_slot,
                            'max_workers_per_slot' => $slot->max_workers_per_slot,
                            'ideal_workers_per_slot' => $slot->ideal_workers_per_slot,
                            'flying_members_per_slot' => $slot->flying_members_per_slot,
                            'max_referents_per_slot' => $slot->max_referents_per_slot,
                            'volunteer' => $slot->volunteer,
                            'accounting' => $slot->accounting,
                        ]);
                    }
                }
            }
        }
        if ($request->incomplete) {
            $res = $res->filter(function ($s) {
                $nbAssignments = $s['assignments']->filter(function ($a) {
                    return $a->switchOffers->count() == 0;
                })->count();
                return $nbAssignments < $s['ideal_workers_per_slot'] && !$s['accounting'];
            })->merge([]);
        }
        return $res;
    }

    public function getCurrentUserAssignments(Request $request)
    {
        if ($request->user) {
            $this->authorize('admin-users');
            $user = User::findOrFail($request->user);
        } else {
            $user = $request->user();
        }

        $assignments = $user->assignments()->whereHas('slot.schedule', function ($q) {
            $q->where('start_at', '<=', now()->startOfDay())
                ->where('end_at', '>=', now()->startOfDay());
        })->with(['slot.schedule', 'referentUsers'])->get();

        $nextAssignment = $user->actualAssignments()
            ->where('end_at', '>=', now())
            ->orderBy('date')
            ->with('slot')
            ->first();
        if (!!$nextAssignment) {
            $nextAssignment->loadKeyCodes = true;
        }
        
        return [
            'assignments' => new AssignmentCollectionResource($assignments),
            'next_assignment' => !!$nextAssignment ? new ActualAssignmentResource($nextAssignment) : null,
        ];
    }

    public function getUserAssignments(ShopScheduleGetUserAssignmentsRequest $request, ShopSchedule $shopSchedule, User $user)
    {
        $assignments = $user->assignments()->whereHas('slot.schedule', function ($q) use ($shopSchedule) {
            $q->where('shop_schedule_id', $shopSchedule->id);
        })->with('slot')->get();

        return new ShopScheduleSlotCollectionResource($assignments);
    }

    public function getCurrentUserCompensations(Request $request)
    {
        $user = $request->user();

        $compensationsToPerform = $user->actualAssignments()
            ->where('compensations_needed', '>', 0)
            ->with('compensations')
            ->withCount('compensationsDone')
            ->get()
            ->filter(function ($a) {
                return $a->compensations_done_count < $a->compensations_needed;
            })
            ->map(function ($a) {
                $done = $a->compensations->filter(function ($c) {
                    return $c->done;
                })->count();
                $scheduled = $a->compensations->filter(function ($c) {
                    return !$c->done;
                })->count();
                $unscheduled = $a->compensations_needed - $done - $scheduled;
                return [
                    'assignment' => $a,
                    'compensations' => [
                        'done' => $done,
                        'scheduled' => $scheduled,
                        'unscheduled' => $unscheduled,
                    ],
                    'limit_date' => $a->start_at->addDays($a->compensation_delay),
                ];
            });
        
        return [
            'status' => User::STATUSES[$user->status],
            'compensations' => $compensationsToPerform,
        ];
    }
}
