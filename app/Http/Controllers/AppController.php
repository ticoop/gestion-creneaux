<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    /**
     * Display home page
     */
    public function showApp(Request $request)
    {
        return view('app');
    }
}
