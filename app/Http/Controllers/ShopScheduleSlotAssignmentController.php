<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopSchedule;
use App\ShopScheduleSlot;
use App\Assignment;
use App\Http\Requests\AssignmentStoreRequest;
use App\Http\Requests\AssignmentUpdateRequest;
use App\Http\Requests\AssignmentDestroyRequest;
use App\Http\Resources\Assignment as AssignmentResource;

class ShopScheduleSlotAssignmentController extends Controller
{
    public function store(AssignmentStoreRequest $request, ShopSchedule $shopSchedule, ShopScheduleSlot $slot)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $conflictResponse = $this->checkConflicts($slot, $data);
        if (!!$conflictResponse) {
            return $conflictResponse;
        }

        $assignment = $slot->assignments()->create($data);
        $assignment->load('user');

        return new AssignmentResource($assignment);
    }

    public function update(
        AssignmentUpdateRequest $request,
        ShopSchedule $shopSchedule,
        ShopScheduleSlot $slot,
        Assignment $assignment
    ) {
        $data = array_intersect_key($request->input(), $request->rules());

        $conflictResponse = $this->checkConflicts($slot, $data, $assignment);
        if (!!$conflictResponse) {
            return $conflictResponse;
        }

        $assignment->update($data);
        $assignment->load('user');

        return new AssignmentResource($assignment);
    }

    public function destroy(
        AssignmentDestroyRequest $request,
        ShopSchedule $shopSchedule,
        ShopScheduleSlot $slot,
        Assignment $assignment
    ) {
        $assignment->delete();
    }

    protected function checkConflicts(ShopScheduleSlot $slot, $data, Assignment $assignment = null)
    {
        $query = $slot->assignments()
            ->where('user_id', $data['user_id'])
            ->where('week_index', $data['week_index']);
        if ($assignment) {
            $query->where('id', '!=', $assignment->id);
        }
        $conflictCount = $query->count();
        if ($conflictCount > 0) {
            return response()->json([
                'error' => __('slot-assignment.cannot-assign-a-user-twice-on-same-slot'),
            ], 409);
        }
    }
}
