<?php

namespace App\Http\Controllers;

use App\DayKeyCode;
use App\Http\Requests\DayKeyCodeIndexRequest;
use App\Http\Requests\DayKeyCodeShowRequest;
use App\Http\Resources\DayKeyCode as DayKeyCodeResource;
use Illuminate\Http\Request;

class DayKeyCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DayKeyCodeIndexRequest $request)
    {
        return $request->process();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DayKeyCode  $dayKeyCode
     * @return \Illuminate\Http\Response
     */
    public function show(DayKeyCodeShowRequest $request, DayKeyCode $dayKeyCode)
    {
        return new DayKeyCodeResource($dayKeyCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DayKeyCode  $dayKeyCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DayKeyCode $dayKeyCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DayKeyCode  $dayKeyCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(DayKeyCode $dayKeyCode)
    {
        //
    }
}
