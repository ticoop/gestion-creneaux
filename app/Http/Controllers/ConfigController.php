<?php

namespace App\Http\Controllers;

use App\ConfigGroup;
use App\ConfigItem;
use Illuminate\Http\Request;
use App\Http\Requests\ConfigGroupShowRequest;
use App\Http\Requests\ConfigGroupSetValuesRequest;
use App\Http\Resources\ConfigGroup as ConfigGroupResource;
use App\Http\Resources\ConfigGroupCollection as ConfigGroupCollectionResource;

class ConfigController extends Controller
{
    public function showGroup(ConfigGroupShowRequest $request, ConfigGroup $configGroup)
    {
        $configGroup->load('items');
        $configGroup->load('children.items');

        return new ConfigGroupResource($configGroup);
    }

    public function setGroupValues(ConfigGroupSetValuesRequest $request, ConfigGroup $configGroup)
    {
        $items = $configGroup->items;
        foreach ($configGroup->children as $child) {
            $items = $items->merge($child->items);
        }

        foreach ($items as $item) {
            $key = $item->key;
            if ($request->has($key)) {
                $item->value = $request->$key;
                $item->save();
            }
        }
    }

    public function getAll()
    {
        return ConfigItem::all()->pluck('value', 'key');
    }
}
