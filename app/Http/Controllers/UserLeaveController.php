<?php

namespace App\Http\Controllers;

use App\Leave;
use App\User;
use App\LeaveType;
use Illuminate\Http\Request;
use App\Http\Requests\LeaveIndexRequest;
use App\Http\Requests\LeaveShowRequest;
use App\Http\Requests\LeaveUpdateRequest;
use App\Http\Requests\LeaveStoreRequest;
use App\Http\Requests\LeaveDestroyRequest;
use App\Http\Resources\Leave as LeaveResource;
use Carbon\Carbon;

class UserLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function index(LeaveIndexRequest $request, User $user)
    {
        return $request->process($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(LeaveStoreRequest $request, User $user)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $minDurationError = $this->checkMinDuration($data);
        if (!!$minDurationError) {
            return $minDurationError;
        }

        if (!$request->user()->hasRole('admin-leaves')) {
            $minNoticeError = $this->checkMinNotice($data);
            if (!!$minNoticeError) {
                return $minNoticeError;
            }

            $leaveType = LeaveType::findOrfail($data['leave_type_id']);
            if ($leaveType->requires_admin) {
                abort(403);
            }
        }

        $leave = $user->leaves()->create($data);

        return new LeaveResource($leave);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveShowRequest $request, User $user, Leave $leave)
    {
        return new LeaveResource($leave);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(LeaveUpdateRequest $request, User $user, Leave $leave)
    {
        $data = array_intersect_key($request->input(), $request->rules());

        $startAt = Carbon::parse($data['start_at']);
        $endAt = Carbon::parse($data['end_at']);

        if (!$request->user()->can('create', Leave::class)) {
            if ($leave->start_at->notEqualTo($startAt) &&
                $leave->start_at->lte(now()->startOfDay())
            ) {
                return response()->json([
                    'errors' => [
                        'start_at' => __('leaves.cannot-modify-start-date-of-started-leave'),
                    ],
                ], 422);
            }
            if ($leave->end_at->notEqualTo($endAt) &&
                $leave->end_at->lt(now()->startOfDay())
            ) {
                return response()->json([
                    'errors' => [
                        'end_at' => __('leaves.cannot-modify-end-date-of-ended-leave'),
                    ],
                ], 422);
            }
            if ($leave->end_at->notEqualTo($endAt) &&
                $endAt->lt(now()->startOfDay()) &&
                $endAt->lt($leave->end_at)
            ) {
                return response()->json([
                    'errors' => [
                        'end_at' => __('validation.custom.end_at.after_now'),
                    ],
                ], 422);
            }
            if ($leave->start_at->lte(now()->startOfDay()) && $data['leave_type_id'] !== $leave->leave_type_id) {
                return response()->json([
                    'errors' => [
                        'leave_type_id' => __('leaves.cannot-modify-leave-type-of-ended-leave'),
                    ],
                ], 422);
            }
        }

        $minDurationError = $this->checkMinDuration($data);
        if (!!$minDurationError) {
            return $minDurationError;
        }

        $minNoticeError = $this->checkMinNotice($data, $leave);
        if (!!$minNoticeError) {
            return $minNoticeError;
        }

        if (!$request->user()->hasRole('admin-leaves')) {
            $leaveType = LeaveType::findOrfail($data['leave_type_id']);
            if ($leaveType->requires_admin) {
                abort(403);
            }
        }

        $leave->update($data);

        $leave->load('leaveType');

        return new LeaveResource($leave);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveDestroyRequest $request, User $user, Leave $leave)
    {
        if ($request->user()->hasRole('admin-leaves') || $leave->start_at->gt(now())) {
            $leave->delete();
        } else {
            abort(403);
        }
    }

    protected function checkMinDuration(&$data)
    {
        $leaveType = LeaveType::findOrFail($data['leave_type_id']);
        $startAt = Carbon::parse($data['start_at']);
        $endAt = Carbon::parse($data['end_at']);

        if ($leaveType->min_duration > 0 && $endAt->addDay()->diffInWeeks($startAt) < $leaveType->min_duration) {
            return response()->json([
                'errors' => [
                    'end_at' => [
                        trans_choice(
                            'validation.leave_min_duration',
                            $leaveType->min_duration,
                            ['duration' => $leaveType->min_duration]
                        ),
                    ],
                ],
            ], 422);
        }
    }

    protected function checkMinNotice(&$data, Leave $leave = null)
    {
        $leaveType = LeaveType::findOrFail($data['leave_type_id']);
        $startAt = Carbon::parse($data['start_at']);

        if ($leave !== null && $startAt->gte($leave->start_at)) {
            return;
        }

        if ($leaveType->min_notice > 0 && $startAt->diffInWeeks(now()) < $leaveType->min_notice) {
            return response()->json([
                'errors' => [
                    'start_at' => [
                        trans_choice(
                            'validation.leave_min_notice',
                            $leaveType->min_notice,
                            ['delay' => $leaveType->min_notice]
                        ),
                    ],
                ],
            ], 422);
        }
    }
}
