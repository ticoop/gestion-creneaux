<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ActualAssignment;
use App\Http\Resources\User as UserResource;

class BadgingController extends Controller
{
    public function getUserByUsername(string $username)
    {
        $user = User::where('username', $username)->firstOrFail();
        return $this->getUser($user);
    }

    public function getUser(User $user)
    {
        $user->next_assigment = $user->actualAssignments()
            ->where('end_at', '>', now())
            ->orderBy('start_at')
            ->first();
        $user->append('status');

        return new UserResource($user);
    }

    public function startAssignment(ActualAssignment $actualAssignment)
    {
        if ($actualAssignment->startable) {
            $actualAssignment->start();
        } else {
            abort(422);
        }
    }
}
