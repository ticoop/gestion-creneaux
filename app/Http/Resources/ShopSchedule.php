<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopSchedule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (array_key_exists('overrides', $res)) {
            $res['overrides'] = new ShopScheduleCollection($this->overrides);
        }

        if (array_key_exists('slots', $res)) {
            $res['slots'] = new ShopScheduleSlotCollection($this->slots);
        }

        return $res;
    }
}
