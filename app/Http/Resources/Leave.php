<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Leave extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (array_key_exists('leave_type', $res)) {
            $res['leave_type'] = new LeaveType($this->leaveType);
        }

        if (array_key_exists('user', $res)) {
            $res['user'] = new User($this->user);
        }

        return $res;
    }
}
