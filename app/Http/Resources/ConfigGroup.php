<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConfigGroup extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);
        
        $res['label'] = $this->label;

        if (array_key_exists('children', $res)) {
            $res['children'] = new ConfigGroupCollection($this->children);
        }

        if (array_key_exists('items', $res)) {
            $res['items'] = new ConfigItemCollection($this->items);
        }
        
        return $res;
    }
}
