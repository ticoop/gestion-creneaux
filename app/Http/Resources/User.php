<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User as UserModel;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (array_key_exists('roles', $res)) {
            $res['roles'] = collect($res['roles'])->pluck('id')->toArray();
        }

        if (array_key_exists('trainings', $res)) {
            $res['trainings'] = collect($res['trainings'])->pluck('id')->toArray();
        }

        if (array_key_exists('next_assignment', $res) && !!$this->nextAssigment) {
            $res['next_assignment'] = new ActualAssignment($this->nextAssigment);
        }

        if (array_key_exists('current_assignments', $res)) {
            $res['current_assignments'] = new AssignmentCollection($this->currentAssignments);
        }

        if (array_key_exists('status', $res)) {
            $res['status'] = UserModel::STATUSES[$res['status']];
        }

        if (array_key_exists('owed_assignments', $res)) {
            $res['owed_assignments'] = new OwedAssignmentCollection($this->owedAssignments);
        }

        if ($this->id != auth()->user()->id && !$this->allow_phone_number_sharing && !auth()->user()->hasRole('admin-users')) {
            unset($res['phone']);
        }

        if ($this->id == auth()->user()->id) {
            $calendarUrl = \URL::signedRoute('user-calendar', ['user' => auth()->user()->id]);
            if (config('app.force_https') && substr($calendarUrl, 0, 7) == 'http://') {
                $calendarUrl = 'https://' . substr($calendarUrl, 7);
            }
            $res['calendar_url'] = $calendarUrl;
            $res['can_impersonate'] = auth()->user()->can('impersonate');
            $res['is_impersonating'] = !!session('impersonated_by');
            $nextAssignment = auth()->user()->actualAssignments()->where('start_at', '>=', now())->orderBy('start_at')->first();
            $res['next_assignment'] = !!$nextAssignment ? new ActualAssignment($nextAssignment) : null;
        }

        if (!$request->user()->hasRole('admin-users') && $this->id != auth()->user()->id) {
            $res = array_diff_key($res, [
                'oauth_id' => true,
                'email' => true,
                'start_date' => true,
                'created_at' => true,
                'updated_at' => true,
                'deleted_at' => true,
                'end_date' => true,
                'mattermost_access_token' => true,
                'note' => true,
                'search_text' => true,
                'birth_date' => true,
                'age' => true,
                'allow_phone_number_sharing' => true,
                'gender_id' => true,
                'shift_email_reminders' => true,
            ]);
        }

        if (isset($res['used_firstname'])) {
            $res['firstname'] = $res['used_firstname'];
        }

        return $res;
    }
}
