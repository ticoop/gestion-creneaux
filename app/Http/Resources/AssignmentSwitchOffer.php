<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AssignmentSwitchOffer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (isset($res['offer_user'])) {
            $res['offer_user'] = new User($this->offerUser);
        }
        
        if (isset($res['slot'])) {
            $res['slot'] = new ShopScheduleSlot($this->slot);
        }

        if (isset($res['possible_replacements'])) {
            $res['possible_replacements'] = new ActualAssignmentCollection($this->possibleReplacements);
        }

        if (isset($res['switchable_assignments'])) {
            $res['switchable_assignments'] = new ActualAssignmentCollection($this->switchableAssignments);
        }

        if (isset($res['replies'])) {
            $res['replies'] = new AssignmentSwitchOfferReplyCollection($this->replies);
        }

        if (isset($res['accepted_replies'])) {
            $res['accepted_replies'] = new AssignmentSwitchOfferReplyCollection($this->acceptedReplies);
        }

        if (isset($res['pending_replies'])) {
            $res['pending_replies'] = new AssignmentSwitchOfferReplyCollection($this->pendingReplies);
        }
        
        if (isset($res['current_user_replies'])) {
            $res['current_user_replies'] = new AssignmentSwitchOfferReplyCollection($this->currentUserReplies);
        }

        if (isset($res['actual_assignment'])) {
            $res['actual_assignment'] = new ActualAssignment($this->actual_assignment);
        }

        return $res;
    }
}
