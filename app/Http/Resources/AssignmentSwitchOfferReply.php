<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AssignmentSwitchOfferReply extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (isset($res['reply_user'])) {
            $res['reply_user'] = new User($this->replyUser);
        }

        if (isset($res['slot'])) {
            $res['slot'] = new ShopScheduleSlot($this->slot);
        }

        return $res;
    }
}
