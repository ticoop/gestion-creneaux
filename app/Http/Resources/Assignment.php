<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Assignment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (isset($res['user'])) {
            $res['user'] = new User($this->user);
        }

        if (isset($res['referent_users'])) {
            $res['referent_users'] = new UserCollection($this->referentUsers);
        }
        
        if (isset($res['slot'])) {
            $res['slot'] = new ShopScheduleSlot($this->slot);
        }

        return $res;
    }
}
