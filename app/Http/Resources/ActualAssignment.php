<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActualAssignment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (isset($res['user'])) {
            $res['user'] = new User($this->user);
        }

        if (isset($res['slot'])) {
            $res['slot'] = new ShopScheduleSlot($this->slot);
        }

        if (isset($res['parallel_assignments'])) {
            $res['parallel_assignments'] = new ActualAssignmentCollection($this->parallelAssignments);
            $referentAssignments = $this->parallelAssignments->filter(function ($a) {
                return $a->referent && $a->date->eq($this->date);
            });
            $res['referents'] = new UserCollection($referentAssignments->map(function ($a) {
                return $a->user;
            }));
            $otherAssignments = $this->parallelAssignments->filter(function ($a) {
                return !$a->referent && $a->date->eq($this->date);
            });
            $res['non_referents'] = new UserCollection($otherAssignments->map(function ($a) {
                return $a->user;
            }));
        }

        if ($this->loadKeyCodes && $this->referent && $this->start_at->isSameDay(now())) {
            $res['key_codes'] = $this->getKeyCodes();
        }

        return $res;
    }
}
