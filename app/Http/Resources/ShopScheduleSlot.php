<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\ConfigItem;

class ShopScheduleSlot extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = parent::toArray($request);

        if (array_key_exists('assignments', $res)) {
            $res['assignments'] = new UserCollection($this->assignments);
            $nbWeeks = ConfigItem::getCached('working_shift_frequency');
            $res['unassigned_count'] = $this->max_workers_per_slot * $nbWeeks - count($res['assignments']);
        }

        return $res;
    }
}
