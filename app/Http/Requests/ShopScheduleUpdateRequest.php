<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\ShopSchedule;
use App\Http\Resources\ShopSchedule as ShopScheduleResource;
use Carbon\Carbon;

class ShopScheduleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->override ?? $this->shop_schedule);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'start_at' => 'required|date',
            'end_at' => 'required|date|after_or_equal:start_at',
            'working_shift_frequency' => 'required|integer|min:1',
            'min_workers_per_slot' => 'integer|nullable',
            'max_workers_per_slot' => 'integer|nullable',
            'ideal_workers_per_slot' => 'integer|nullable',
            'flying_members_per_slot' => 'integer|nullable',
            'max_referents_per_slot' => 'integer|nullable',
            'slots' => 'array',
            'slots.*' => 'array',
            'slots.*.start_at' => 'regex:/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',
            'slots.*.end_at' => 'regex:/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',
            'slots.*.weekday' => 'integer|in:0,1,2,3,4,5,6',
            'slots.*.shop_open' => 'boolean',
            'slots.*.min_workers_per_slot' => 'integer|nullable',
            'slots.*.max_workers_per_slot' => 'integer|nullable',
            'slots.*.ideal_workers_per_slot' => 'integer|nullable',
            'slots.*.flying_members_per_slot' => 'integer|nullable',
            'slots.*.max_referents_per_slot' => 'integer|nullable',
            'slots.*.comment' => 'string|nullable',
            'slots.*.accounting' => 'boolean',
            'slots.*.generate_key_code' => 'boolean',
            'slots.*.special_slot_type' => 'string|in:inventory|nullable',
            'slots.*.frequency' => 'integer|nullable',
            'slots.*.start_date' => 'date|nullable',
            'slots.*.end_date' => 'date|nullable',
        ];
    }

    public function process(ShopSchedule &$shopSchedule, ShopSchedule &$parent = null)
    {
        $data = array_intersect_key($this->input(), $this->rules());

        $startAt = Carbon::parse($data['start_at'])->setTimezone(config('app.timezone'));
        $endAt = Carbon::parse($data['end_at'])->setTimezone(config('app.timezone'));
        $data['start_at'] = $startAt;
        $data['end_at'] = $endAt;
        
        if ($shopSchedule->start_at->notEqualTo($startAt) &&
            $shopSchedule->start_at->lte(now()->startOfDay())
        ) {
            return response()->json([
                'errors' => [
                    'start_at' => __('schedule.cannot-modify-start-date-of-started-schedule'),
                ],
            ], 422);
        }
        if ($shopSchedule->end_at->notEqualTo($endAt) &&
            $shopSchedule->end_at->lt(now()->startOfDay())
        ) {
            return response()->json([
                'errors' => [
                    'end_at' => __('schedule.cannot-modify-end-date-of-ended-schedule'),
                ],
            ], 422);
        }
        if ($shopSchedule->start_at->notEqualTo($startAt) &&
            $startAt->lt(now()->startOfDay())
        ) {
            return response()->json([
                'errors' => [
                    'start_at' => __('validation.custom.start_at.after_now'),
                ],
            ], 422);
        }
        if ($shopSchedule->end_at->notEqualTo($endAt) &&
            $endAt->lt(now()->startOfDay())
        ) {
            return response()->json([
                'errors' => [
                    'end_at' => __('validation.custom.end_at.after_now'),
                ],
            ], 422);
        }
        if ($shopSchedule->working_shift_frequency !== (int)$data['working_shift_frequency'] &&
            $shopSchedule->start_at->lte(now()->startOfDay())
        ) {
            return response()->json([
                'errors' => [
                    'working_shift_frequency' => __('schedule.cannot-modify-working-shift-frequency-of-started-schedule'),
                ],
            ], 422);
        }

        if (ShopSchedule::checkConflict($data, $shopSchedule, $parent)) {
            return response()->json([
                'errors' => [
                    'start_at' => [__('validation.shop_schedule_overlap')],
                    'end_at' => [__('validation.shop_schedule_overlap')],
                ],
            ], 409);
        }

        if (!!$parent) {
            $intervalErrors = [];
            if ($startAt->lt($parent->start_at)) {
                $intervalErrors['start_at'] = [__('validation.shop_schedule_override_starts_too_early')];
            }
            if ($endAt->gt($parent->end_at)) {
                $intervalErrors['end_at'] = [__('validation.shop_schedule_override_ends_too_late')];
            }
            if (count($intervalErrors) > 0) {
                return response()->json([
                    'errors' => $intervalErrors,
                ], 422);
            }
        }

        if (isset($data['slots'])) {
            $slots = collect($data['slots']);
            unset($data['slots']);
            $createSlots = $slots->filter(function ($s) {
                return $s['id'] < 0;
            })->map(function ($s) {
                unset($s['id']);
                return $s;
            });
            $updateSlots = $slots->filter(function ($s) {
                return $s['id'] > 0;
            });
            $keepSlots = $updateSlots->pluck('id');
            $shopSchedule->slots()->whereNotIn('id', $keepSlots)->delete();
            foreach ($updateSlots as $slot) {
                $shopSchedule->slots->find($slot['id'])->update($slot);
            }
            foreach ($createSlots as $slot) {
                $shopSchedule->slots()->create($slot);
            }
        }

        if (count($data) > 0) {
            $shopSchedule->update($data);
        }

        if ($shopSchedule->start_at->notEqualTo($startAt) || $shopSchedule->end_at->notEqualTo($endAt)) {
            $shopSchedule->slots()->each(function ($slot) {
                $slot->should_update_assignments = true;
                $slot->save();
            });
        }

        $shopSchedule->load('slots');

        return new ShopScheduleResource($shopSchedule);
    }
}
