<?php

namespace App\Http\Requests;

use App\AssignmentSwitch;
use App\Http\Resources\AssignmentSwitchCollection as AssignmentSwitchCollectionResource;
use Illuminate\Foundation\Http\FormRequest;

class AssignmentSwitchIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('admin-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'with_assignment' => 'boolean',
            'with_replacement_user' => 'boolean',
        ];
    }

    public function process()
    {
        $paginate = $this->input('paginate', false);
        $perPage = $this->input('per_page', 10);

        if (!!$this->user) {
            $query = $this->user->assignmentSwitches();
        } else {
            $query = AssignmentSwitch::query();
        }

        $query->has('replacementUser');

        if ($this->with_assignment) {
            $query->with(['assignment' => function ($q) {
                $q->with(['slot', 'user']);
            }]);
        }

        if ($this->with_replacement_user) {
            $query->with('replacementUser');
        }

        $query->orderBy('date', $this->input('order_dir', 'asc'));

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }

        return new AssignmentSwitchCollectionResource($res);
    }
}
