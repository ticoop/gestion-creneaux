<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignmentSwitchOfferShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'with_slot' => 'boolean',
            'with_offer_user' => 'boolean',
            'with_switchable_assignments' => 'boolean',
            'with_pending_replies' => 'boolean',
            'with_actual_assignment' => 'boolean',
        ];
    }
}
