<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeaveTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->leave_type);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'string',
            'shopping_allowed' => 'boolean',
            'requires_admin' => 'boolean',
            'min_duration' => 'nullable|integer|min:0',
            'min_notice' => 'nullable|integer|min:0',
        ];
    }
}
