<?php

namespace App\Http\Requests;

use App\Http\Resources\SentEmailCollection as SentEmailCollectionResource;
use jdavidbakr\MailTracker\Model\SentEmail;
use Illuminate\Foundation\Http\FormRequest;

class SentEmailIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole('see-sent-emails');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'order_by' => 'string|in:created_at',
            'order_dir' => 'string|in:asc,desc',
            'search' => 'string|nullable',
        ];
    }

    public function process()
    {
        $params = array_intersect_key($this->input(), $this->rules());

        $query = SentEmail::query();

        $paginate = $this->input('paginate', true);
        $perPage = $this->input('per_page', 10);
        $orderBy = $this->input('order_by', 'created_at');
        $orderDir = $this->input('order_dir', 'desc');

        foreach ($params as $key => $value) {
            switch ($key) {
                case 'search':
                    if ($value) {
                        $words = array_filter(explode(' ', $value), 'strlen');
                        foreach ($words as $w) {
                            $query->where(function ($q) use ($w) {
                                $q->where('recipient', 'ILIKE', '%' . $w . '%')
                                    ->orWhere('subject', 'ILIKE', '%' . $w . '%');
                            });
                        }
                    }
                    break;
            }
        }

        if ($orderBy) {
            switch ($orderBy) {
                default:
                    $query->orderBy($orderBy, $orderDir);
                    break;
            }
        }

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }
        
        return new SentEmailCollectionResource($res);
    }
}
