<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActualAssignmentShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->id == $this->actual_assignment->user_id ||
            $this->user()->hasRole('admin-slot-assignment');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
