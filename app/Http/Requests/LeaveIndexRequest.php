<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Leave;
use App\User;
use App\Http\Resources\LeaveCollection as LeaveCollectionResource;

class LeaveIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (!!$this->user && $this->user->id === $this->user()->id) || $this->user()->can('viewAny', Leave::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'order_by' => 'string|in:start_at,end_at,title',
            'order_dir' => 'string|in:asc,desc',
            'hide_passed' => 'boolean',
            'ending' => 'boolean',
            'with_user' => 'boolean',
        ];
    }

    public function process(User $user = null)
    {
        $params = array_intersect_key($this->input(), $this->rules());

        if ($user === null) {
            $query = Leave::query();
        } else {
            $query = $user->leaves();
        }

        $paginate = $this->input('paginate', true);
        $perPage = $this->input('per_page', 10);
        $orderBy = $this->input('order_by', null);
        $orderDir = $this->input('order_dir', 'asc');

        foreach ($params as $key => $value) {
            switch ($key) {
                case 'hide_passed':
                    if ($value) {
                        $query->where('end_at', '>=', now());
                    }
                    break;
                case 'ending':
                    if ($value) {
                        $query->where('end_at', '>', now())
                            ->where('end_at', '<=', now()->addMonth());
                    }
                    break;
                case 'with_user':
                    if ($value) {
                        $query->with('user');
                    }
                    break;
            }
        }

        if ($orderBy) {
            switch ($orderBy) {
                default:
                    $query->orderBy($orderBy, $orderDir);
                    break;
            }
        }

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }

        return new LeaveCollectionResource($res);
    }
}
