<?php

namespace App\Http\Requests;

use App\DayKeyCode;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Resources\DayKeyCodeCollection as DayKeyCodeCollectionResource;

class DayKeyCodeIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole('admin-key-codes');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
        ];
    }

    public function process()
    {
        $paginate = $this->input('paginate', true);
        $perPage = $this->input('per_page', 10);

        $query = DayKeyCode::orderBy("date", "DESC");

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }
        
        return new DayKeyCodeCollectionResource($res);
    }
}
