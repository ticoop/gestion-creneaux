<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActualAssignmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole('admin-slot-assignment') ||
            $this->user()->hasRole('admin-attendance') ||
            $this->actual_assignment->hasReferentUser($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $res = [];
        if ($this->user()->hasRole('admin-attendance')) {
            $res['done'] = 'boolean';
            $res['compensations_needed'] = 'integer|nullable';
            $res['compensation_delay'] = 'integer|nullable';
        }
        if ($this->user()->hasRole('admin-slot-assignment')) {
            $res['referent'] = 'boolean';
        }
        if ($this->actual_assignment->hasReferentUser($this->user()) && !$this->user()->hasRole('admin-attendance')) {
            $res['done'] = 'boolean';
        }
        return $res;
    }
}
