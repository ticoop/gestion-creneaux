<?php

namespace App\Http\Requests;

use App\Exports\UsersExport;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Maatwebsite\Excel\Facades\Excel;

class ExportUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole('admin-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users_with_note' => 'boolean',
            'users' => 'required_without:users_with_note|array',
            'users.*' => 'integer',
        ];
    }

    public function process()
    {
        if ($this->users_with_note) {
            $users = User::whereNotNUll('note')->get();
        } else {
            $users = User::whereIn('id', $this->users)->get();
        }
        return Excel::download(new UsersExport($users), 'users.ods', \Maatwebsite\Excel\Excel::ODS);
    }
}
