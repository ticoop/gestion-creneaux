<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\LeaveType;
use App\Http\Resources\LeaveTypeCollection as LeaveTypeCollectionResource;

class LeaveTypeIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('viewAny', LeaveType::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function process()
    {
        $query = LeaveType::orderByRaw("label->>'" . app()->getLocale() . "'");
        if (!$this->user()->hasRole('admin-leaves')) {
            $query->where('requires_admin', false);
        }

        $results = $query->get();
        
        return new LeaveTypeCollectionResource($results);
    }
}
