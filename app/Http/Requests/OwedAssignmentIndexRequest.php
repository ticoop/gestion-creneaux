<?php

namespace App\Http\Requests;

use App\Http\Resources\OwedAssignmentCollection as OwedAssignmentCollectionResource;
use App\OwedAssignment;
use Illuminate\Foundation\Http\FormRequest;

class OwedAssignmentIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !!$this->user && $this->user()->id == $this->user->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'order_by' => 'string|in:initial_assignment_date',
            'order_dir' => 'string|in:asc,desc',
        ];
    }

    public function process()
    {
        if (!!$this->user) {
            $query = $this->user->owedAssignments()->doesntHave('actualAssignment');
        } else {
            $query = OwedAssignment::query();
        }

        $paginate = $this->input('paginate', false);
        $perPage = $this->input('per_page', 10);

        $query->orderBy($this->input('order_by', 'initial_assignment_date'), $this->input('order_dir', 'asc'));

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }

        return new OwedAssignmentCollectionResource($res);
    }
}
