<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\ActualAssignment;
use App\Http\Resources\ActualAssignmentCollection as ActualAssignmentCollectionResource;

class ActualAssignmentIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (!!$this->user && $this->user->id === $this->user()->id) || $this->user()->can('admin-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'with_slot' => 'boolean',
            'with_parallel_assignments' => 'boolean',
            'past' => 'boolean',
            'future' => 'boolean',
            'current' => 'boolean',
            'referent' => 'boolean',
            'after' => 'date',
            'before' => 'date',
            'no_flying' => 'boolean',
            'order_dir' => 'string|in:asc,desc',
        ];
    }

    public function process()
    {
        $paginate = $this->input('paginate', false);
        $perPage = $this->input('per_page', 10);

        if (!!$this->user) {
            $query = $this->user->actualAssignments();
        } else {
            $query = ActualAssignment::query();
        }

        if ($this->with_slot) {
            $query->with('slot');
        }

        if ($this->with_parallel_assignments) {
            $query->with('parallelAssignments.user');
        }

        if ($this->future) {
            $query->where('date', '>=', now()->startOfDay());
        }

        if ($this->past) {
            $query->where('end_at', '<=', now());
        }

        if ($this->current) {
            $query->where('start_at', '<', now()->addHour())->where('end_at', '>', now()->subMinutes(30));
        }

        if ($this->after) {
            $query->where('date', '>=', $this->after);
        }

        if ($this->before) {
            $query->where('date', '<=', $this->before);
        }

        if ($this->no_flying) {
            $query->has('assignment');
        }

        if ($this->referent) {
            $query->where('referent', true);
        }

        $query->orderBy('date', $this->input('order_dir', 'asc'));

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }

        return new ActualAssignmentCollectionResource($res);
    }
}
