<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $res = [
            'rescue_team_days' => 'array|nullable',
            'rescue_team_days.*.id' => 'integer',
            'rescue_team_days.*.pivot' => 'array',
            'rescue_team_days.*.pivot.weekdays' => 'string',
            'shift_email_reminders' => 'boolean',
            'allow_phone_number_sharing' => 'boolean',
        ];
        if ($this->user()->hasRole('admin-users')) {
            $res = array_merge($res, [
                'roles' => 'array',
                'roles.*' => 'integer',
                'trainings' => 'array',
                'trainings.*' => 'integer',
                'referent' => 'boolean',
                'flying_team' => 'boolean',
                'start_date' => 'date|nullable',
                'end_date' => 'date|nullable',
                'accounting_team' => 'boolean',
                'service_account' => 'boolean',
                'emergency_team_days' => 'array|nullable',
                'emergency_team_days.*' => 'in:0am,0pm,0ev,1am,1pm,1ev,2am,2pm,2ev,3am,3pm,3ev,4am,4pm,4ev,5am,5pm,5ev,6am,6pm,6ev',
                'note' => 'string|nullable',
            ]);
        }
        return $res;
    }
}
