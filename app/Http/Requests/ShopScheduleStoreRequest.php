<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\ShopSchedule;
use App\Http\Resources\ShopSchedule as ShopScheduleResource;
use Carbon\Carbon;

class ShopScheduleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', ShopSchedule::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'start_at' => 'required|date|after:now',
            'end_at' => 'required|date|after_or_equal:start_at',
            'duplicate_schedule' => 'integer',
            'working_shift_frequency' => 'required_without:duplicate_schedule|integer|min:1',
            'min_workers_per_slot' => 'integer|nullable',
            'max_workers_per_slot' => 'integer|nullable',
            'ideal_workers_per_slot' => 'integer|nullable',
            'flying_members_per_slot' => 'integer|nullable',
            'max_referents_per_slot' => 'integer|nullable',
            'slots' => 'array',
            'slots.*' => 'array',
            'slots.*.start_at' => 'regex:/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',
            'slots.*.end_at' => 'regex:/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',
            'slots.*.weekday' => 'integer|in:0,1,2,3,4,5,6',
            'slots.*.shop_open' => 'boolean',
            'slots.*.min_workers_per_slot' => 'integer|nullable',
            'slots.*.max_workers_per_slot' => 'integer|nullable',
            'slots.*.ideal_workers_per_slot' => 'integer|nullable',
            'slots.*.flying_members_per_slot' => 'integer|nullable',
            'slots.*.max_referents_per_slot' => 'integer|nullable',
            'slots.*.comment' => 'string|nullable',
            'slots.*.accounting' => 'boolean',
            'slots.*.generate_key_code' => 'boolean',
            'slots.*.special_slot_type' => 'string|in:inventory|nullable',
            'slots.*.frequency' => 'integer|nullable',
            'slots.*.start_date' => 'date|nullable',
            'slots.*.end_date' => 'date|nullable',
        ];
    }

    public function process(ShopSchedule &$parent = null)
    {
        $data = array_intersect_key($this->input(), $this->rules());

        if (isset($data['duplicate_schedule'])) {
            $duplicateSchedule = ShopSchedule::/*with('slots.assignments')->*/findOrFail($data['duplicate_schedule']);
            $data = array_merge(
                array_intersect_key($duplicateSchedule->toArray(), $this->rules()),
                $data,
            );
        } else {
            $duplicateSchedule = null;
        }

        if (ShopSchedule::checkConflict($data, null, $parent)) {
            return response()->json([
                'errors' => [
                    'start_at' => [__('validation.shop_schedule_overlap')],
                    'end_at' => [__('validation.shop_schedule_overlap')],
                ],
            ], 409);
        }

        $startAt = Carbon::parse($data['start_at']);
        $endAt = Carbon::parse($data['end_at']);

        if (!!$parent) {
            $intervalErrors = [];
            if ($startAt->lt($parent->start_at)) {
                $intervalErrors['start_at'] = [__('validation.shop_schedule_override_starts_too_early')];
            }
            if ($endAt->gt($parent->end_at)) {
                $intervalErrors['end_at'] = [__('validation.shop_schedule_override_ends_too_late')];
            }
            if (count($intervalErrors) > 0) {
                return response()->json([
                    'errors' => $intervalErrors,
                ], 422);
            }
        }

        if (isset($data['slots'])) {
            $slots = $data['slots'];
            unset($data['slots']);
        } else {
            $slots = [];
        }

        if (!!$parent) {
            $schedule = $parent->overrides()->create($data);
        } else {
            $schedule = ShopSchedule::create($data);
        }

        if (count($slots) > 0) {
            foreach ($slots as $slot) {
                $schedule->slots()->create($slot);
            }
        }

        if ($duplicateSchedule !== null) {
            $duplicateSchedule->load('slots.assignments');
            $slotsDataFilter = [];
            foreach ($this->rules() as $key => $value) {
                if (substr($key, 0, 8) == 'slots.*.') {
                    $slotsDataFilter[substr($key, 8)] = true;
                }
            }
            foreach ($duplicateSchedule->slots as $slot) {
                $slotData = array_intersect_key($slot->toArray(), $slotsDataFilter);
                $newSlot = $schedule->slots()->create($slotData);
                foreach ($slot->assignments as $assignment) {
                    $assignmentData = $assignment->toArray();
                    unset($assignmentData['id']);
                    unset($assignmentData['created_at']);
                    unset($assignmentData['updated_at']);
                    unset($assignmentData['shop_schedule_slot_id']);
                    $newSlot->assignments()->create($assignmentData);
                }
            }
        }

        return new ShopScheduleResource($schedule);
    }
}
