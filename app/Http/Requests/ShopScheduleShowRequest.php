<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopScheduleShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('view', $this->shop_schedule);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'with_children' => 'boolean',
            'with_slots' => 'boolean',
            'with_assignments' => 'boolean',
        ];
    }
}
