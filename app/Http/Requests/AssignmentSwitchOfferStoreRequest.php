<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignmentSwitchOfferStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_actual_assignment_id' => 'required|integer',
            'possible_replacements' => 'required|array',
            'possible_replacements.*' => 'array',
            'possible_replacements.*.dates' => 'required|array',
            'possible_replacements.*.dates.*' => 'string',
            'comment' => 'string|nullable',
            'target_user_id' => 'integer|nullable',
            'ignore_existing_offers' => 'boolean',
        ];
    }
}
