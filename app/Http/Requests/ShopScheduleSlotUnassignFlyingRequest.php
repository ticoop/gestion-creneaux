<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopScheduleSlotUnassignFlyingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user_id === $this->user()->id || $this->user()->hasRole('admin-slot-assignment');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'date' => 'required|date' . ($this->user()->hasRole('admin-slot-assignment') ? '' : '|after_or_equal:today'),
            'admin' => 'boolean',
        ];
    }
}
