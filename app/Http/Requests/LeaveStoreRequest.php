<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Leave;

class LeaveStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !!$this->user && $this->user->id === $this->user()->id || $this->user()->can('create', Leave::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'leave_type_id' => 'required|integer',
            'start_at' => ($this->user()->can('create', Leave::class) ? 'required|date' : 'required|date|after:now'),
            'end_at' => 'required|date|after_or_equal:start_at',
            'comment' => 'string|nullable',
        ];
    }
}
