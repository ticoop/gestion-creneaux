<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\ShopSchedule;
use App\Http\Resources\ShopScheduleCollection as ShopScheduleCollectionResource;

class ShopScheduleIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('viewAny', ShopSchedule::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'order_by' => 'string|in:start_at,end_at,title',
            'order_dir' => 'string|in:asc,desc',
            'hide_passed' => 'boolean',
            'root_only' => 'boolean',
            'with_slots' => 'boolean',
            'with_slot_dates' => 'boolean',
        ];
    }

    public function process(ShopSchedule $parent = null)
    {
        $params = array_intersect_key($this->input(), $this->rules());

        $query = ShopSchedule::query();

        if ($parent !== null) {
            $query->where(function ($q) use ($parent) {
                $q->where('override_shop_schedule_id', $parent->id)
                    ->orWhereHas('parent', function ($q2) use ($parent) {
                        $q2->where('override_shop_schedule_id', $parent->id);
                    });
            });
        }

        $paginate = $this->input('paginate', !$parent);
        $perPage = $this->input('per_page', 10);
        $orderBy = $this->input('order_by', null);
        $orderDir = $this->input('order_dir', 'asc');

        foreach ($params as $key => $value) {
            switch ($key) {
                case 'hide_passed':
                    if ($value) {
                        $query->where('end_at', '>=', now());
                    }
                    break;
                case 'root_only':
                    if ($value) {
                        $query->whereNull('override_shop_schedule_id');
                    }
                    break;
                case 'with_slots':
                case 'with_slot_dates':
                    if ($value) {
                        $query->with('slots');
                        if ($this->with_slot_dates) {
                            $query->with('overrides.slots');
                        }
                    }
                    break;
                case 'paginate':
                case 'per_page':
                case 'order_by':
                case 'order_dir':
                    break;
            }
        }

        if ($orderBy) {
            switch ($orderBy) {
                default:
                    $query->orderBy($orderBy, $orderDir);
                    break;
            }
        }

        if ($paginate) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }

        if ($this->with_slot_dates) {
            foreach ($res as $schedule) {
                foreach ($schedule->slots as $slot) {
                    $slot->append('dates');
                }
            }
        }

        return new ShopScheduleCollectionResource($res);
    }
}
