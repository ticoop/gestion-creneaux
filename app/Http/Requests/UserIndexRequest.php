<?php

namespace App\Http\Requests;

use App\DayPart;
use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Http\Resources\UserCollection as UserCollectionResource;
use Carbon\Carbon;
use Exception;
use Illuminate\Pagination\Paginator;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;

class UserIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('viewAny', User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'order_by' => 'string|in:email,username,fullname,subscription_date',
            'order_dir' => 'string|in:asc,desc',
            'search' => 'string|nullable',
            'is_referent' => 'boolean',
            'accounting_team' => 'boolean',
            'emergency_team' => 'boolean',
            'current_emergency_team' => 'boolean',
            'with_roles' => 'boolean',
            'with_current_assignments' => 'boolean',
            'without_assignment' => 'boolean',
            'flying_without_assignment' => 'boolean',
            'referent' => 'boolean',
            'exemption' => 'boolean',
            'role' => 'integer',
            'slots' => 'array',
            'slots.*' => 'string',
            'flying_team' => 'integer',
            'status' => 'array',
            'status.*' => 'string|in:up-to-date,alert,suspended,blocked,inactive,leave-no-shopping,unsubscribed',
        ];
    }

    protected function getDayPart(Carbon $date)
    {
        return DayPart::where(function ($q) use ($date) {
            $q->whereNull('start_time')->orWhere('start_time', '<=', $date->format('H:i'));
        })->where(function ($q) use ($date) {
            $q->whereNull('end_time')->orWhere('end_time', '>', $date->format('H:i'));
        })->first();
    }

    public function process()
    {
        $params = array_intersect_key($this->input(), $this->rules());

        $query = User::query();
        
        $paginate = $this->input('paginate', true);
        $perPage = $this->input('per_page', 10);
        $orderBy = $this->input('order_by', null);
        $orderDir = $this->input('order_dir', 'asc');

        foreach ($params as $key => $value) {
            switch ($key) {
                case 'search':
                    if ($value) {
                        $query->whereMatchesText($value);
                    }
                    break;
                case 'is_referent':
                    if ($value) {
                        $query->where('referent', 1);
                    }
                    break;
                case 'accounting_team':
                    if ($value) {
                        $query->where('accounting_team', 1);
                    }
                    break;
                case 'emergency_team':
                    if ($value) {
                        $query->whereNotNull('emergency_team_days');
                    }
                    break;
                case 'current_emergency_team':
                    if ($value) {
                        $key = now()->dayOfWeek . $this->getDayPart(now())->short_key;
                        $query->whereNotNull('emergency_team_days')->where('emergency_team_days', 'LIKE', '%' . $key . '%');
                    }
                    break;
                case 'with_roles':
                    if ($value) {
                        $query->with('roles');
                    }
                    break;
                case 'with_current_assignments':
                    if ($value) {
                        $query->with('currentAssignments.slot');
                    }
                    break;
                case 'referent':
                    $query->where('referent', $value);
                    break;
                case 'exemption':
                    if ($value) {
                        $query->whereHas('leaves', function ($q) {
                            $q->whereHas('leaveType', function ($q2) {
                                $q2->where('shopping_allowed', true);
                            })->where('start_at', '<=', now()->startOfDay())->where('end_at', '>=', now()->startOfDay());
                        });
                    } else {
                        $query->whereDoesntHave('leaves', function ($q) {
                            $q->whereHas('leaveType', function ($q2) {
                                $q2->where('shopping_allowed', true);
                            })->where('start_at', '<=', now()->startOfDay())->where('end_at', '>=', now()->startOfDay());
                        });
                    }
                    break;
                case 'flying_team':
                    $query->where('flying_team', $value);
                    break;
                case 'role':
                    if ($value) {
                        $query->whereHas('roles', function ($q) use ($value) {
                            $q->where('id', $value)->orWhere('key', 'admin');
                        });
                    }
                    break;
                case 'slots':
                    if ($value) {
                        $query->where(function ($q) use ($value) {
                            foreach ($value as $slot) {
                                list($slotId, $weekIndex) = explode('_', $slot);
                                $q->orWhereHas('assignments', function ($q) use ($slotId, $weekIndex) {
                                    $q->where('shop_schedule_slot_id', $slotId)->where('week_index', $weekIndex);
                                });
                            }
                        });
                    }
                    break;
                case 'status':
                    if (!!$value) {
                        $query->with([
                            'currentNoShoppingAllowedLeaves',
                            'doneNotCompensationActualAssignments',
                            'actualAssignmentsWithCompensationNeeded',
                        ]);
                    }
                    break;
                case 'without_assignment':
                    if ($value) {
                        $query->whereActive()
                            ->where('flying_team', false)
                            ->whereDoesntHave('assignments.slot.schedule', function ($q) {
                                $q->where('start_at', '<=', now()->startOfDay())
                                    ->where('end_at', '>=', now()->startOfDay());
                            });
                    }
                    break;
                case 'flying_without_assignment':
                    if ($value) {
                        $query->whereActive()
                            ->where('flying_team', true)
                            ->whereDoesntHave('actualAssignments', function ($q) {
                                $q->where('done', true);
                            });
                    }
                    break;
            }
        }

        if ($orderBy) {
            switch ($orderBy) {
                case 'fullname':
                    $query->orderBy('firstname', $orderDir)->orderBy('lastname', $orderDir);
                    break;
                default:
                    $query->orderBy($orderBy, $orderDir);
                    break;
            }
        }

        if ($paginate && !isset($params['status'])) {
            $res = $query->paginate($perPage);
        } else {
            $res = $query->get();
        }

        if (isset($params['status'])) {
            $res = $res->filter(function ($u) use ($params) {
                return collect($params['status'])->contains(User::STATUSES[$u->status]);
            });
            if ($paginate) {
                $items = $res->slice(($this->input('page', 1) - 1) * $perPage, $perPage);
                $total = $res->count();
                $currentPage = $this->input('page', 1);
                $res = Container::getInstance()->makeWith(
                    LengthAwarePaginator::class,
                    compact('items', 'total', 'perPage', 'currentPage')
                );
            }
        }

        foreach ($res as $user) {
            $user->append('status');
        }

        return new UserCollectionResource($res);
    }
}
