<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\AssignmentSwitchOffer;
use App\Http\Resources\AssignmentSwitchOfferCollection as AssignmentSwitchOfferCollectionResource;

class AssignmentSwitchOfferIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer|min:1',
            'paginate' => 'boolean',
            'per_page' => 'integer|min:1,max:100',
            'order_by' => 'string|in:offer_date',
            'order_dir' => 'string|in:asc,desc',
            'with_slot' => 'boolean',
            'with_offer_user' => 'boolean',
            'offer_user_id' => 'integer',
            'switchable' => 'boolean',
            'with_pending_replies' => 'boolean',
            'with_current_user_replies' => 'boolean',
            'pending_reply_confirmation' => 'boolean',
            'active' => 'boolean',
            'without_accepted_reply' => 'boolean',
        ];
    }

    public function process()
    {
        $query = AssignmentSwitchOffer::query();

        $paginate = $this->input('paginate', true);
        $perPage = $this->input('per_page', 10);
        $orderBy = $this->input('order_by', 'offer_date');
        $orderDir = $this->input('order_dir', 'asc');

        if ($this->with_slot) {
            $query->with('slot');
        }

        if ($this->with_offer_user) {
            $query->with('offerUser');
        }

        if ($this->offer_user_id) {
            $query->where('offer_user_id', $this->offer_user_id);
        }

        if ($this->switchable) {
            $query->where('offer_user_id', '!=', auth()->user()->id)
                ->doesntHave('acceptedReplies')
                ->where(function ($q) {
                    $q->whereNull('target_user_id')->orWhere('target_user_id', $this->user()->id);
                })
                ->with([
                    'slot',
                    'possibleReplacements.actualAssignments' => function ($q) {
                        $q->whereNotNull('assignment_id')->where('user_id', auth()->user()->id);
                    },
                ]);
        }

        if ($this->pending_reply_confirmation) {
            $query->whereHas('currentUserReplies', function ($q) {
                $q->whereNull('accepted');
            });
        }

        if ($this->with_current_user_replies) {
            $query->with('currentUserReplies');
        }

        if ($this->with_pending_replies) {
            $query->with('pendingReplies');
        }

        if ($this->active) {
            $query->doesntHave('acceptedReplies')->where('offer_date', '>=', now()->startOfDay());
        }

        if ($this->without_accepted_reply) {
            $query->doesntHave('acceptedReplies');
        }

        if ($orderBy) {
            switch ($orderBy) {
                default:
                    $query->orderBy($orderBy, $orderDir);
                    break;
            }
        }

        if ($this->switchable) {
            $exclude = $this->user()->actualAssignments->map(function ($a) {
                return $a->shop_schedule_slot_id . "_" . substr($a->start_at, 0, 10);
            });
        }

        if ($paginate) {
            $res = $query->paginate($perPage);
            if ($this->switchable) {
                $collection = $res->getCollection();
                $filteredCollection = $this->filterSwitchableAssignments($collection, $exclude);
                $res->setCollection($filteredCollection);
            }
        } else {
            $res = $query->get();
            if ($this->switchable) {
                $res = $this->filterSwitchableAssignments($res, $exclude);
            }
        }

        return new AssignmentSwitchOfferCollectionResource($res);
    }

    protected function filterSwitchableAssignments($collection, $exclude)
    {
        return $collection->filter(function ($model) use ($exclude) {
            if (auth()->user()->flying_team) {
                $actualSwitchableAssignments = $model->possibleReplacements;
            } else {
                $actualSwitchableAssignments = $model->possibleReplacements->filter(function ($r) {
                    return $r->actualAssignments->filter(function ($a) use ($r) {
                        return $a->user_id == auth()->user()->id &&
                            $a->date->gt(now()) &&
                            collect(explode(",", $r->pivot->dates))->contains($a->date_string);
                    })->count() > 0;
                });
            }
            return $actualSwitchableAssignments->count() > 0 &&
                !$exclude->contains($model->slot->id . '_' . substr($model->offer_date, 0, 10));
        });
    }
}
