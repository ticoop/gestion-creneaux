<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignmentSwitchOfferUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->assignment_switch_offer->offer_user_id === $this->user()->id &&
            $this->assignment_switch_offer->acceptedReplies->count() == 0;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'possible_replacements' => 'required|array',
            'possible_replacements.*' => 'array',
            'possible_replacements.*.dates' => 'required|array',
            'possible_replacements.*.dates.*' => 'string',
            'comment' => 'string|nullable',
        ];
    }
}
