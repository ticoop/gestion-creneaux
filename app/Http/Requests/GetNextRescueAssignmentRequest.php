<?php

namespace App\Http\Requests;

use App\RescueAssignment;
use Illuminate\Foundation\Http\FormRequest;

class GetNextRescueAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('viewAny', RescueAssignment::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'period' => 'string|in:now,next-assignment',
        ];
    }
}
