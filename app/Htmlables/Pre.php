<?php

namespace App\Htmlables;

use Illuminate\Contracts\Support\Htmlable;

class Pre implements Htmlable
{
    protected $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Get content as a string of HTML.
     *
     * @return string
     */
    public function toHtml()
    {
        return '<pre>' . $this->content . '</pre>';
    }
}
