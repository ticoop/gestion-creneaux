<?php

namespace App\Policies;

use App\LeaveType;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeaveTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any leave types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the leave type.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function view(User $user, LeaveType $leaveType)
    {
        return $user->hasRole('admin-leave-types');
    }

    /**
     * Determine whether the user can create leave types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('admin-leave-types');
    }

    /**
     * Determine whether the user can update the leave type.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function update(User $user, LeaveType $leaveType)
    {
        return $user->hasRole('admin-leave-types');
    }

    /**
     * Determine whether the user can delete the leave type.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function delete(User $user, LeaveType $leaveType)
    {
        return $user->hasRole('admin-leave-types');
    }

    /**
     * Determine whether the user can restore the leave type.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function restore(User $user, LeaveType $leaveType)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the leave type.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function forceDelete(User $user, LeaveType $leaveType)
    {
        return false;
    }
}
