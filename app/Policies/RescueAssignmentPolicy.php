<?php

namespace App\Policies;

use App\RescueAssignment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RescueAssignmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any rescue assignments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('view-rescue-assignments') ||
            $user->isWorking() ||
            $user->hasUpcomingReferentAssignment();
    }

    /**
     * Determine whether the user can view the rescue assignment.
     *
     * @param  \App\User  $user
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return mixed
     */
    public function view(User $user, RescueAssignment $rescueAssignment)
    {
        return $user->hasRole('view-rescue-assignments') ||
            $user->isWorking() ||
            $user->hasUpcomingReferentAssignment();
    }

    /**
     * Determine whether the user can create rescue assignments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the rescue assignment.
     *
     * @param  \App\User  $user
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return mixed
     */
    public function update(User $user, RescueAssignment $rescueAssignment)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the rescue assignment.
     *
     * @param  \App\User  $user
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return mixed
     */
    public function delete(User $user, RescueAssignment $rescueAssignment)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the rescue assignment.
     *
     * @param  \App\User  $user
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return mixed
     */
    public function restore(User $user, RescueAssignment $rescueAssignment)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the rescue assignment.
     *
     * @param  \App\User  $user
     * @param  \App\RescueAssignment  $rescueAssignment
     * @return mixed
     */
    public function forceDelete(User $user, RescueAssignment $rescueAssignment)
    {
        return false;
    }
}
