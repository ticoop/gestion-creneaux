<?php

namespace App\Policies;

use App\ConfigGroup;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConfigGroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any config groups.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the config group.
     *
     * @param  \App\User  $user
     * @param  \App\ConfigGroup  $configGroup
     * @return mixed
     */
    public function view(User $user, ConfigGroup $configGroup)
    {
        if ($configGroup->admin_user_role) {
            return $user->hasRole($configGroup->admin_user_role);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can create config groups.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the config group.
     *
     * @param  \App\User  $user
     * @param  \App\ConfigGroup  $configGroup
     * @return mixed
     */
    public function update(User $user, ConfigGroup $configGroup)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the config group.
     *
     * @param  \App\User  $user
     * @param  \App\ConfigGroup  $configGroup
     * @return mixed
     */
    public function delete(User $user, ConfigGroup $configGroup)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the config group.
     *
     * @param  \App\User  $user
     * @param  \App\ConfigGroup  $configGroup
     * @return mixed
     */
    public function restore(User $user, ConfigGroup $configGroup)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the config group.
     *
     * @param  \App\User  $user
     * @param  \App\ConfigGroup  $configGroup
     * @return mixed
     */
    public function forceDelete(User $user, ConfigGroup $configGroup)
    {
        return false;
    }
}
