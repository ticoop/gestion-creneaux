<?php

namespace App\Policies;

use App\ShopSchedule;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShopSchedulePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any shop schedules.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the shop schedule.
     *
     * @param  \App\User  $user
     * @param  \App\ShopSchedule  $shopSchedule
     * @return mixed
     */
    public function view(User $user, ShopSchedule $shopSchedule)
    {
        return $user->hasRole('admin-shop-schedule');
    }

    /**
     * Determine whether the user can create shop schedules.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('admin-shop-schedule');
    }

    /**
     * Determine whether the user can update the shop schedule.
     *
     * @param  \App\User  $user
     * @param  \App\ShopSchedule  $shopSchedule
     * @return mixed
     */
    public function update(User $user, ShopSchedule $shopSchedule)
    {
        return $user->hasRole('admin-shop-schedule');
    }

    /**
     * Determine whether the user can delete the shop schedule.
     *
     * @param  \App\User  $user
     * @param  \App\ShopSchedule  $shopSchedule
     * @return mixed
     */
    public function delete(User $user, ShopSchedule $shopSchedule)
    {
        return $user->hasRole('admin-shop-schedule');
    }

    /**
     * Determine whether the user can restore the shop schedule.
     *
     * @param  \App\User  $user
     * @param  \App\ShopSchedule  $shopSchedule
     * @return mixed
     */
    public function restore(User $user, ShopSchedule $shopSchedule)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the shop schedule.
     *
     * @param  \App\User  $user
     * @param  \App\ShopSchedule  $shopSchedule
     * @return mixed
     */
    public function forceDelete(User $user, ShopSchedule $shopSchedule)
    {
        return false;
    }
}
