<?php

namespace App\Policies;

use App\Leave;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeavePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any leaves.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('admin-leaves');
    }

    /**
     * Determine whether the user can view the leave.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function view(User $user, Leave $leave)
    {
        return $user->hasRole('admin-leaves') || $user->id === $leave->user_id;
    }

    /**
     * Determine whether the user can create leaves.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('admin-leaves');
    }

    /**
     * Determine whether the user can update the leave.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function update(User $user, Leave $leave)
    {
        return $user->hasRole('admin-leaves') || $user->id === $leave->user_id;
    }

    /**
     * Determine whether the user can delete the leave.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function delete(User $user, Leave $leave)
    {
        return $user->hasRole('admin-leaves') || $user->id === $leave->user_id;
    }

    /**
     * Determine whether the user can restore the leave.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function restore(User $user, Leave $leave)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the leave.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function forceDelete(User $user, Leave $leave)
    {
        return false;
    }
}
