<?php

namespace App\Observers;

use App\AssignmentSwitch;

class AssignmentSwitchObserver
{
    /**
     * Handle the assignment switch "created" event.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return void
     */
    public function created(AssignmentSwitch $assignmentSwitch)
    {
        $assignmentSwitch->assignment->slot->should_update_assignments = true;
        $assignmentSwitch->assignment->slot->save();
    }

    /**
     * Handle the assignment switch "updated" event.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return void
     */
    public function updated(AssignmentSwitch $assignmentSwitch)
    {
        $assignmentSwitch->assignment->slot->should_update_assignments = true;
        $assignmentSwitch->assignment->slot->save();
    }

    /**
     * Handle the assignment switch "deleted" event.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return void
     */
    public function deleted(AssignmentSwitch $assignmentSwitch)
    {
        $assignmentSwitch->assignment->slot->should_update_assignments = true;
        $assignmentSwitch->assignment->slot->save();
    }

    /**
     * Handle the assignment switch "restored" event.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return void
     */
    public function restored(AssignmentSwitch $assignmentSwitch)
    {
        //
    }

    /**
     * Handle the assignment switch "force deleted" event.
     *
     * @param  \App\AssignmentSwitch  $assignmentSwitch
     * @return void
     */
    public function forceDeleted(AssignmentSwitch $assignmentSwitch)
    {
        //
    }
}
