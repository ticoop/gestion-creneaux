<?php

namespace App\Observers;

use App\ShopSchedule;

class ShopScheduleObserver
{
    /**
     * Handle the shop schedule "created" event.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return void
     */
    public function created(ShopSchedule $shopSchedule)
    {
        //
    }

    /**
     * Handle the shop schedule "updated" event.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return void
     */
    public function updated(ShopSchedule $shopSchedule)
    {
        //
    }

    /**
     * Handle the shop schedule "deleted" event.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return void
     */
    public function deleted(ShopSchedule $shopSchedule)
    {
        $shopSchedule->slots->each(function ($slot) {
            $slot->delete();
        });
        $parent = $shopSchedule->parent;
        while (!!$parent) {
            $parent->slots()->update(["should_update_assignments" => true]);
            $parent = $parent->parent;
        }
    }

    /**
     * Handle the shop schedule "restored" event.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return void
     */
    public function restored(ShopSchedule $shopSchedule)
    {
        //
    }

    /**
     * Handle the shop schedule "force deleted" event.
     *
     * @param  \App\ShopSchedule  $shopSchedule
     * @return void
     */
    public function forceDeleted(ShopSchedule $shopSchedule)
    {
        //
    }
}
