<?php

namespace App\Observers;

use App\ShopScheduleSlot;

class ShopScheduleSlotObserver
{
    /**
     * Handle the shop schedule slot "created" event.
     *
     * @param  \App\ShopScheduleSlot  $shopScheduleSlot
     * @return void
     */
    public function created(ShopScheduleSlot $shopScheduleSlot)
    {
        //
    }

    /**
     * Handle the shop schedule slot "updated" event.
     *
     * @param  \App\ShopScheduleSlot  $shopScheduleSlot
     * @return void
     */
    public function updated(ShopScheduleSlot $shopScheduleSlot)
    {
        //
    }

    /**
     * Handle the shop schedule slot "deleted" event.
     *
     * @param  \App\ShopScheduleSlot  $shopScheduleSlot
     * @return void
     */
    public function deleted(ShopScheduleSlot $shopScheduleSlot)
    {
        $shopScheduleSlot->assignments()->delete();
        $shopScheduleSlot->actualAssignments()->delete();
    }

    /**
     * Handle the shop schedule slot "restored" event.
     *
     * @param  \App\ShopScheduleSlot  $shopScheduleSlot
     * @return void
     */
    public function restored(ShopScheduleSlot $shopScheduleSlot)
    {
        //
    }

    /**
     * Handle the shop schedule slot "force deleted" event.
     *
     * @param  \App\ShopScheduleSlot  $shopScheduleSlot
     * @return void
     */
    public function forceDeleted(ShopScheduleSlot $shopScheduleSlot)
    {
        //
    }
}
