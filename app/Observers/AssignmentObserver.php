<?php

namespace App\Observers;

use App\Assignment;

class AssignmentObserver
{
    /**
     * Handle the shop schedule slot user "created" event.
     *
     * @param  \App\Assignment  $assignment
     * @return void
     */
    public function created(Assignment $assignment)
    {
        $assignment->slot->should_update_assignments = true;
        $assignment->slot->save();
    }

    /**
     * Handle the shop schedule slot user "updated" event.
     *
     * @param  \App\Assignment  $assignment
     * @return void
     */
    public function updated(Assignment $assignment)
    {
        $assignment->slot->should_update_assignments = true;
        $assignment->slot->save();
    }

    /**
     * Handle the shop schedule slot user "deleted" event.
     *
     * @param  \App\Assignment  $assignment
     * @return void
     */
    public function deleted(Assignment $assignment)
    {
        $assignment->slot->should_update_assignments = true;
        $assignment->slot->save();
    }

    /**
     * Handle the shop schedule slot user "restored" event.
     *
     * @param  \App\Assignment  $assignment
     * @return void
     */
    public function restored(Assignment $assignment)
    {
        $assignment->slot->should_update_assignments = true;
        $assignment->slot->save();
    }

    /**
     * Handle the shop schedule slot user "force deleted" event.
     *
     * @param  \App\Assignment  $assignment
     * @return void
     */
    public function forceDeleted(Assignment $assignment)
    {
        $assignment->slot->should_update_assignments = true;
        $assignment->slot->save();
    }
}
