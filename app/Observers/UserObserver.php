<?php

namespace App\Observers;

use App\User;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        if ($user->isDirty('start_date') || $user->isDirty('end_date')) {
            $user->assignments->each(function ($assignment) {
                $assignment->slot->should_update_assignments = true;
                $assignment->slot->save();
            });
        }
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $user->assignments->each(function ($assignment) {
            $assignment->slot->should_update_assignments = true;
            $assignment->slot->save();
        });
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        $user->assignments->each(function ($assignment) {
            $assignment->slot->should_update_assignments = true;
            $assignment->slot->save();
        });
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        $user->assignments->each(function ($assignment) {
            $assignment->slot->should_update_assignments = true;
            $assignment->slot->save();
        });
    }
}
