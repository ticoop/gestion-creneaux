<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cache;

class ConfigItem extends Model
{
    use HasTranslations;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'required' => 'boolean',
    ];

    public $translatable = [
        'label',
        'description',
    ];

    public function getValueAttribute($value)
    {
        if ($value === null) {
            return null;
        } else {
            switch ($this->type) {
                case 'integer':
                    return (int)$value;
                default:
                    return $value;
            }
        }
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = $value;
        Cache::forget('ConfigItem::' . $this->key . '::value');
    }

    public static function getCached($key)
    {
        $cacheKey = 'ConfigItem::' . $key . '::value';
        $value = Cache::get($cacheKey);
        if ($value === null) {
            $value = ConfigItem::where('key', $key)->first()->value;
            Cache::put($cacheKey, $value, now()->addDay());
        }
        return $value;
    }
}
