<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\Socialite\TicoopProvider;
use App\Assignment;
use App\ShopSchedule;
use App\ShopScheduleSlot;
use App\AssignmentSwitch;
use App\Leave;
use App\User;
use App\Observers\AssignmentObserver;
use App\Observers\ShopScheduleObserver;
use App\Observers\ShopScheduleSlotObserver;
use App\Observers\AssignmentSwitchObserver;
use App\Observers\LeaveObserver;
use App\Observers\UserObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // if (config('app.force_https')) {
        //     \URL::forceScheme('https');
        // }
        
        $this->bootTicoopSocialite();

        Assignment::observe(AssignmentObserver::class);
        ShopSchedule::observe(ShopScheduleObserver::class);
        ShopScheduleSlot::observe(ShopScheduleSlotObserver::class);
        AssignmentSwitch::observe(AssignmentSwitchObserver::class);
        Leave::observe(LeaveObserver::class);
        User::observe(UserObserver::class);
    }

    private function bootTicoopSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'ticoop',
            function ($app) use ($socialite) {
                $config = $app['config']['services.ticoop_auth'];
                return $socialite->buildProvider(TicoopProvider::class, $config);
            }
        );
    }
}
