<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Gate::define('impersonate', function ($user) {
            return $user->hasRole('impersonate') && !session('impersonated_by');
        });
        Gate::define('see-sent-emails', function ($user) {
            return $user->hasRole('admin');
        });
        Gate::define('admin-users', function ($user) {
            return $user->hasRole('admin-users');
        });
        Gate::define('admin', function ($user) {
            return $user->hasRole('admin');
        });
    }
}
