<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Dolibarr\DolibarrClient;

class DolibarrServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('DolibarrClient', function ($app) {
            return new DolibarrClient();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
