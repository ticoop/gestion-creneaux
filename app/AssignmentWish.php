<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentWish extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flying_team',
        'referent',
        'user_id',
        'shop_schedule_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'flying_team' => 'boolean',
        'referent' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function schedule()
    {
        return $this->belongsTo('App\ShopSchedule', 'shop_schedule_id');
    }

    public function slots()
    {
        return $this->belongsToMany('App\ShopScheduleSlot', 'assignment_wish_shop_schedule_slot');
    }
}
