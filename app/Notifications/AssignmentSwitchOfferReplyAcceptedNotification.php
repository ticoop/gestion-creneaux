<?php

namespace App\Notifications;

use App\AssignmentSwitchOfferReply;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignmentSwitchOfferReplyAcceptedNotification extends Notification
{
    use Queueable;

    protected $reply;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AssignmentSwitchOfferReply $reply)
    {
        $this->reply = $reply;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $offerDateTime = Carbon::parse(
            substr($this->reply->offer->offer_date, 0, 10) . ' ' . $this->reply->offer->assignment->slot->start_at
        );
        if (!$this->reply->reply_flying) {
            $replyDateTime = Carbon::parse(
                substr($this->reply->reply_date, 0, 10) . ' ' . $this->reply->replyAssignment->slot->start_at
            );
        }
        $message = (new MailMessage)
            ->markdown('notifications::email', [
                'mailTitle' => __('mails.assignment-switch-offer-reply-accepted.title'),
            ])
            ->subject(__('mails.assignment-switch-offer-reply-accepted.subject'))
            ->line(__('mails.assignment-switch-offer-reply-accepted.details' . ($this->reply->reply_flying ? '-flying' : ''), [
                'offerUser' => $this->reply->offer->offerUser->fullname,
                'offerDateTime' => $offerDateTime->format(__('dates.carbon.date-time')),
                'offerWeekday' => __('dates.weekday-' . $offerDateTime->format('w')),
                'replyDateTime' => ($this->reply->reply_flying ? '' : $replyDateTime->format(__('dates.carbon.date-time'))),
                'replyWeekday' => ($this->reply->reply_flying ? '' : __('dates.weekday-' . $replyDateTime->format('w'))),
            ]))
            ->line(__('mails.assignment-switch-offer-reply-accepted.explanation-text'))
            ->action(__('mails.assignment-switch-offer-reply-accepted.action-label'), url('/my-schedule'));

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
