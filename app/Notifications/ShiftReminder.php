<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\ActualAssignment;

class ShiftReminder extends Notification
{
    use Queueable;

    protected $assignment;
    protected $weekkly;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ActualAssignment $assignment, $weekkly = false)
    {
        $this->assignment = $assignment;
        $this->weekkly = $weekkly;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->markdown('notifications::email', [
                'mailTitle' => __('mails.shift-reminder.title' . ($this->weekkly ? '-weekly' : '')),
            ])
            ->subject(__('mails.shift-reminder.subject' . ($this->weekkly ? '-weekly' : '')))
            ->line(__('mails.shift-reminder.reminder-text' . ($this->weekkly ? '-weekly' : ''), [
                'shiftTime' => $this->assignment->start_at->format(
                    __('dates.carbon.' . ($this->weekkly ? 'date-time' : 'time-format'))
                ),
            ]))
            ->line(__('mails.shift-reminder.useful-infos'))
            ->line(__('mails.shift-reminder.news-infos'))
            ->line(__('mails.shift-reminder.unable-text'))
            ->line(__('mails.shift-reminder.help-text'))
            ->action(__('mails.shift-reminder.action-label'), url('/my-schedule'));
        
        if ($this->assignment->referent && $this->assignment->slot->generate_key_code && !$this->weekkly) {
            $keyCodes = $this->assignment->getKeyCodes();
            $message = $message->line('---')
                ->line(__('mails.shift-reminder.key-codes', $keyCodes))
                ->line(__('mails.shift-reminder.opening-key-code', $keyCodes))
                ->line(__('mails.shift-reminder.closing-key-code', $keyCodes))
                ->line('---');
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
