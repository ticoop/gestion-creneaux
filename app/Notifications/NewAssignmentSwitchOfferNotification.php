<?php

namespace App\Notifications;

use App\AssignmentSwitchOffer;
use App\Htmlables\Pre;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewAssignmentSwitchOfferNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $offer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AssignmentSwitchOffer $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $dateTime = Carbon::parse(
            substr($this->offer->offer_date, 0, 10) . ' ' . $this->offer->assignment->slot->start_at
        );
        $message = (new MailMessage)
            ->markdown('notifications::email', [
                'mailTitle' => __('mails.new-assignment-switch-offer.title'),
            ])
            ->subject(__('mails.new-assignment-switch-offer.subject'))
            ->line(__('mails.new-assignment-switch-offer.offer-text', [
                'dateTime' => $dateTime->format(__('dates.carbon.date-time')),
                'weekday' => __('dates.weekday-' . $dateTime->format('w')),
            ]));
        if ($this->offer->comment) {
            $message = $message->line(__('mails.new-assignment-switch-offer.comment'))
                ->line(new Pre($this->offer->comment));
        }
        $message = $message->line(__('mails.new-assignment-switch-offer.explanation-text'))
            ->action(__('mails.new-assignment-switch-offer.action-label'), url('/assignment-switch-offers'));

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
