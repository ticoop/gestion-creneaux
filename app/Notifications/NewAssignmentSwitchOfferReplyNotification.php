<?php

namespace App\Notifications;

use App\AssignmentSwitchOfferReply;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewAssignmentSwitchOfferReplyNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $reply;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AssignmentSwitchOfferReply $reply)
    {
        $this->reply = $reply;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $offerDateTime = Carbon::parse(
            substr($this->reply->offer->offer_date, 0, 10) . ' ' . $this->reply->offer->assignment->slot->start_at
        );
        $message = (new MailMessage)
            ->markdown('notifications::email', [
                'mailTitle' => __('mails.new-assignment-switch-offer-reply.title'),
            ])
            ->subject(__('mails.new-assignment-switch-offer-reply.subject'))
            ->line(__('mails.new-assignment-switch-offer-reply.offer-text', [
                'offerDateTime' => $offerDateTime->format(__('dates.carbon.date-time')),
                'offerWeekday' => __('dates.weekday-' . $offerDateTime->format('w')),
            ]))
            ->line(__('mails.new-assignment-switch-offer-reply.explanation-text'))
            ->action(__('mails.new-assignment-switch-offer-reply.action-label'), url('/assignment-switch-offers/' . $this->reply->offer->id . '/check-replies'));

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
