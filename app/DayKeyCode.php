<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayKeyCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'opening_code',
        'closing_code',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];

    public static function generateCode()
    {
        $digits = config('key-codes.digits');
        $res = '';
        for ($i = 0; $i < $digits; $i++) {
            $res .= rand(0, 9);
        }
        return $res;
    }
}
