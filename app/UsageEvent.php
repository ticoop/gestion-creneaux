<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UsageLog;

/**
 * The UsageEvent model represents events that can be logged (eg login, logout, ...)
 */
class UsageEvent extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
    ];

    public function logs()
    {
        return $this->hasMany(UsageLog::class);
    }
}
