<?php

namespace App\Relations;

use App\ActualAssignment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Collection;

class ParallelActualAssignments extends Relation
{
    public function __construct(Model $parent)
    {
        $query = ActualAssignment::query();
        parent::__construct($query, $parent);
    }

    public function addConstraints()
    {
        if (static::$constraints) {
            $this->query->where('shop_schedule_slot_id', $this->parent->shop_schedule_slot_id)
                ->where('date', $this->parent->date);
        }
    }

    public function addEagerConstraints(array $models)
    {
        $this->query->whereIn('shop_schedule_slot_id', collect($models)->pluck('shop_schedule_slot_id'))
            ->whereIn('date', collect($models)->pluck('date'));
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, $this->related->newCollection());
        }

        return $models;
    }

    public function match($models, Collection $results, $relation)
    {
        $map = [];
        foreach ($results as $result) {
            if (!isset($map[$result->shop_schedule_slot_id])) {
                $map[$result->shop_schedule_slot_id] = [];
            }
            if (!isset($map[$result->shop_schedule_slot_id][$result->date->toDatetimeString()])) {
                $map[$result->shop_schedule_slot_id][$result->date->toDatetimeString()] = collect();
            }
            $map[$result->shop_schedule_slot_id][$result->date->toDatetimeString()]->push($result);
        }
    
        foreach ($models as $model) {
            if (isset($map[$model->shop_schedule_slot_id]) &&
                isset($map[$model->shop_schedule_slot_id][$model->date->toDatetimeString()])
            ) {
                $model->setRelation($relation, $map[$model->shop_schedule_slot_id][$model->date->toDatetimeString()]);
            }
        }

        return $models;
    }

    public function getResults()
    {
        return $this->query->get();
    }
}
