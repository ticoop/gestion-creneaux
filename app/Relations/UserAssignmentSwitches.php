<?php

namespace App\Relations;

use App\AssignmentSwitch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Collection;

class UserAssignmentSwitches extends Relation
{
    public function __construct(Model $parent)
    {
        $query = AssignmentSwitch::query();
        parent::__construct($query, $parent);
    }

    public function addConstraints()
    {
        if (static::$constraints) {
            $this->query->where(function ($q) {
                $q->whereHas('assignment', function ($q2) {
                    $q2->where('user_id', $this->parent->id);
                })->orWhere('replacement_user_id', $this->parent->id);
            });
        }
    }

    public function addEagerConstraints(array $models)
    {
        $this->query->where(function ($q) use ($models) {
            $q->whereHas('assignment', function ($q2) use ($models) {
                $q2->whereIn('user_id', collect($models)->pluck('id'));
            })->orWhereIn('replacement_user_id', collect($models)->pluck('id'));
        });
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, $this->related->newCollection());
        }

        return $models;
    }

    public function match($models, Collection $results, $relation)
    {
        $map = [];
        foreach ($results as $result) {
            if (!isset($map[$result->replacement_user_id])) {
                $map[$result->replacement_user_id] = collect();
            }
            if (!isset($map[$result->assignment->user_id])) {
                $map[$result->assignment->user_id] = collect();
            }
            $map[$result->replacement_user_id]->push($result);
            $map[$result->assignment->user_id]->push($result);
        }
    
        foreach ($models as $model) {
            if (isset($map[$model->id])) {
                $model->setRelation($relation, $map[$model->id]);
            }
        }

        return $models;
    }

    public function getResults()
    {
        return $this->query->get();
    }
}
