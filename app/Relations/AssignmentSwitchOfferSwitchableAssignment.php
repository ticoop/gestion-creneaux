<?php

namespace App\Relations;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\ActualAssignment;

class AssignmentSwitchOfferSwitchableAssignment extends Relation
{
    public function __construct(Model $parent)
    {
        $query = ActualAssignment::where('user_id', auth()->user()->id)->where('start_at', '>', now());
        parent::__construct($query, $parent);
    }

    public function addConstraints()
    {
        if (static::$constraints) {
            $this->query->where(function ($q) {
                foreach ($this->parent->possibleReplacements as $replacement) {
                    $q->orWhere(function ($q2) use ($replacement) {
                        $q2->where('shop_schedule_slot_id', $replacement->id)->whereIn('date', explode(',', $replacement->pivot->dates));
                    });
                }
            });
        }
    }

    public function addEagerConstraints(array $models)
    {
        $this->query->where(function ($q) use ($models) {
            foreach ($models as $model) {
                foreach ($model->possibleReplacements as $replacement) {
                    $q->orWhere(function ($q2) use ($replacement) {
                        $q2->where('shop_schedule_slot_id', $replacement->id)->whereIn('date', explode(',', $replacement->pivot->dates));
                    });
                }
            }
        });
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, []);
        }

        return $models;
    }

    public function match($models, Collection $results, $relation)
    {
        $map = [];
        foreach ($results as $result) {
            if (!isset($map[$result->shop_schedule_slot_id])) {
                $map[$result->shop_schedule_slot_id] = [];
            }
            $date = $result->date_string;
            if (!isset($map[$result->shop_schedule_slot_id][$date])) {
                $map[$result->shop_schedule_slot_id][$date] = [];
            }
            $map[$result->shop_schedule_slot_id][$date][] = $result;
        }
    
        foreach ($models as $model) {
            $relationValue = collect();
            foreach ($model->possibleReplacements as $slot) {
                if (isset($map[$slot->id])) {
                    foreach (explode(",", $slot->pivot->dates) as $date) {
                        if (isset($map[$slot->id][$date])) {
                            $relationValue = $relationValue->merge($map[$slot->id][$date]);
                        }
                    }
                }
            }
            $model->setRelation($relation, $relationValue);
        }

        return $models;
    }

    public function getResults()
    {
        return $this->query->get();
    }
}
