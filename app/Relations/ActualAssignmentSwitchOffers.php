<?php

namespace App\Relations;

use App\AssignmentSwitchOffer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Collection;

class ActualAssignmentSwitchOffers extends Relation
{
    public function __construct(Model $parent)
    {
        $query = AssignmentSwitchOffer::query();
        parent::__construct($query, $parent);
    }

    public function addConstraints()
    {
        if (static::$constraints) {
            $this->query->where('offer_assignment_id', $this->parent->assignment_id)
                ->where('offer_date', $this->parent->date);
        }
    }

    public function addEagerConstraints(array $models)
    {
        $this->query->whereIn('offer_assignment_id', collect($models)->pluck('assignment_id'))
            ->whereIn('offer_date', collect($models)->pluck('date'));
    }

    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, $this->related->newCollection());
        }

        return $models;
    }

    public function match($models, Collection $results, $relation)
    {
        $map = [];
        foreach ($results as $result) {
            if (!isset($map[$result->offer_assignment_id])) {
                $map[$result->offer_assignment_id] = [];
            }
            if (!isset($map[$result->offer_assignment_id][$result->offer_date->toDatetimeString()])) {
                $map[$result->offer_assignment_id][$result->offer_date->toDatetimeString()] = collect();
            }
            $map[$result->offer_assignment_id][$result->offer_date->toDatetimeString()]->push($result);
        }
    
        foreach ($models as $model) {
            if (isset($map[$model->assignment_id]) &&
                isset($map[$model->assignment_id][$model->date->toDatetimeString()])
            ) {
                $model->setRelation($relation, $map[$model->assignment_id][$model->date->toDatetimeString()]);
            }
        }

        return $models;
    }

    public function getResults()
    {
        return $this->query->get();
    }
}
