<?php

namespace App\Relations;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\User;

class AssignmentReferentUsersRelation extends Relation
{
    public function __construct(Model $parent)
    {
        $query = User::with(['assignments' => function ($q) {
            $q->where('referent', 1);
        }]);
        parent::__construct($query, $parent);
    }

    public function addConstraints()
    {
        if (static::$constraints) {
            $this->query->whereHas('assignments', function ($q) {
                $q->where('shop_schedule_slot_id', $this->parent->shop_schedule_slot_id)
                    ->where('week_index', $this->parent->week_index)
                    ->where('referent', 1);
            });
        }
    }

    public function addEagerConstraints(array $models)
    {
        $this->query->whereHas('assignments', function ($q) use ($models) {
            $q->whereIn('shop_schedule_slot_id', collect($models)->pluck('shop_schedule_slot_id'))
                ->whereIn('week_index', collect($models)->pluck('week_index'))
                ->where('referent', 1);
        });
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, $this->related->newCollection());
        }

        return $models;
    }

    public function match($models, Collection $results, $relation)
    {
        $map = [];
        foreach ($results as $result) {
            foreach ($result->assignments as $assignment) {
                if (!isset($map[$assignment->shop_schedule_slot_id])) {
                    $map[$assignment->shop_schedule_slot_id] = [];
                }
                if (!isset($map[$assignment->shop_schedule_slot_id][$assignment->week_index])) {
                    $map[$assignment->shop_schedule_slot_id][$assignment->week_index] = new Collection();
                }
                $map[$assignment->shop_schedule_slot_id][$assignment->week_index]->push($result);
            }
        }
    
        foreach ($models as $model) {
            if (isset($map[$model->shop_schedule_slot_id]) &&
                isset($map[$model->shop_schedule_slot_id][$model->week_index])
            ) {
                $model->setRelation($relation, $map[$model->shop_schedule_slot_id][$model->week_index]);
            }
        }

        return $models;
    }

    public function getResults()
    {
        return $this->query->get();
    }
}
