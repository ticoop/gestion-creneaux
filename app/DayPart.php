<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class DayPart extends Model
{
    use HasTranslations;
    
    public $translatable = ['label'];
    public $timestamps = false;
}
