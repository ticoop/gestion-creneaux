<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $users;

    public function __construct($users)
    {
        $this->users = $users;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->users->map(function ($user) {
            $assignments = $user->currentAssignments->map(function ($a) {
                return chr(65 + $a->week_index) . ' - ' . __('global.weekday-' . $a->slot->weekday) . ' ' .
                    substr($a->slot->start_at, 0, 5);
            });
            if ($user->flying_team) {
                $assignments->push(__('users.flying-team'));
            }
            return [
                'username' => $user->username,
                'email' => $user->email,
                'fullname' => $user->fullname,
                'status' => __('users.status-'  . User::STATUSES[$user->status]),
                'assignments' => $assignments->implode(PHP_EOL),
                'referent' => $user->referent ? __('global.yes') : __('global.no'),
                'roles' => $user->roles->pluck('label')->implode(', '),
                'note' => $user->note,
            ];
        });
    }

    public function headings(): array
    {
        return [
            __('users.username'),
            __('users.email'),
            __('users.full-name'),
            __('users.member-status'),
            __('users.assigned-slot'),
            __('users.referent'),
            __('users.roles'),
            __('users.note'),
        ];
    }
}
