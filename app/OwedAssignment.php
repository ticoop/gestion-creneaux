<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwedAssignment extends Model
{
    protected $fillable = [
        'initial_assignment_date',
        'user_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'max_assignment_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'initial_assignment_date' => 'datetime:Y-m-d',
    ];

    public function actualAssignment()
    {
        return $this->hasOne(ActualAssignment::class);
    }

    public function getMaxAssignmentDateAttribute()
    {
        return $this->initial_assignment_date->addDays(ConfigItem::getCached('flying_switch_delay'));
    }
}
