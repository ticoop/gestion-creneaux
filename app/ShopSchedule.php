<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopSchedule extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'start_at',
        'end_at',
        'working_shift_frequency',
        'min_workers_per_slot',
        'max_workers_per_slot',
        'ideal_workers_per_slot',
        'flying_members_per_slot',
        'max_referents_per_slot',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_at',
        'end_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_at' => 'datetime:Y-m-d',
        'end_at' => 'datetime:Y-m-d',
    ];

    protected static $computeSlotsByDayFullResult = [];

    public function slots()
    {
        return $this->hasMany(ShopScheduleSlot::class)->orderBy('weekday')->orderBy('start_at');
    }
    
    public function overrides()
    {
        return $this->hasMany(ShopSchedule::class, 'override_shop_schedule_id');
    }

    public function parent()
    {
        return $this->belongsTo(ShopSchedule::class, 'override_shop_schedule_id');
    }

    public function assignmentWishes()
    {
        return $this->hasMany(AssignmentWish::class);
    }

    public static function checkConflict(&$data, ShopSchedule $ignore = null, ShopSchedule $parent = null)
    {
        if (!!$parent) {
            $query = $parent->overrides();
        } else {
            $query = ShopSchedule::whereNull('override_shop_schedule_id');
        }
        
        $query->where('start_at', '<=', $data['end_at'])
            ->where('end_at', '>=', $data['start_at']);
        
        if (!!$ignore) {
            $query->where('id', '!=', $ignore->id);
        }
        
        $conflictingCount = $query->count();
        
        if ($conflictingCount > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function computeSlotsByDay($start, $end)
    {
        $s = max($start, $this->start_at);
        $e = min($end, $this->end_at);

        $slotsByWeekDay = [];
        foreach ($this->slots as $slot) {
            if (!array_key_exists($slot->weekday, $slotsByWeekDay)) {
                $slotsByWeekDay[$slot->weekday] = collect();
            }
            $slotsByWeekDay[$slot->weekday]->push($slot);
        }

        $res = [];

        $day = $s->copy();
        while ($day->lte($e)) {
            $slots = isset($slotsByWeekDay[$day->dayOfWeek]) ? $slotsByWeekDay[$day->dayOfWeek] : collect();
            $slots = $slots->filter(function ($slot) use ($day) {
                return ($slot->start_date === null || $slot->start_date->lte($day)) &&
                    ($slot->end_date === null || $slot->end_date->gte($day)) &&
                    (
                        $slot->special_slot_type === null ||
                        $slot->frequency === null ||
                        (
                            $day->diffInWeeks($slot->start_date) % $slot->frequency == 0 &&
                                ($slot->end_date === null || $day->lte($slot->end_date))
                        )
                    );
            });
            $res[$day->toDateString()] = $slots;
            $day->addDay();
        }

        foreach ($this->overrides as $override) {
            foreach ($override->computeSlotsByDay($start, $end) as $dayString => $slots) {
                $res[$dayString] = $slots;
            }
        }

        return $res;
    }

    public function computeSlotsByDayFull()
    {
        if (!array_key_exists($this->id, static::$computeSlotsByDayFullResult)) {
            static::$computeSlotsByDayFullResult[$this->id] = $this->computeSlotsByDay($this->start_at, $this->end_at);
        }
        return static::$computeSlotsByDayFullResult[$this->id];
    }

    public function getStartAtAttribute($value)
    {
        $res = Carbon::parse($value);
        $res->setTimezone(config('app.timezone'));
        return $res;
    }

    public function setStartAtAttribute($value)
    {
        if ($value !== $this->start_at) {
            $this->attributes['start_at'] = $value;
            $this->slots->each(function ($slot) {
                $slot->should_update_assignments = true;
                $slot->save();
            });
        }
    }

    public function getEndAtAttribute($value)
    {
        $res = Carbon::parse($value);
        $res->setTimezone(config('app.timezone'));
        return $res;
    }

    public function setEndAtAttribute($value)
    {
        if ($value !== $this->end_at) {
            $this->attributes['end_at'] = $value;
            $this->slots->each(function ($slot) {
                $slot->should_update_assignments = true;
                $slot->save();
            });
        }
    }

    public function getMinWorkersPerSlotAttribute($value)
    {
        return $value === null ? ConfigItem::getCached('min_workers_per_slot') : $value;
    }

    public function getMaxWorkersPerSlotAttribute($value)
    {
        return $value === null ? ConfigItem::getCached('max_workers_per_slot') : $value;
    }

    public function getIdealWorkersPerSlotAttribute($value)
    {
        return $value === null ? ConfigItem::getCached('ideal_workers_per_slot') : $value;
    }

    public function getFlyingMembersPerSlotAttribute($value)
    {
        return $value === null ? ConfigItem::getCached('flying_members_per_slot') : $value;
    }

    public function getMaxReferentsPerSlotAttribute($value)
    {
        return $value === null ? ConfigItem::getCached('max_referents_per_slot') : $value;
    }
}
