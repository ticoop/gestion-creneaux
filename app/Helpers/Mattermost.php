<?php

namespace App\Helpers;

use GuzzleHttp\Client as GuzzleClient;

abstract class Mattermost
{
    protected static $client = null;

    protected static function getClient()
    {
        if (self::$client === null) {
            self::$client = new GuzzleClient();
        }
        return self::$client;
    }

    protected static function request($method, $path, $params = [])
    {
        $client = self::getClient();
        $parsedUrl = parse_url(config('app.mattermost_message_url'));
        $url = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . '/api/v4/' . $path;
        if (!isset($params['headers'])) {
            $params['headers'] = [];
        }
        if (!isset($params['headers']['Authorization'])) {
            $params['headers']['Authorization'] = 'Bearer ' . config('app.mattermost_api_key');
        }
        return json_decode((string) $client->request($method, $url, $params)->getBody(), true);
    }

    public static function getUserByUsername(string $username)
    {
        return self::request('GET', 'users/username/' . $username);
    }

    public static function createUserToken($userId)
    {
        return self::request('POST', 'users/' . $userId . '/tokens', [
            'json' => [
                'description' => 'shift_app_token',
            ],
        ])['token'];
    }

    public static function createGroupConversation($token, $userIds)
    {
        return self::request('POST', 'channels/group', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => $userIds,
        ]);
    }
}
