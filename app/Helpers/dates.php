<?php

use App\ConfigItem;
use Carbon\Carbon;

function getWeekIndex(Carbon $date)
{
    $nbWeeks = ConfigItem::getCached('working_shift_frequency');
    return $date->diffInWeeks(Carbon::parse("1970-01-01")->startOfWeek(Carbon::MONDAY)) % $nbWeeks;
}
