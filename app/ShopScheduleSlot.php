<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopScheduleSlot extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weekday',
        'start_at',
        'end_at',
        'comment',
        'shop_open',
        'min_workers_per_slot',
        'max_workers_per_slot',
        'ideal_workers_per_slot',
        'flying_members_per_slot',
        'max_referents_per_slot',
        'volunteer',
        'accounting',
        'generate_key_code',
        'special_slot_type',
        'start_date',
        'end_date',
        'frequency',
        'should_update_assignments',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'shop_open' => 'boolean',
        'accounting' => 'boolean',
        'generate_key_code' => 'boolean',
    ];

    public function schedule()
    {
        return $this->belongsTo(ShopSchedule::class, 'shop_schedule_id');
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class);
    }

    public function actualAssignments()
    {
        return $this->hasMany(ActualAssignment::class);
    }

    public function referents()
    {
        return $this->assignments()->wherePivot('referent', 1);
    }

    public function assignmentWishes()
    {
        return $this->belongsToMany('App\AssignmentWish', 'assignment_wish_shop_schedule_slot');
    }

    public function setStartAtAttribute($value)
    {
        if ($value !== $this->start_at) {
            $this->attributes['start_at'] = $value;
            $this->should_update_assignments = true;
        }
    }

    public function setEndAtAttribute($value)
    {
        if ($value !== $this->end_at) {
            $this->attributes['end_at'] = $value;
            $this->should_update_assignments = true;
        }
    }

    public function setStartDateAttribute($value)
    {
        if ($value !== $this->start_date) {
            $this->attributes['start_date'] = $value;
            $this->should_update_assignments = true;
        }
    }

    public function setEndDateAttribute($value)
    {
        if ($value !== $this->end_date) {
            $this->attributes['end_date'] = $value;
            $this->should_update_assignments = true;
        }
    }

    public function setFrequencyAttribute($value)
    {
        if ($value !== $this->frequency) {
            $this->attributes['frequency'] = $value;
            $this->should_update_assignments = true;
        }
    }
    
    public function setSpecialSlotTypeAttribute($value)
    {
        if ($value !== $this->special_slot_type) {
            $this->attributes['special_slot_type'] = $value;
            $this->should_update_assignments = true;
        }
    }

    public function getMinWorkersPerSlotAttribute($value)
    {
        return $value === null ? $this->schedule->min_workers_per_slot : $value;
    }

    public function getMaxWorkersPerSlotAttribute($value)
    {
        return $value === null ? $this->schedule->max_workers_per_slot : $value;
    }

    public function getIdealWorkersPerSlotAttribute($value)
    {
        return $value === null ? $this->schedule->ideal_workers_per_slot : $value;
    }

    public function getFlyingMembersPerSlotAttribute($value)
    {
        return $value === null ? $this->schedule->flying_members_per_slot : $value;
    }

    public function getMaxReferentsPerSlotAttribute($value)
    {
        return $value === null ? $this->schedule->max_referents_per_slot : $value;
    }

    public function getDatesAttribute()
    {
        $schedule = $this->schedule;
        while (!!$schedule->parent) {
            $schedule = $schedule->parent;
        }
        $compute = $schedule->computeSlotsByDayFull();
        return collect(array_keys($compute))->filter(function ($date) use ($compute) {
            return $compute[$date]->pluck('id')->contains($this->id);
        });
    }
}
