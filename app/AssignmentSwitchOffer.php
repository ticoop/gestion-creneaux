<?php

namespace App;

use App\Notifications\NewAssignmentSwitchOfferNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Relations\AssignmentSwitchOfferSwitchableAssignment;

class AssignmentSwitchOffer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'offer_date',
        'comment',
        'offer_user_id',
        'target_user_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'offer_date',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'targetUser',
    ];

    public function assignment()
    {
        return $this->belongsTo(Assignment::class, 'offer_assignment_id');
    }

    public function possibleReplacements()
    {
        return $this->belongsToMany(ShopScheduleSlot::class, 'assignment_switch_offer_possible_replacements')
            ->withPivot(['dates']);
    }

    public function switchableAssignments()
    {
        return new AssignmentSwitchOfferSwitchableAssignment($this);
    }

    public function slot()
    {
        return $this->hasOneThrough(
            ShopScheduleSlot::class,
            Assignment::class,
            'id',
            'id',
            'offer_assignment_id',
            'shop_schedule_slot_id'
        );
    }

    public function offerUser()
    {
        return $this->belongsTo(User::class, 'offer_user_id');
    }

    public function targetUser()
    {
        return $this->belongsTo(User::class, 'target_user_id');
    }

    public function replies()
    {
        return $this->hasMany(AssignmentSwitchOfferReply::class, 'assignment_switch_offer_id');
    }

    public function acceptedReplies()
    {
        return $this->replies()->where('accepted', 1);
    }

    public function pendingReplies()
    {
        return $this->replies()->whereNull('accepted');
    }

    public function currentUserReplies()
    {
        return $this->replies()->where('reply_user_id', auth()->user()->id);
    }

    public function notifyRecipients()
    {
        foreach ($this->possibleReplacements()->with('assignments.user')->get() as $slot) {
            foreach ($slot->assignments as $assignment) {
                if ($assignment->user->id != $this->offerUser->id) {
                    $assignment->user->notify(new NewAssignmentSwitchOfferNotification($this));
                }
            }
        }
    }

    public function getActualAssignmentAttribute()
    {
        return $this->assignment->actualAssignments()
            ->where('date', $this->offer_date)
            ->where('user_id', $this->offer_user_id)
            ->first();
    }
}
