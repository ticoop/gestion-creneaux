<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstname,
        'lastname' => $faker->lastname,
        'username' => $faker->unique()->username,
        'oauth_id' => $faker->unique()->md5,
        'email' => $faker->unique()->email,
        'start_date' => $faker->dateTimeBetween('-1 year'),
    ];
});

$factory->state(User::class, 'referent', [
    'referent' => true,
]);

$factory->state(User::class, 'flying_team', [
    'flying_team' => true,
]);
