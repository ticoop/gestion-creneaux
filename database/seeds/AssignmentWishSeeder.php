<?php

use Illuminate\Database\Seeder;
use App\ShopSchedule;
use App\User;
use App\AssignmentWish;

class AssignmentWishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssignmentWish::query()->forceDelete();

        $schedules = ShopSchedule::with('slots.assignments')->whereNull('override_shop_schedule_id')->get();
        foreach ($schedules as $schedule) {
            $users = User::where('flying_team', false)
                ->whereDoesntHave('assignments.slot', function ($q) use ($schedule) {
                    $q->where('shop_schedule_id', $schedule->id);
                })
                ->get();
            foreach ($users as $user) {
                $nbWishes = rand(0, 5);
                if ($nbWishes > 0) {
                    $wishes = collect(array_rand($schedule->slots->toArray(), $nbWishes))
                        ->map(function ($i) use ($schedule) {
                            return $schedule->slots[$i]->id;
                        });
                    $userWish = $user->assignmentWish()->create([
                        'shop_schedule_id' => $schedule->id,
                        'referent' => !rand(0, 4),
                    ]);
                    $userWish->slots()->sync($wishes);
                } else {
                    $userWish = $user->assignmentWish()->create([
                        'shop_schedule_id' => $schedule->id,
                        'flying_team' => true,
                    ]);
                }
            }
        }
    }
}
