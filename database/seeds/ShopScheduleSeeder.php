<?php

use Illuminate\Database\Seeder;
use App\ShopSchedule;

class ShopScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShopSchedule::query()->forceDelete();

        foreach ([date('Y') - 1, date('Y'), date('Y') + 1] as $year) {
            $schedule = ShopSchedule::create([
                'title' => $year,
                'start_at' => $year . '-05-25',
                'end_at' => $year . '-12-31',
                'working_shift_frequency' => 4,
            ]);
            foreach ([1, 3, 5, 6] as $day) {
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '15:45',
                    'end_at' => '18:45',
                ]);
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '18:30',
                    'end_at' => '21:30',
                ]);
            }
            foreach ([3, 6] as $day) {
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '10:15',
                    'end_at' => '13:15',
                ]);
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '13:00',
                    'end_at' => '16:00',
                ]);
            }
            foreach ([3] as $day) {
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '08:00',
                    'end_at' => '10:00',
                    'shop_open' => false,
                    'comment' => 'Livraisons',
                ]);
            }
            foreach ([1, 2, 4, 5] as $day) {
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '08:00',
                    'end_at' => '10:00',
                    'shop_open' => false,
                    'comment' => 'Livraisons',
                ]);
                $schedule->slots()->create([
                    'weekday' => $day,
                    'start_at' => '10:00',
                    'end_at' => '12:00',
                    'shop_open' => false,
                    'comment' => 'Livraisons',
                ]);
            }
            $schedule->slots()->create([
                'weekday' => 2,
                'start_at' => '18:00',
                'end_at' => '21:00',
                'shop_open' => false,
                'special_slot_type' => 'inventory',
                'start_date' => $year . '-06-02',
                'end_date' => $year . '-12-31',
                'frequency' => 4,
                'comment' => 'Inventaire',
                'min_workers_per_slot' => 6,
                'max_workers_per_slot' => 12,
                'ideal_workers_per_slot' => 10,
            ]);
        }
    }
}
