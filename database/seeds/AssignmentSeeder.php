<?php

use Illuminate\Database\Seeder;
use App\User;
use App\ShopSchedule;
use App\Assignment;
use App\ActualAssignment;
use App\ConfigItem;

class AssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Assignment::query()->forceDelete();

        $users = User::where('flying_team', false)->get();

        $nbPerSlot = ConfigItem::where('key', 'max_workers_per_slot')->first()->value;
        $nbWeeks = ConfigItem::where('key', 'working_shift_frequency')->first()->value;

        $nbToFillValues = collect();
        for ($i = 0; $i <= $nbPerSlot; $i++) {
            for ($j = 0; $j <= $i; $j++) {
                $nbToFillValues->push($i);
            }
        }

        $schedules = ShopSchedule::with('slots')->get();
        foreach ($schedules as $schedule) {
            $scheduleUsers = (clone $users)->shuffle();
            $scheduleReferents = $scheduleUsers->filter(function ($u) {
                return $u->referent;
            });
            $scheduleOthers = $scheduleUsers->filter(function ($u) {
                return !$u->referent;
            });
            foreach ($schedule->slots as $slot) {
                for ($w = 0; $w < $nbWeeks; $w++) {
                    $nbToFill = $nbToFillValues->random();
                    if ($nbToFill > 0 && $scheduleReferents->count() > 0) {
                        $referent = $scheduleReferents->shift();
                        Assignment::create([
                            'shop_schedule_slot_id' => $slot->id,
                            'user_id' => $referent->id,
                            'week_index' => $w,
                            'referent' => true,
                        ]);
                    }
                    if ($nbToFill > 1) {
                        for ($i = 0; $i < $nbToFill - 1; $i++) {
                            if ($scheduleOthers->count() > 0) {
                                $user = $scheduleOthers->shift();
                                Assignment::create([
                                    'shop_schedule_slot_id' => $slot->id,
                                    'user_id' => $user->id,
                                    'week_index' => $w,
                                    'referent' => false,
                                ]);
                            }
                        }
                    }
                }
            }
        }

        Artisan::call('generate-actual-schedule --include-past');

        $flyingTeamSlots = ActualAssignment::where('date', '<', now()->startOfDay())
            ->get()
            ->unique(function ($a) {
                return $a->shop_schedule_slot_id . ':' . $a->date;
            });

        $users = User::where('flying_team', true)->whereNotNull('start_date')->get();
        foreach ($users as $user) {
            $userNbWeeks = now()->diffInWeeks($user->start_date);
            if ($userNbWeeks > 0) {
                $userAssignments = collect(array_rand($flyingTeamSlots->toArray(), ceil($userNbWeeks / $nbWeeks)))
                    ->map(function ($i) use ($flyingTeamSlots) {
                        return $flyingTeamSlots[$i];
                    });
                foreach ($userAssignments as $assignment) {
                    ActualAssignment::create([
                        'shop_schedule_slot_id' => $assignment->shop_schedule_slot_id,
                        'user_id' => $user->id,
                        'date' => $assignment->date,
                        'start_at' => $assignment->start_at,
                        'end_at' => $assignment->end_at,
                        'referent' => false,
                    ]);
                }
            }
        }

        $assignments = ActualAssignment::where('date', '<', now()->startOfDay())->get();
        foreach ($assignments as $assignment) {
            $done = (rand(0, 19) !== 0);
            $assignment->done = $done;
            $assignment->compensations_needed = ($done ? 0 : 2);
            $assignment->save();
        }
    }
}
