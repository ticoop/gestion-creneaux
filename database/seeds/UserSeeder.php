<?php

use Illuminate\Database\Seeder;
use App\User;
use App\ShopSchedule;
use App\ConfigItem;
use App\DayPart;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->forceDelete();

        $slotCount = ShopSchedule::withCount('slots')->get()->pluck('slots_count')->max();
        $nbPerSlot = ConfigItem::where('key', 'max_workers_per_slot')->first()->value;
        $nbWeeks = ConfigItem::where('key', 'working_shift_frequency')->first()->value;
        $nbUsersToCreate = $nbWeeks * $slotCount * $nbPerSlot;

        $localDataFile = database_path('seeds/local-data/users.json');
        if (file_exists($localDataFile)) {
            $data = json_decode(file_get_contents($localDataFile), true);
            $nbUsersToCreate -= count($data);
            $roleMap = Role::all()->pluck('id', 'key');
            foreach ($data as $userData) {
                if (isset($userData['roles'])) {
                    $roles = $userData['roles'];
                    unset($userData['roles']);
                } else {
                    $roles = [];
                }
                $user = User::create($userData);
                $roles = collect($roles)->map(function ($role) use ($roleMap) {
                    return $roleMap[$role];
                });
                $user->roles()->sync($roles);
            }
        }
        
        $factoryUsers = factory(User::class, $nbWeeks * $slotCount)->states('referent')->create();
        $factoryUsers = $factoryUsers->merge(factory(User::class, 50)->states('flying_team')->create());
        if ($nbUsersToCreate > $nbWeeks * $slotCount) {
            $factoryUsers = $factoryUsers->merge(factory(User::class, $nbUsersToCreate - $nbWeeks * $slotCount)->create());
        }
        
        $possibleRescueDays = [];
        foreach (DayPart::all() as $dayPart) {
            foreach ([0, 1, 2, 3, 4, 5, 6] as $weekday) {
                $possibleRescueDays[] = $weekday . '_' . $dayPart->id;
            }
        }
        $factoryUsers->each(function ($user) use ($possibleRescueDays) {
            if (rand(0, 1)) {
                $nbRescueDays = rand(1, 14);
                $rescueDays = array_slice($possibleRescueDays, 0, $nbRescueDays);
                shuffle($possibleRescueDays);

                $rescueTeamDays = [];
                foreach ($rescueDays as $item) {
                    list($weekday, $dayPart) = explode('_', $item);
                    if (!isset($rescueTeamDays[$dayPart])) {
                        $rescueTeamDays[$dayPart] = ['weekdays' => []];
                    }
                    $rescueTeamDays[$dayPart]['weekdays'][] = $weekday;
                }
                foreach ($rescueTeamDays as $key => $value) {
                    $rescueTeamDays[$key]['weekdays'] = implode(",", $rescueTeamDays[$key]['weekdays']);
                }
                $user->rescueTeamDays()->sync($rescueTeamDays);
            }
        });
    }
}
