<?php

use Illuminate\Database\Seeder;

class LeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Leave::query()->forceDelete();

        $user = \App\User::first();
        $leaveType = \App\LeaveType::first()->id;

        if (!!$user) {
            $user->leaves()->create([
                'leave_type_id' => $leaveType,
                'start_at' => now()->subMonths(2)->startOfDay(),
                'end_at' => now()->subMonths(2)->addWeeks(2)->startOfDay(),
            ]);
            $user->leaves()->create([
                'leave_type_id' => $leaveType,
                'start_at' => now()->addWeek()->startOfDay(),
                'end_at' => now()->addWeeks(2)->startOfDay(),
            ]);
        }
    }
}
