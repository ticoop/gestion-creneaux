<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ShopScheduleSeeder::class);
        $this->call(TrainingSeeder::class);
        $this->call(LeaveSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AssignmentSeeder::class);
    }
}
