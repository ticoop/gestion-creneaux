<?php

use Illuminate\Database\Seeder;
use App\Training;

class TrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Training::query()->forceDelete();

        DB::table('trainings')->insert([
            [
                'label' => json_encode([
                    'fr' => 'Vrac',
                ]),
            ],
            [
                'label' => json_encode([
                    'fr' => 'Caisse',
                ]),
            ],
        ]);
    }
}
