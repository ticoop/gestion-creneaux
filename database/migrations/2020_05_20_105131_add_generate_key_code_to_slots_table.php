<?php

use App\ShopScheduleSlot;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenerateKeyCodeToSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->boolean('generate_key_code')->default(true);
        });
        ShopScheduleSlot::where('accounting', true)->update(['generate_key_code' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->dropColumn('generate_key_code');
        });
    }
}
