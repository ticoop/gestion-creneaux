<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('label');
            $table->boolean('shopping_allowed')->default(0);
            $table->unsignedInteger('min_duration')->nullable();
            $table->unsignedInteger('min_notice')->nullable();
            $table->softDeletes();
        });

        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('leave_type_id')->unsigned()->nullable();
            $table->foreign('leave_type_id')->references('id')->on('leave_types')
                ->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->text('comment')->nullable();
            $table->timestamps();
        });

        DB::table('leave_types')->insert([
            'label' => json_encode([
                'fr' => 'Vacances',
            ]),
            'shopping_allowed' => 0,
            'min_duration' => 4,
            'min_notice' => 4,
        ]);
        DB::table('leave_types')->insert([
            'label' => json_encode([
                'fr' => 'Congé maternité',
            ]),
            'shopping_allowed' => 1,
            'min_duration' => 10,
            'min_notice' => 4,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
        Schema::dropIfExists('leave_types');
    }
}
