<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->date('start_at');
            $table->date('end_at');
            $table->bigInteger('override_shop_schedule_id')->unsigned()->nullable();
            $table->foreign('override_shop_schedule_id')->references('id')->on('shop_schedules')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('working_shift_frequency');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_schedules');
    }
}
