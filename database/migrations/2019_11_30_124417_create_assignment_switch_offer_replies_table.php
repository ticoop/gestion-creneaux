<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentSwitchOfferRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_switch_offer_replies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('assignment_switch_offer_id')->unsigned();
            $table->foreign('assignment_switch_offer_id')->references('id')->on('assignment_switch_offers')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('reply_user_id')->unsigned();
            $table->foreign('reply_user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('reply_assignment_id')->unsigned();
            $table->foreign('reply_assignment_id')->references('id')->on('assignments')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->date('reply_date');
            $table->boolean('accepted')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_switch_offer_replies');
    }
}
