<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNbWorkersOnSlots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_schedules', function (Blueprint $table) {
            $table->unsignedInteger('min_workers_per_slot')->nullable()->after('working_shift_frequency');
            $table->unsignedInteger('max_workers_per_slot')->nullable()->after('min_workers_per_slot');
            $table->unsignedInteger('flying_members_per_slot')->nullable()->after('max_workers_per_slot');
            $table->unsignedInteger('max_referents_per_slot')->nullable()->after('flying_members_per_slot');
        });
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->unsignedInteger('min_workers_per_slot')->nullable()->after('should_update_assignments');
            $table->unsignedInteger('max_workers_per_slot')->nullable()->after('min_workers_per_slot');
            $table->unsignedInteger('flying_members_per_slot')->nullable()->after('max_workers_per_slot');
            $table->unsignedInteger('max_referents_per_slot')->nullable()->after('flying_members_per_slot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_schedules', function (Blueprint $table) {
            $table->dropColumn('min_workers_per_slot');
            $table->dropColumn('max_workers_per_slot');
            $table->dropColumn('flying_members_per_slot');
            $table->dropColumn('max_referents_per_slot');
        });
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->dropColumn('min_workers_per_slot');
            $table->dropColumn('max_workers_per_slot');
            $table->dropColumn('flying_members_per_slot');
            $table->dropColumn('max_referents_per_slot');
        });
    }
}
