<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOwedAssignmentOnActualAssignments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actual_assignments', function (Blueprint $table) {
            $table->unsignedBigInteger('owed_assignment_id')->nullable();
            $table->foreign('owed_assignment_id')->references('id')->on('owed_assignments')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actual_assignments', function (Blueprint $table) {
            $table->dropColumn('owed_assignment_id');
        });
    }
}
