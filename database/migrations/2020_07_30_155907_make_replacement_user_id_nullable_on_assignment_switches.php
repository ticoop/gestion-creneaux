<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeReplacementUserIdNullableOnAssignmentSwitches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignment_switches', function (Blueprint $table) {
            $table->bigInteger('replacement_user_id')->unsigned()->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignment_switches', function (Blueprint $table) {
            $table->bigInteger('replacement_user_id')->unsigned()->nullable(false)->change();
        });
    }
}
