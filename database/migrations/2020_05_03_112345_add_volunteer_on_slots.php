<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVolunteerOnSlots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->boolean('volunteer')->default(false)->after('max_referents_per_slot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->dropColumn('volunteer');
        });
    }
}
