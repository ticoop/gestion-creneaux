<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopScheduleSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_schedule_slots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_schedule_id')->unsigned();
            $table->foreign('shop_schedule_id')->references('id')->on('shop_schedules')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->tinyInteger('weekday');
            $table->time('start_at');
            $table->time('end_at');
            $table->boolean('shop_open')->default(1);
            $table->string('comment')->nullable();
            $table->boolean('should_update_assignments')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_schedule_slots');
    }
}
