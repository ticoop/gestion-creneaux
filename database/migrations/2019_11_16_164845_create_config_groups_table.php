<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique();
            $table->bigInteger('parent_group_id')->unsigned()->nullable();
            $table->foreign('parent_group_id')->references('id')->on('config_groups')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->json('label');
            $table->unsignedInteger('order');
            $table->string('admin_user_role')->nullable();
            $table->boolean('display_in_menu')->default(0);
            $table->string('icon')->nullable();
        });

        $shopOperationId = DB::table('config_groups')->insert([
            'key' => 'shop-operation',
            'label' => json_encode([
                'fr' => 'Fonctionnement magasin',
            ]),
            'order' => 1,
            'admin_user_role' => 'admin-shop-operation',
            'display_in_menu' => 1,
            'icon' => 'shopping_cart',
        ]);
        DB::table('config_groups')->insert([
            'parent_group_id' => $shopOperationId,
            'key' => 'shop-operation-slots',
            'label' => json_encode([
                'fr' => 'Créneaux de travail',
            ]),
            'order' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_groups');
    }
}
