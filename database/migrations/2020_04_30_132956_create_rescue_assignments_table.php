<?php

use App\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRescueAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rescue_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->date('date');
            $table->enum('half_day', ['am', 'pm']);
            $table->unsignedInteger('order');
            $table->boolean('tried')->default(false);
            $table->timestamps();
        });
        DB::table('roles')->insert([
            'key' => 'view-rescue-assignments',
            'label' => json_encode([
                'fr' => 'Consultation de l\'équipe secours',
            ]),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rescue_assignments');
        Role::where('key', 'view-rescue-assignments')->forceDelete();
    }
}
