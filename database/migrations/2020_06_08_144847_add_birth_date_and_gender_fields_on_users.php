<?php

use App\Gender;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBirthDateAndGenderFieldsOnUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('label');
            $table->string('dolibarr_code');
        });
        Gender::insert([
            'dolibarr_code' => 'man',
            'label' => json_encode(['fr' => 'Homme']),
        ]);
        Gender::insert([
            'dolibarr_code' => 'woman',
            'label' => json_encode(['fr' => 'Femme']),
        ]);

        Schema::table('users', function (Blueprint $table) {
            $table->date('birth_date')->nullable();
            $table->unsignedInteger('gender_id')->nullable();
            $table->foreign('gender_id')->references('id')->on('genders')
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('birth_date');
            $table->dropColumn('gender_id');
        });
        Schema::dropIfExists('genders');
    }
}
