<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ConfigGroup;

class CreateConfigItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique();
            $table->bigInteger('config_group_id')->unsigned();
            $table->foreign('config_group_id')->references('id')->on('config_groups')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->json('label');
            $table->json('description')->nullable();
            $table->enum('type', ['string', 'integer']);
            $table->json('type_params')->nullable();
            $table->boolean('required')->default(0);
            $table->unsignedInteger('order');
            $table->json('value')->nullable();
            $table->timestamps();
        });

        $configGroups = ConfigGroup::all()->pluck('id', 'key');

        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'working_shift_frequency',
            'label' => json_encode([
                'fr' => 'Fréquence des créneaux de travail',
            ]),
            'description' => json_encode([
                'fr' => 'Nombre de semaines entre chaque créneau de travail',
            ]),
            'type' => 'integer',
            'order' => 1,
            'required' => 1,
            'value' => 4,
        ]);
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'min_workers_per_slot',
            'label' => json_encode([
                'fr' => 'Nombre de personnes minimum par créneau',
            ]),
            'type' => 'integer',
            'order' => 2,
            'required' => 1,
            'value' => 4,
        ]);
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'max_workers_per_slot',
            'label' => json_encode([
                'fr' => 'Nombre de personnes maximum par créneau',
            ]),
            'description' => json_encode([
                'fr' => 'Idéalement, on devrait toujours avoir ce nombre. Cela n\'inclut pas les équipes volantes.',
            ]),
            'type' => 'integer',
            'order' => 2,
            'required' => 1,
            'value' => 5,
        ]);
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'flying_members_per_slot',
            'label' => json_encode([
                'fr' => 'Nombre de places pour l\'équipe volante et les rattrapages',
            ]),
            'type' => 'integer',
            'order' => 3,
            'required' => 1,
            'value' => 2,
        ]);
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'compensation_delay',
            'label' => json_encode([
                'fr' => 'Délai (en jours) pour effectuer un rattrapage',
            ]),
            'type' => 'integer',
            'order' => 4,
            'required' => 1,
            'value' => 42,
        ]);
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'slot_max_start_before',
            'label' => json_encode([
                'fr' => 'Avance possible (en minutes) pour le démarrage d\'un créneau',
            ]),
            'type' => 'integer',
            'order' => 5,
            'required' => 1,
            'value' => 30,
        ]);
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'slot_max_start_after',
            'label' => json_encode([
                'fr' => 'Retard possible (en minutes) pour le démarrage d\'un créneau',
            ]),
            'type' => 'integer',
            'order' => 6,
            'required' => 1,
            'value' => 30,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_items');
    }
}
