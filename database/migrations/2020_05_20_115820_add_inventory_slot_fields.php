<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventorySlotFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->enum('special_slot_type', ['inventory'])->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->unsignedTinyInteger('frequency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->dropColumn('special_slot_type');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('frequency');
        });
    }
}
