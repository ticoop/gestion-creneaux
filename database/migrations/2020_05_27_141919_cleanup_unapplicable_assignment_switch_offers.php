<?php

use App\AssignmentSwitchOffer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CleanupUnapplicableAssignmentSwitchOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $offers = AssignmentSwitchOffer::has('acceptedReplies')->with('acceptedReplies')->get();
        foreach ($offers as $offer) {
            AssignmentSwitchOffer::where('offer_assignment_id', $offer->offer_assignment_id)
                ->where('offer_date', $offer->offer_date)
                ->where('id', '!=', $offer->id)
                ->doesntHave('acceptedReplies')
                ->delete();
            foreach ($offer->acceptedReplies as $reply) {
                AssignmentSwitchOffer::where('offer_assignment_id', $reply->reply_assignment_id)
                    ->where('offer_date', $reply->reply_date)
                    ->where('id', '!=', $offer->id)
                    ->doesntHave('acceptedReplies')
                    ->delete();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
