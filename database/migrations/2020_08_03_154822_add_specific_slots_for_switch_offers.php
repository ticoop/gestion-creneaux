<?php

use App\AssignmentSwitchOffer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpecificSlotsForSwitchOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignment_switch_offer_possible_replacements', function (Blueprint $table) {
            $table->text('dates')->nullable();
        });

        $computes = [];
        $switchOffers = AssignmentSwitchOffer::withTrashed()->with(['possibleReplacements.schedule.slots', 'possibleReplacements.schedule.overrides.slots', 'assignment'])->get();
        foreach ($switchOffers as $offer) {
            $schedule = $offer->assignment->slot->schedule;
            
            foreach ($offer->possibleReplacements as $replacement) {
                $dates = [];
                if (!isset($computes[$replacement->schedule->id])) {
                    $computes[$replacement->schedule->id] = $schedule->computeSlotsByDay($replacement->schedule->start_at, $replacement->schedule->end_at);
                }
                foreach ($computes[$schedule->id] as $date => $slots) {
                    if ($slots->pluck('id')->contains($replacement->id)) {
                        $dates[] = $date;
                    }
                }
                $offer->possibleReplacements()->updateExistingPivot($replacement, ['dates' => implode(',', $dates)]);
            }
        }

        Schema::table('assignment_switch_offer_possible_replacements', function (Blueprint $table) {
            $table->text('dates')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignment_switch_offer_possible_replacements', function (Blueprint $table) {
            $table->dropColumn('dates');
        });
    }
}
