<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccountingFieldsOnUsersAndSlotsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('accounting_team')->default(false);
        });
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->boolean('accounting')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('accounting_team');
        });
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->dropColumn('accounting');
        });
    }
}
