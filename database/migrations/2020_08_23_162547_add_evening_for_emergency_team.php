<?php

use App\DayPart;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEveningForEmergencyTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = User::whereNotNull('emergency_team_days')->get();
        foreach ($users as $user) {
            $newDays = [];
            foreach ($user->emergency_team_days as $day) {
                $newDays[] = $day;
                if (substr($day, -2) == 'pm') {
                    $newDays[] = substr($day, 0, -2) . 'ev';
                }
                $user->emergency_team_days = $newDays;
                $user->save();
            }
        }
        Schema::table('day_parts', function (Blueprint $table) {
            $table->string('short_key')->nullable()->after('key');
        });
        DayPart::all()->each(function ($dayPart) {
            $dayPart->short_key = $dayPart->key == 'morning' ? 'am' : ($dayPart->key == 'afternoon' ? 'pm' : 'ev');
            $dayPart->save();
        });
        Schema::table('day_parts', function (Blueprint $table) {
            $table->string('short_key')->nullable(false)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('day_parts', function (Blueprint $table) {
            $table->dropColumn('short_key');
        });
        $users = User::whereNotNull('emergency_team_days')->get();
        foreach ($users as $user) {
            $newDays = [];
            foreach ($user->emergency_team_days as $day) {
                if (substr($day, -2) != 'ev') {
                    $newDays[] = $day;
                }
                $user->emergency_team_days = $newDays;
                $user->save();
            }
        }
    }
}
