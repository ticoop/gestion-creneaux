<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentWishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_wishes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('shop_schedule_id')->unsigned();
            $table->foreign('shop_schedule_id')->references('id')->on('shop_schedules')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->boolean('flying_team')->default(false);
            $table->boolean('referent')->default(false);
            $table->timestamps();
        });
        Schema::create('assignment_wish_shop_schedule_slot', function (Blueprint $table) {
            $table->bigInteger('assignment_wish_id')->unsigned();
            $table->foreign('assignment_wish_id')->references('id')->on('assignment_wishes')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('shop_schedule_slot_id')->unsigned();
            $table->foreign('shop_schedule_slot_id')->references('id')->on('shop_schedule_slots')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_wish_shop_schedule_slot');
        Schema::dropIfExists('assignment_wishes');
    }
}
