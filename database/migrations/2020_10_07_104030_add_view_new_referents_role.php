<?php

use App\Role;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddViewNewReferentsRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert([
            'key' => 'view-new-referents',
            'label' => json_encode([
                'fr' => 'Accès à la liste des nouveaux référents',
            ]),
        ]);

        $role = Role::where('key', 'view-new-referents')->first();

        User::whereHas('roles', function ($q) {
            $q->where('key', 'admin-slot-assignment');
        })->get()->each(function ($user) use ($role) {
            $user->roles()->attach($role);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::where('key', 'view-new-referents')->forceDelete();
    }
}
