<?php

use App\KeyCode;
use App\DayKeyCode;
use App\ShopSchedule;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConvertKeycodesToOneByDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_key_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('opening_code');
            $table->string('closing_code');
            $table->timestamps();
        });

        $lastCode = KeyCode::orderBy('date', 'DESC')->first();
        if (!!$lastCode) {
            $closingCode = $lastCode->value;
            $openingCode = KeyCode::where('date', '<', $lastCode->date->startOfDay())->orderBy('date', 'DESC')->first();
            if (!!$openingCode) {
                $openingCode = $openingCode->value;
            } else {
                $openingCode = config('key-codes.default-code');
            }
            DayKeyCode::create([
                'date' => $lastCode->date,
                'opening_code' => $openingCode,
                'closing_code' => $closingCode,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_key_codes');
    }
}
