<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTargetOnAssignmentSwitchOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignment_switch_offers', function (Blueprint $table) {
            $table->unsignedBigInteger('target_user_id')->nullable()->after('offer_assignment_id');
            $table->foreign('target_user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignment_switch_offers', function (Blueprint $table) {
            $table->dropColumn('target_user_id');
        });
    }
}
