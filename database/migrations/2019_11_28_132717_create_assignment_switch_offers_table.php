<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentSwitchOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_switch_offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('offer_user_id')->unsigned();
            $table->foreign('offer_user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('offer_assignment_id')->unsigned();
            $table->foreign('offer_assignment_id')->references('id')->on('assignments')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->date('offer_date');
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('assignment_switch_offer_possible_replacements', function (Blueprint $table) {
            $table->bigInteger('assignment_switch_offer_id')->unsigned();
            $table->foreign('assignment_switch_offer_id')->references('id')->on('assignment_switch_offers')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('shop_schedule_slot_id')->unsigned();
            $table->foreign('shop_schedule_slot_id')->references('id')->on('shop_schedule_slots')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_switch_offer_possible_replacements');
        Schema::dropIfExists('assignment_switch_offers');
    }
}
