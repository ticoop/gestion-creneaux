<?php

use App\ConfigGroup;
use App\ConfigItem;
use App\ShopSchedule;
use App\ShopScheduleSlot;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdealWorkersPerSlotField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $configGroups = ConfigGroup::all()->pluck('id', 'key');
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'ideal_workers_per_slot',
            'label' => json_encode([
                'fr' => 'Nombre de personnes idéal par créneau',
            ]),
            'description' => json_encode([
                'fr' => 'Idéalement, on devrait toujours avoir ce nombre. Cela n\'inclut pas les équipes volantes.',
            ]),
            'type' => 'integer',
            'order' => 2,
            'required' => 1,
            'value' => 5,
        ]);
        Schema::table('shop_schedules', function (Blueprint $table) {
            $table->unsignedInteger('ideal_workers_per_slot')->nullable()->after('max_workers_per_slot');
        });
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->unsignedInteger('ideal_workers_per_slot')->nullable()->after('max_workers_per_slot');
        });
        foreach (ShopSchedule::all() as $schedule) {
            $schedule->ideal_workers_per_slot = $schedule->max_workers_per_slot;
            $schedule->save();
        }
        foreach (ShopScheduleSlot::all() as $slot) {
            $slot->ideal_workers_per_slot = $slot->max_workers_per_slot;
            $slot->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ConfigItem::where('key', 'ideal_workers_per_slot')->forceDelete();
        Schema::table('shop_schedules', function (Blueprint $table) {
            $table->dropColumn('ideal_workers_per_slot');
        });
        Schema::table('shop_schedule_slots', function (Blueprint $table) {
            $table->dropColumn('ideal_workers_per_slot');
        });
    }
}
