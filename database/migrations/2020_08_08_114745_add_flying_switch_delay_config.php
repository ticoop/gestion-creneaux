<?php

use App\ConfigGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlyingSwitchDelayConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $configGroups = ConfigGroup::all()->pluck('id', 'key');
        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'flying_switch_delay',
            'label' => json_encode([
                'fr' => 'Délai pour programmer un créneau suite à un échange avec un volant',
            ]),
            'type' => 'integer',
            'order' => 8,
            'required' => 1,
            'value' => 30,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ConfigItem::where('key', 'flying_switch_delay')->forceDelete();
    }
}
