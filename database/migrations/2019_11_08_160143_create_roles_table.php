<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Role;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique();
            $table->json('label');
        });

        Schema::create('role_user', function (Blueprint $table) {
            $table->bigInteger('role_id')->unsigned()->nullable();
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unique(['role_id', 'user_id']);
        });
        
        DB::table('roles')->insert([
            'key' => 'admin',
            'label' => json_encode([
                'fr' => 'Administrateur',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'admin-users',
            'label' => json_encode([
                'fr' => 'Gestion des utilisateurs',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'admin-shop-schedule',
            'label' => json_encode([
                'fr' => 'Gestion du planning du magasin',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'admin-slot-assignment',
            'label' => json_encode([
                'fr' => 'Gestion des affectations aux créneaux',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'admin-trainings',
            'label' => json_encode([
                'fr' => 'Paramétrage des formations',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'admin-leave-types',
            'label' => json_encode([
                'fr' => 'Paramétrage des types de congés',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'admin-leaves',
            'label' => json_encode([
                'fr' => 'Gestion des congés',
            ]),
        ]);
        DB::table('roles')->insert([
            'key' => 'impersonate',
            'label' => json_encode([
                'fr' => 'Connexion à place d\'un autre utilisateur',
            ]),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('roles');
    }
}
