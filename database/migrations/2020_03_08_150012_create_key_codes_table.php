<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_schedule_slot_id');
            $table->foreign('shop_schedule_slot_id')->references('id')->on('shop_schedule_slots')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamp('date');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_codes');
    }
}
