<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\UsageEvent;

class CreateUsageEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usage_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique();
        });

        UsageEvent::create(['key' => 'login']);
        UsageEvent::create(['key' => 'logout']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usage_events');
    }
}
