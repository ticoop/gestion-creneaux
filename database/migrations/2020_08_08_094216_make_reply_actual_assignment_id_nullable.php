<?php

use App\AssignmentSwitchOffer;
use App\AssignmentSwitchOfferReply;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeReplyActualAssignmentIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignment_switch_offer_replies', function (Blueprint $table) {
            $table->bigInteger('reply_assignment_id')->unsigned()->nullable(true)->change();
            $table->date('reply_date')->nullable(true)->change();
            $table->boolean('reply_flying')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        AssignmentSwitchOfferReply::where('reply_flying', true)->forceDelete();
        Schema::table('assignment_switch_offer_replies', function (Blueprint $table) {
            $table->bigInteger('reply_assignment_id')->unsigned()->nullable(false)->change();
            $table->date('reply_date')->nullable(false)->change();
            $table->dropColumn('reply_flying');
        });
    }
}
