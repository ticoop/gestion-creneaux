<?php

use App\ConfigGroup;
use App\ConfigItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferentCountConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $configGroups = ConfigGroup::all()->pluck('id', 'key');

        DB::table('config_items')->insert([
            'config_group_id' => $configGroups['shop-operation-slots'],
            'key' => 'max_referents_per_slot',
            'label' => json_encode([
                'fr' => 'Nombre maximal de référent·e·s par créneau',
            ]),
            'type' => 'integer',
            'order' => 7,
            'required' => 1,
            'value' => 2,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ConfigItem::where('key', 'max_referents_per_slot')->forceDelete();
    }
}
