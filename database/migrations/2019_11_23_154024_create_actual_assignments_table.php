<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActualAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_schedule_slot_id')->unsigned()->nullable();
            $table->foreign('shop_schedule_slot_id')->references('id')->on('shop_schedule_slots')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->date('date');
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->bigInteger('assignment_id')->unsigned()->nullable();
            $table->foreign('assignment_id')->references('id')->on('assignments')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->boolean('referent');
            $table->boolean('done')->nullable();
            $table->timestamp('arrived_at')->nullable();
            $table->tinyInteger('compensations_needed')->default(0);
            $table->bigInteger('compensates_actual_assignment_id')->unsigned()->nullable();
            $table->foreign('compensates_actual_assignment_id')->references('id')->on('actual_assignments')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_assignments');
    }
}
