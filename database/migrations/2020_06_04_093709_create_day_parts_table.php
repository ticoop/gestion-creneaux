<?php

use App\DayPart;
use App\RescueAssignment;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDayPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_parts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key');
            $table->json('label');
            $table->unsignedSmallInteger('order');
            $table->string('start_time', 5)->nullable();
            $table->string('end_time', 5)->nullable();
        });
        DayPart::insert([
            'key' => 'morning',
            'label' => json_encode([
                'fr' => 'matin',
            ]),
            'start_time' => null,
            'end_time' => '12:00',
            'order' => 1,
        ]);
        DayPart::insert([
            'key' => 'afternoon',
            'label' => json_encode([
                'fr' => 'après-midi',
            ]),
            'start_time' => '12:00',
            'end_time' => '18:30',
            'order' => 2,
        ]);
        DayPart::insert([
            'key' => 'evening',
            'label' => json_encode([
                'fr' => 'soir',
            ]),
            'start_time' => '18:30',
            'end_time' => null,
            'order' => 3,
        ]);

        Schema::create('rescue_team_days', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('day_part_id');
            $table->foreign('day_part_id')->references('id')->on('day_parts')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('weekdays');
        });

        $dayParts = DayPart::all()->pluck('id', 'key');
        $users = User::whereNotNull('rescue_days')->get();
        foreach ($users as $user) {
            $newDays = [];
            $rescueDays = explode(",", $user->rescue_days);
            foreach ($rescueDays as $day) {
                $weekday = $day[0];
                $thisDayParts = ['am' => ['morning'], 'pm' => ['afternoon', 'evening']][substr($day, 1)];
                foreach ($thisDayParts as $dayPart) {
                    if (!isset($newDays[$dayParts[$dayPart]])) {
                        $newDays[$dayParts[$dayPart]] = [];
                    }
                    $newDays[$dayParts[$dayPart]][] = $weekday;
                }
            }
            foreach ($newDays as $key => $value) {
                $newDays[$key] = ['weekdays' => implode(',', $value)];
            }
            $user->rescueTeamDays()->sync($newDays);
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('rescue_days');
        });

        Schema::table('rescue_assignments', function (Blueprint $table) {
            $table->unsignedInteger('day_part_id')->nullable();
            $table->foreign('day_part_id')->references('id')->on('day_parts')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
        foreach (RescueAssignment::all() as $rescueAssignment) {
            if ($rescueAssignment->half_day == 'pm') {
                $rescueAssignment->day_part_id = $dayParts['afternoon'];
                $rescueAssignment->save();
            } else {
                $rescueAssignment->day_part_id = $dayParts['morning'];
                $rescueAssignment->save();
            }
        }
        Schema::table('rescue_assignments', function (Blueprint $table) {
            $table->unsignedInteger('day_part_id')->nullable(false)->change();
            $table->dropColumn('half_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rescue_assignments', function (Blueprint $table) {
            $table->string('half_day')->after('day_part_id')->nullable();
        });
        foreach (RescueAssignment::all() as $rescueAssignment) {
            if ($rescueAssignment->dayPart->key == 'morning') {
                $rescueAssignment->half_day = 'am';
                $rescueAssignment->save();
            } else {
                $rescueAssignment->half_day = 'pm';
                $rescueAssignment->save();
            }
        }
        Schema::table('rescue_assignments', function (Blueprint $table) {
            $table->string('half_day')->nullable(false)->change();
            $table->dropColumn('day_part_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('rescue_days', 255)->nullable()->after('start_date');
        });

        $users = User::has('rescueTeamDays')->with('rescueTeamDays')->get();
        foreach ($users as $user) {
            $halfDays = [];
            foreach ($user->rescueTeamDays as $day) {
                $weekdays = explode(',', $day->pivot->weekdays);
                $halfDay = ['morning' => 'am', 'afternoon' => 'pm', 'evening' => 'pm'][$day->key];
                foreach ($weekdays as $weekday) {
                    $halfDays[] = $weekday . $halfDay;
                }
            }
            $user->rescue_days = collect($halfDays)->unique()->implode(",");
            $user->save();
        }
        Schema::dropIfExists('rescue_team_days');
        Schema::dropIfExists('day_parts');
    }
}
