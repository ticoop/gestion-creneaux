<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:web,api')->group(function () {
    Route::get('/user', 'UserController@showCurrentUser');

    Route::get('menus/{menu}', 'MenuController@show');
    Route::get('users/export', 'UserController@export');
    Route::apiResource('users', 'UserController', ['only' => ['index', 'show', 'update']]);
    Route::get('users/{user}/photo', 'UserController@getPhoto');
    Route::post('users/{user}/impersonate', 'UserController@impersonate');
    Route::post('users/stop-impersonating', 'UserController@stopImpersonating');
    Route::apiResource('roles', 'RoleController', ['only' => ['index']]);
    Route::get('shop-schedules/compute', 'ShopScheduleController@compute');
    Route::get('shop-schedules/get-user-assignments', 'ShopScheduleController@getCurrentUserAssignments');
    Route::get('shop-schedules/get-user-compensations', 'ShopScheduleController@getCurrentUserCompensations');
    Route::get('shop-schedules/{shop_schedule}/assignments/{user}', 'ShopScheduleController@getUserAssignments');
    Route::apiResource('shop-schedules', 'ShopScheduleController');
    Route::apiResource('shop-schedules.overrides', 'ShopScheduleOverrideController', ['except' => ['show', 'destroy']]);
    Route::apiResource('shop-schedules.slots.assignments', 'ShopScheduleSlotAssignmentController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('trainings', 'TrainingController');
    Route::apiResource('users.actual-assignments', 'UserActualAssignmentController');
    Route::apiResource('assignment-switch-offers', 'AssignmentSwitchOfferController');
    Route::apiResource('key-codes', 'KeyCodeController', ['only' => ['index']]);
    Route::apiResource('day-key-codes', 'DayKeyCodeController', ['only' => ['index', 'show']]);
    Route::post('assignment-switch-offers/{assignment_switch_offer}/reply', 'AssignmentSwitchOfferController@reply');
    Route::post('assignment-switch-offers/{assignment_switch_offer}/cancel-reply', 'AssignmentSwitchOfferController@cancelReply');
    Route::post('assignment-switch-offers/{assignment_switch_offer}/accept-reply', 'AssignmentSwitchOfferController@acceptReply');
    Route::post('assignment-switch-offers/{assignment_switch_offer}/reject-reply', 'AssignmentSwitchOfferController@rejectReply');
    Route::get('config-groups/{config_group}', 'ConfigController@showGroup');
    Route::put('config-groups/{config_group}/set-values', 'ConfigController@setGroupValues');
    Route::get('config', 'ConfigController@getAll');
    Route::get('leaves', 'LeaveController@index');
    Route::get('leaves/{leave}', 'LeaveController@show');
    Route::get('users/{user}/leaves', 'UserLeaveController@index');
    Route::get('users/{user}/leaves/{leave}', 'UserLeaveController@show');
    Route::post('users/{user}/leaves', 'UserLeaveController@store');
    Route::put('users/{user}/leaves/{leave}', 'UserLeaveController@update');
    Route::delete('users/{user}/leaves/{leave}', 'UserLeaveController@destroy');
    Route::apiResource('leave-types', 'LeaveTypeController');
    Route::post('shop-schedule-slots/{shop_schedule_slot}/assign-flying', 'ShopScheduleSlotController@assignFlying');
    Route::post('shop-schedule-slots/{shop_schedule_slot}/unassign-flying', 'ShopScheduleSlotController@unassignFlying');
    Route::get('badging/user/{user}', 'BadgingController@getUser')->where('user', '[0-9]+');
    Route::get('badging/user/{username}', 'BadgingController@getUserByUsername');
    Route::post('badging/start-assignment/{actual_assignment}', 'BadgingController@startAssignment');
    Route::get('rescue-assignments/get-next-user', 'RescueAssignmentController@getNextUser');
    Route::post('rescue-assignments/set-unavailable', 'RescueAssignmentController@setUnavailable');
    Route::apiResource('actual-assignments', 'ActualAssignmentController', ['only' => ['update', 'show']]);
    Route::apiResource('sent-emails', 'SentEmailController', ['only' => ['index', 'show']]);
    Route::apiResource('day-parts', 'DayPartController', ['only' => ['index']]);
    Route::get('stats/assignment-switches', 'StatsController@getAssignmentSwitchesStats');
    Route::get('stats/users', 'StatsController@getUsersStats');
    Route::get('stats/members-age', 'StatsController@getMembersAgeStats');
    Route::get('stats/members-gender', 'StatsController@getMembersGenderStats');
    Route::get('stats/attendance', 'StatsController@getAttendanceStats');
    Route::get('mattermost/get-conversation-url', 'MattermostController@getConversationUrl');
    Route::apiResource('assignment-switches', 'AssignmentSwitchController', ['only' => ['destroy']]);
    Route::apiResource('users.assignment-switches', 'UserAssignmentSwitchController', ['only' => ['index']]);
    Route::get('export-database', 'TechnicalToolsController@exportDatabase');
    Route::apiResource('users.owed-assignments', 'UserOwedAssignmentController', ['only' => ['index']]);
});

Route::get('users/{user}/calendar.ics', 'UserController@getIcsCalendar')
    ->name('user-calendar')
    ->middleware('signed');

Route::fallback(function () {
    abort(404);
});
